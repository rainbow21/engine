xof 0302txt 0032

// Direct3D .x file translation of Scene_Root
// Generated by 3DSMax FSModelExp Plug-in, Version 1.00
// Tue Apr 17 00:34:21 2018


template Header {
 <3D82AB43-62DA-11cf-AB39-0020AF71E433>
 WORD major;
 WORD minor;
 DWORD flags;
}

template Vector {
 <3D82AB5E-62DA-11cf-AB39-0020AF71E433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template Coords2d {
 <F6F23F44-7686-11cf-8F52-0040333594A3>
 FLOAT u;
 FLOAT v;
}

template Matrix4x4 {
 <F6F23F45-7686-11cf-8F52-0040333594A3>
 array FLOAT matrix[16];
}

template ColorRGBA {
 <35FF44E0-6C7C-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <D3E16E81-7835-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template TextureFilename {
 <A42790E1-7810-11cf-8F52-0040333594A3>
 STRING filename;
}

template Material {
 <3D82AB4D-62DA-11cf-AB39-0020AF71E433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshFace {
 <3D82AB5F-62DA-11cf-AB39-0020AF71E433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template MeshTextureCoords {
 <F6F23F40-7686-11cf-8F52-0040333594A3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshMaterialList {
 <F6F23F42-7686-11cf-8F52-0040333594A3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material]
}

template MeshNormals {
 <F6F23F43-7686-11cf-8F52-0040333594A3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template Mesh {
 <3D82AB44-62DA-11cf-AB39-0020AF71E433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template FrameTransformMatrix {
 <F6F23F41-7686-11cf-8F52-0040333594A3>
 Matrix4x4 frameMatrix;
}

template Frame {
 <3D82AB46-62DA-11cf-AB39-0020AF71E433>
 [...]
}
template FloatKeys {
 <10DD46A9-775B-11cf-8F52-0040333594A3>
 DWORD nValues;
 array FLOAT values[nValues];
}

template TimedFloatKeys {
 <F406B180-7B3B-11cf-8F52-0040333594A3>
 DWORD time;
 FloatKeys tfkeys;
}

template AnimationKey {
 <10DD46A8-775B-11cf-8F52-0040333594A3>
 DWORD keyType;
 DWORD nKeys;
 array TimedFloatKeys keys[nKeys];
}

template AnimationOptions {
 <E2BF56C0-840F-11cf-8F52-0040333594A3>
 DWORD openclosed;
 DWORD positionquality;
}

template Animation {
 <3D82AB4F-62DA-11cf-AB39-0020AF71E433>
 [...]
}

template AnimationSet {
 <3D82AB50-62DA-11cf-AB39-0020AF71E433>
 [Animation]
}

template DiffuseTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07401>
 STRING filename;
}

template AmbientTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07402>
 STRING filename;
}

template EmissiveTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07403>
 STRING filename;
}

template ReflectionTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07404>
 STRING filename;
}

template ShininessTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07405>
 STRING filename;
}

template BumpTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07406>
 STRING filename;
}

template DisplacementTextureFileName {
 <E00200E2-D4AB-481a-9B85-E20F9AE07407>
 STRING filename;
}

template PartData {
 <79B183BA-7E70-44d1-914A-23B304CA91E5>
 DWORD nByteCount;
 array BYTE XMLData[ nByteCount ];
}

Header {
	1;
	0;
	1;
	}

//=====================
// FILE NODE HEIRARCHY
//=====================
// Scene_Root (1 Children)
//   Cone01 (0 Children)

Frame frm-MasterScale {
FrameTransformMatrix {
   0.000977, 0.0, 0.0, 0.0,
   0.0, 0.000977, 0.0, 0.0,
   0.0, 0.0, 0.000977, 0.0,
   0.0, 0.0, 0.0, 1.0;;
}		// End frm-MasterScale FrameTransformMatrix
Frame frm-MasterUnitConversion {
FrameTransformMatrix {
   26.009600, 0.0, 0.0, 0.0,
   0.0, 26.009600, 0.0, 0.0,
   0.0, 0.0, 26.009600, 0.0,
   0.0, 0.0, 0.0, 1.0;;
}		// End frm-MasterUnitConversion FrameTransformMatrix
Frame RotateAroundX {
FrameTransformMatrix {
   1.0, 0.0, 0.0, 0.0,
   0.0, 0.0, 1.0, 0.0,
   0.0, -1.0, 0.0, 0.0,
   0.0, 0.0, 0.0, 1.0;;
}		// End Frame RotateAroundX FrameTransformMatrix
Frame frm-Cone01 {
FrameTransformMatrix {
  1.000000, 0.000000, 0.000000, 0.0,
  0.000000, -1.000000, 0.000000, 0.0,
  0.000000, -0.000000, -1.000000, 0.0,
  -25.210003, 24.722038, 0.000000, 1.0;;
}		// End FrameTransformMatrix

Mesh Cone01 {
25;		// Mesh 'Cone01' contains 25 vertices
  0.000000; 0.000000; 0.000000;,
  7.000000; 0.000000; 0.000000;,
  5.362311; 4.499514; 0.000000;,
  1.215537; 6.893654; 0.000000;,
  -3.500001; 6.062177; 0.000000;,
  -6.577849; 2.394140; 0.000000;,
  -6.577848; -2.394142; 0.000000;,
  -3.499999; -6.062179; 0.000000;,
  1.215539; -6.893654; 0.000000;,
  5.362312; -4.499512; 0.000000;,
  31.000000; 0.000000; -40.000000;,
  23.747377; 19.926416; -40.000000;,
  5.383091; 30.529041; -40.000000;,
  -15.500003; 26.846786; -40.000000;,
  -29.130472; 10.602620; -40.000000;,
  -29.130468; -10.602631; -40.000000;,
  -15.499993; -26.846790; -40.000000;,
  5.383102; -30.529039; -40.000000;,
  23.747383; -19.926409; -40.000000;,
  0.000000; 0.000000; -40.000000;,
  1.215537; 6.893654; 0.000000;,
  1.215537; 6.893654; 0.000000;,
  5.383091; 30.529041; -40.000000;,
  1.215537; 6.893654; 0.000000;,
  5.383091; 30.529041; -40.000000;;

36;		// Mesh 'Cone01' contains 36 Faces
  3; 1, 2, 0;,
  3; 2, 3, 0;,
  3; 20, 4, 0;,
  3; 4, 5, 0;,
  3; 5, 6, 0;,
  3; 6, 7, 0;,
  3; 7, 8, 0;,
  3; 8, 9, 0;,
  3; 9, 1, 0;,
  3; 10, 11, 1;,
  3; 11, 2, 1;,
  3; 11, 12, 2;,
  3; 12, 3, 2;,
  3; 22, 13, 21;,
  3; 13, 4, 23;,
  3; 13, 14, 4;,
  3; 14, 5, 4;,
  3; 14, 15, 5;,
  3; 15, 6, 5;,
  3; 15, 16, 6;,
  3; 16, 7, 6;,
  3; 16, 17, 7;,
  3; 17, 8, 7;,
  3; 17, 18, 8;,
  3; 18, 9, 8;,
  3; 18, 10, 9;,
  3; 10, 1, 9;,
  3; 11, 10, 19;,
  3; 12, 11, 19;,
  3; 13, 24, 19;,
  3; 14, 13, 19;,
  3; 15, 14, 19;,
  3; 16, 15, 19;,
  3; 17, 16, 19;,
  3; 18, 17, 19;,
  3; 10, 18, 19;;
MeshMaterialList {
  1; // 1 Materials
  36; // 36 Faces have materials specified
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;
  Material Mtl__10 {
  0.588000; 0.588000; 0.588000; 1.000000;;
  0.000000;
  0.900000; 0.900000; 0.900000;;
  0.000000; 0.000000; 0.000000;;
  TextureFileName {
     "Player.bmp";
  }
  DiffuseTextureFileName  {
    "Player.bmp";
  }
}		// End Material 'Mtl__10'
}		// End MaterialList for 'Cone01'
MeshNormals {		// Mesh normals for Cone01
20;		// 20 vertex normals
  0.000000; 0.000000; 1.000000;,
  0.852890; 0.103476; 0.511734;,
  0.719864; 0.468960; 0.511734;,
  0.852890; -0.103475; 0.511734;,
  0.586839; 0.627494; 0.511734;,
  0.250006; 0.821964; 0.511734;,
  0.046199; 0.857901; 0.511734;,
  -0.336833; 0.790362; 0.511734;,
  -0.516057; 0.686886; 0.511734;,
  -0.766064; 0.388941; 0.511734;,
  -0.836845; 0.194470; 0.511734;,
  -0.836845; -0.194471; 0.511734;,
  -0.766064; -0.388941; 0.511734;,
  -0.516057; -0.686887; 0.511734;,
  -0.336832; -0.790362; 0.511734;,
  0.046200; -0.857901; 0.511734;,
  0.250006; -0.821964; 0.511734;,
  0.586839; -0.627494; 0.511734;,
  0.719864; -0.468960; 0.511734;,
  0.000000; 0.000000; -1.000000;;
36;		// 36 faces with normals
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 0, 0, 0;,
  3; 3, 2, 1;,
  3; 2, 4, 1;,
  3; 2, 5, 4;,
  3; 5, 6, 4;,
  3; 5, 7, 6;,
  3; 7, 8, 6;,
  3; 7, 9, 8;,
  3; 9, 10, 8;,
  3; 9, 11, 10;,
  3; 11, 12, 10;,
  3; 11, 13, 12;,
  3; 13, 14, 12;,
  3; 13, 15, 14;,
  3; 15, 16, 14;,
  3; 15, 17, 16;,
  3; 17, 18, 16;,
  3; 17, 3, 18;,
  3; 3, 1, 18;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;,
  3; 19, 19, 19;;
}		// End Mesh normals for Cone01
  MeshTextureCoords {
  25;		// 25 uv pairs
  0.500000; 1.000000;,
  0.750000; 1.000000;,
  0.861111; 1.000000;,
  0.972222; 1.000000;,
  0.083333; 1.000000;,
  0.194444; 1.000000;,
  0.305556; 1.000000;,
  0.416667; 1.000000;,
  0.527778; 1.000000;,
  0.638889; 1.000000;,
  0.750000; 0.000000;,
  0.861111; 0.000000;,
  0.972222; 0.000000;,
  0.083333; 0.000000;,
  0.194444; 0.000000;,
  0.305556; 0.000000;,
  0.416667; 0.000000;,
  0.527778; 0.000000;,
  0.638889; 0.000000;,
  0.500000; 0.000000;,
  -0.027778; 1.000000;,
  -0.027778; 1.000000;,
  -0.027778; 0.000000;,
  -0.027778; 1.000000;,
  -0.027778; 0.000000;;
  }		// End MeshTexture Coords for 'Cone01'
}		// End of Mesh 'Cone01'
}		 // End of frame frm-Cone01

}		 // End of RotateAroundX frame

}		 // End of frm-MasterUnitConversion frame

}		 // End of frm-MasterScale frame


AnimationSet {		// Collection of Animations
}		// End of AnimationSet.
