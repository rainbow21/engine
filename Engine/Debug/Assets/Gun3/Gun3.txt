#begin

type            number 0
name            string "Gun 3"
ellipse_radius  vector 1.0 1.0 1.0

mesh            string "Gun3.x"
mesh_path       string "./Assets/Gun3/"
#end