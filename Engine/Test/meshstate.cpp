#include"meshstate.h"

MeshState::MeshState(unsigned long id) :State(id)
{
	m_id = id;
}

void MeshState::load()
{
	m_mesh = new Mesh("Gun1.x", "./Assets/Gun1/");

	Matrix4x4 view;
	D3DXMatrixLookAtLH(&view, &Vector3(-50.0f, 50.0f, -150.0f), &Vector3(0.0f, 0.0f, 0.0f), &Vector3(0.0f, 1.0f, 0.0f));
	g_engine->getDevice()->SetTransform(D3DTS_VIEW, &view);
}

void MeshState::update(float elapsed)
{
	static Vector3 pos = Vector3(-50.0f, 50.0f, -150.0f);
	if (g_engine->getInput()->getKeysPress(DIK_W, true))	pos.z += 0.1f;
	if (g_engine->getInput()->getKeysPress(DIK_S, true))	pos.z -= 0.1f;
	if (g_engine->getInput()->getKeysPress(DIK_A, true))	pos.x -= 0.1f;
	if (g_engine->getInput()->getKeysPress(DIK_D, true))	pos.x += 0.1f;

	if (g_engine->getInput()->getKeysPress(DIK_Q, true))
		PostQuitMessage(0);

	Matrix4x4 view;
	D3DXMatrixLookAtLH(&view, &pos, &Vector3(pos.x + 50.0f, pos.y - 50.0f, pos.z + 150.0f), &Vector3(0.0f, 1.0f, 0.0f));
	g_engine->getDevice()->SetTransform(D3DTS_VIEW, &view);
}

void MeshState::close()
{
	SAFE_DELETE(m_mesh);
}

void MeshState::requestViewer(ViewerSetup *viewer)
{
	viewer->viewClearFlags = D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER;
	viewer->viewer = NULL;
}

void MeshState::render()
{
	m_mesh->render();
}