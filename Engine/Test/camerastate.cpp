#include "camerastate.h"

CameraState::CameraState(unsigned long id) :State(id)
{
	camera = new SceneObject;
	camera->setTranslation(0, 0, -400.0f);
	camera->setFriction(8.0);
	speedCamera = 2;
}

void CameraState::requestViewer(ViewerSetup *viewer)
{
	viewer->viewer = camera;
	viewer->viewClearFlags = D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER;
}