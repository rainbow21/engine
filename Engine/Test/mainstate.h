#ifndef MAINSTATE_H
#define MAINSTATE_H
#include "../Engine/engine.h"
#include "camerastate.h"

#define TEST_MOVEMENT_BASED_ON_THE_TIME

class MainState :public CameraState
{
protected:
	LinkedList<SceneObject> object;		// �������
	LinkedList<Label>		label;		// �������
	Font					font;

	int playerIndex;							// ������ ������ � ������� ��������
	int speedPlayer;

	// ���������� �������
	void cameraControl(float elapsed);
	// ����� ���������� � ������
	void cameraInfoView();
	void playerControl(float elapsed);
public:
	MainState(unsigned long id);
	virtual void load();
	virtual void render();
	virtual void update(float elapsed);
	virtual void close();
};

#endif
