#include "fontstate.h"
FontState::FontState(unsigned long id) :State(id)
{
	m_id = id;
}

// ��������������� ��������
void FontState::load()
{
	m_font = new Font;
}

// ����-������������ ��������
void FontState::close()
{
	SAFE_DELETE(m_font);
}

// ��������� ���� ��� ������� ������
void FontState::requestViewer(ViewerSetup *viewer)
{
	viewer->viewClearFlags = D3DCLEAR_TARGET;
	viewer->viewer = NULL;
}

void FontState::update(float elapsed)
{
	// ������������ FPS
	static float frameTime = 1.0f;
	static int frameCount = 0;
	frameTime += elapsed;
	frameCount++;

	// ��������� ������
	if (frameTime > 1.0f)
	{
		sprintf(m_fps, "%d fps", frameCount);
		frameTime = 0.0f;
		frameCount = 0;
	}

	if (g_engine->getInput()->getKeysPress(DIK_Q))
		PostQuitMessage(0);
}

void FontState::render()
{
	m_font->render("HEEEEEEE!", 0, 0);
	m_font->render(m_fps, 10, 100, D3DCOLOR_COLORVALUE(1.0f, 0.0f, 1.0f, 1.0f));
	m_font->render("!!!!", 100, 100, CL_GREEN);
}