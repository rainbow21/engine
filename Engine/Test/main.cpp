#include <Windows.h>

#include "..\Engine\engine.h"
#include "fontstate.h"
#include "meshstate.h"
#include "scenestate.h"
#include "mainstate.h"

void stateSetup()
{
	//ENGINE->addState(new FontState(0), true);
	//ENGINE->addState(new MeshState(1), true);
	//ENGINE->addState(new SceneState(2), true);
	//ENGINE->addState(new MainState(3), true);
	ENGINE->addState(new MainState(0), true);
}

int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev, LPSTR cmdLine, int cmdShow)
{
	EngineSetup setup;
	setup.instance = instance;
	setup.name = "Object Test";
	setup.totalBackBuffers = 2;
	setup.scale = 0.5;
	setup.stateSetup = stateSetup;


	new Engine(&setup);
	g_engine->run();

	return true;
}