#ifndef FONTSTATE_H
#define FONTSTATE_H
#include "../Engine/engine.h"

class FontState :public State
{
private:
	Font *m_font;	// ����� ��� ������
	char m_fps[16];	// ��������� fps

public:
	FontState(unsigned long id);
	// ��������������� ��������
	virtual void load();
	// ����-������������ ��������
	virtual void close();
	// ��������� ���� ��� ������� ������
	virtual void requestViewer(ViewerSetup *viewer);
	virtual void update(float elapsed);
	virtual void render();
};


#endif
