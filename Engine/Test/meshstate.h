#ifndef MESHSTATE_H
#define MESHSTATE_H
#include "../Engine/engine.h"

class MeshState :public State
{
private:
	Mesh *m_mesh;	// ��������� �� 3D-���
public:
	MeshState(unsigned long id);
	virtual void load();
	virtual void update(float elapsed);
	virtual void close();
	virtual void requestViewer(ViewerSetup *viewer);
	virtual void render();
};


#endif
