#ifndef SCENESTATE_H
#define SCENESTATE_H
#include "../Engine/engine.h"
class SceneState :public State
{
private:
	AnimatedObject	*m_man;	// ������������� ������ ���������
	SceneObject		*m_viewer;		// ������ ������
	SpawnerObject	*m_spawner;		// �������� �����
public:
	SceneState(unsigned long id);
	virtual void load();
	virtual void close();
	virtual void update(float elapsed);
	virtual void render();
	virtual void requestViewer(ViewerSetup *viewer);
};
#endif