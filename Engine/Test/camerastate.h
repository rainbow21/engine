#ifndef CAMERASTATE_H
#define CAMERASTATE_H
#include "../Engine/engine.h"

// ���������, � ������� ���� ������. ����� �����������.
class CameraState :public State
{
protected:
	SceneObject *camera;
	char cameraInfo[MAX_PATH];
	int speedCamera;
public:
	CameraState(unsigned long id = 0);
	virtual void requestViewer(ViewerSetup *viewer);
};

#endif
