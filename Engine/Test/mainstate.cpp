#include "mainstate.h"
#include <limits>

MainState::MainState(unsigned long id) :CameraState(id)
{
	m_id = id;
	// ������-������ � ����������� � ������
	label.add(new Label(cameraInfo, 10, 10));
	speedPlayer = 2;
}

void MainState::load()
{ 
	SceneObject *o;

	float start = -500;
	float delta = 200;
	for (int i = 0; i < 5; i++)
	{
		o = object.add(new SpawnerObject("Box.txt", "./Assets/"));
		o->addTranslation(start + i*delta, 0.0, 0.0);
		o->setFriction(8);
	}

	o = object.add(new SceneObject(TYPE_SCENE_OBJECT, "Player.x", "./Assets/Base/Player/"));
	playerIndex = object.getTotalElements() - 1;
	o->setTranslation(0, 0, -200);
	BoundingSphere s = o->getBoundingSphere();
	o->setBoundingSphere(s.center - Vector3(0, 0, 40), 5);
	o->setFriction(3);
}

void MainState::close()
{

}

void MainState::render()
{
	// ���������� ���� ��������
	label.iterate(true);
	while (label.iterate())
		font.render(*label.getCurrent());

	// ���������� ���� ��������
	object.iterate(true);
	while (object.iterate())
		if (object.getCurrent()->isVisible())
			object.getCurrent()->render();
}

void MainState::update(float elapsed)
{
	SceneObject *player = object.get(playerIndex);

	if (ENGINE->mouseDeltaWheel() > 0)
		camera->addVelocity(camera->getForwardVector() * speedCamera * 10);
	if (ENGINE->mouseDeltaWheel() < 0)
		camera->addVelocity(-camera->getForwardVector() * speedCamera * 10);

	if (ENGINE->getInput()->getKeysPress(DIK_Q, true))
		PostQuitMessage(0);

	if (ENGINE->mouseClick(LEFT_MOUSE_BUTTON, true))
	{
		Vector3 v = player->getTranslation();
		Vector3 delta(0, 50, -100);
		camera->setTranslation(v + delta);

		Vector3 cameraPos = camera->getTranslation();
		Vector3 playerPos = player->getTranslation();
		Vector3 see(playerPos.x - cameraPos.x, playerPos.y - cameraPos.y, playerPos.z - cameraPos.z);

		v = player->getForwardVector();
		camera->setRotation(0, v.x, 0);
	}

	// ���������� �������
	playerControl(elapsed);

	
	if (ENGINE->getInput()->getButtonPress(0, false))
		label.add(new Label("X", ENGINE->getInput()->getPosX(), ENGINE->getInput()->getPosY(), CL_RED));
	
	// �������� ������������ � �������
	int n = object.getTotalElements();
	/*
	for (int i = 0; i < n; i++)
	{
		if (i == playerIndex)
			continue;
		SceneObject *o = object.get(i);
		float sum = player->getBoundingSphere().radius + ((SpawnerObject*) o)->getBoundingSphere().radius;
		if (o->getType() == TYPE_SPAWNER_OBJECT)
		{
			if (isSphereCollidingWithSphere(0, player->getBoundingSphere().center, o->getBoundingSphere().center, Vector3(0,0,0), sum))
				o->collisionOccurred(player, 0);
		
		}
	}
	*/

	// ���������� �������
	cameraControl(elapsed);
	// ���������� ������ - ��� �� ������ � ������ ��������
	camera->update(elapsed);
	cameraInfoView();

	// ���������� ���� ��������
	object.iterate(true);
	while (object.iterate())
		object.getCurrent()->update(elapsed);
}

void MainState::cameraControl(float elapsed)
{
	if (ENGINE->mouseClick(RIGHT_MOUSE_BUTTON, true))
	{
		camera->addSpin(
			ENGINE->mouseDeltaY() * 10 * elapsed,
			ENGINE->mouseDeltaX() * 10 * elapsed,
			0.0);
		if (ENGINE->keyPress(DIK_W, true))
			camera->addVelocity(camera->getForwardVector() * speedCamera);
			
		if (ENGINE->keyPress(DIK_S, true))
			camera->addVelocity(-camera->getForwardVector() * speedCamera);
			
		if (ENGINE->keyPress(DIK_A, true))
			camera->addVelocity(-camera->getRightVector() * speedCamera);
		if (ENGINE->keyPress(DIK_D, true))
			camera->addVelocity(camera->getRightVector() * speedCamera);
	}

	
}

void MainState::cameraInfoView()
{
	Vector3 velocity = object.get(playerIndex)->getVelocity();
	Vector3 forward = object.get(playerIndex)->getForwardVector();
	sprintf(cameraInfo,
		"%5.2f %5.2f %5.2f;\n"
		"Forward vector: %5.2f %5.2f %5.2f;\n"
		"Friction: %5.2f;\n"
		"Forward pl: %5.2f %5.2f %5.2f;\n"
		"Velocity pl: %5.2f %5.2f %5.2f;\n",
		
		camera->getTranslation().x, camera->getTranslation().y, camera->getTranslation().z,
		camera->getForwardVector().x, camera->getForwardVector().y, camera->getForwardVector().z,
		camera->getFriction(),
		forward.x, forward.y, forward.z,
		velocity.x, velocity.y, velocity.z
	);
}

void MainState::playerControl(float elapsed)
{
	SceneObject *player = object.get(playerIndex);
	
#ifdef TEST_MOVEMENT_BASED_ON_THE_TIME
	if (ENGINE->keyPress(DIK_NUMPAD8, true))
		player->addVelocity(player->getForwardVector() * speedPlayer);
	if (ENGINE->keyPress(DIK_NUMPAD2, true))
		player->addVelocity(-player->getForwardVector() * speedPlayer);
	if (ENGINE->keyPress(DIK_NUMPAD4, true))
		player->addVelocity(-player->getRightVector() * speedPlayer);
	if (ENGINE->keyPress(DIK_NUMPAD6, true))
		player->addVelocity(player->getRightVector() * speedPlayer);

	if (ENGINE->keyPress(DIK_NUMPAD7, true))
		player->addSpin(0, -PI / 4 * elapsed * 15, 0);
	if (ENGINE->keyPress(DIK_NUMPAD9, true))
		player->addSpin(0, PI/4 * elapsed * 15, 0);
#else
	if (ENGINE->keyPress(DIK_NUMPAD8, false))
		player->addTranslation(player->getForwardVector() * speedPlayer);
	if (ENGINE->keyPress(DIK_NUMPAD2, false))
		player->addTranslation(-player->getForwardVector() * speedPlayer);
	if (ENGINE->keyPress(DIK_NUMPAD4, false))
		player->addTranslation(-player->getRightVector() * speedPlayer);
	if (ENGINE->keyPress(DIK_NUMPAD6, false))
		player->addTranslation(player->getRightVector() * speedPlayer);

	if (ENGINE->keyPress(DIK_NUMPAD7, false))
		player->addRotation(0, -45, 0);


	if (ENGINE->keyPress(DIK_NUMPAD9, false))
		player->addRotation(0, 45, 0);
#endif
}
