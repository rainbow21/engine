#include "scenestate.h"

SceneState::SceneState(unsigned long id) :State(id)
{
	m_id = id;
}

void SceneState::load()
{
	// ������� ������������� ������ � ������������� ���� �� ��� ��������
	m_man = new AnimatedObject("Marine.x", "./Assets/");
	m_man->setTranslation(-100.0f, 0.0f, 0.0f);
	m_man->setRotation(0.0f, 3.14f, 0.0f);
	m_man->playAnimation(3, 0.0f);

	// ������ �����. ������.
	m_viewer = new SceneObject;
	m_viewer->setTranslation(0.0f, 0.0f, -400.0f);
	m_viewer->setFriction(1.0f);

	// ������� ������ �����
	m_spawner = new SpawnerObject("Gun1.txt", "./Assets/");
	m_spawner->setTranslation(100.0f, -50.0f, 0.0f);
}

void SceneState::close()
{
	SAFE_DELETE(m_man);
	SAFE_DELETE(m_viewer);
	SAFE_DELETE(m_spawner);
}

void SceneState::update(float elapsed)
{
	if (g_engine->getInput()->getKeysPress(DIK_W, true))
		m_viewer->drive(1000.0f *elapsed, false);
	if (g_engine->getInput()->getKeysPress(DIK_S, true))
		m_viewer->drive(-1000.0f * elapsed, false);
	if (g_engine->getInput()->getKeysPress(DIK_Q, true))
		PostQuitMessage(0);

	// ���������� �������� ������ �� ����������� ����
	m_viewer->addRotation(
		(float)g_engine->getInput()->getDeltaX()*elapsed,
		(float)g_engine->getInput()->getDeltaY()*elapsed,
		0.0f
	);

	m_man->update(elapsed);
	m_viewer->update(elapsed);
	m_spawner->update(elapsed);
}

void SceneState::render()
{
	m_man->render();
	m_spawner->render();
}

void SceneState::requestViewer(ViewerSetup *viewer)
{
	viewer->viewer = m_viewer;
	viewer->viewClearFlags = D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER;
}