#ifndef DEVICE_ENUMERATION_H
#define DEVICE_ENUMERATION_H

#include <Windows.h>
#include <d3dx9.h>
#include "scripting.h"
#include "define.h"
#include "resource.h"

// ����� �������
struct DisplayMode
{
	D3DDISPLAYMODE mode;	// ����� ������� Direct3D
	char bpp[6];			// ������� ����������� (������� ���������)
};

// ������������ ���������
class DeviceEnumeration
{

private:
	Script *m_settingsScript;					// ������ � �����������
	D3DADAPTER_IDENTIFIER9 m_adapter;			// ������������� ��������
	LinkedList<DisplayMode> *m_displayModes;	// ������������ ���� ��������� �������
	D3DDISPLAYMODE m_selectedMode;				// ��������� �����
	bool m_windowed;							// ������ �� ���������� ����������� � ������� ������
	bool m_vsync;								// ������������ �������������

	void ComboBoxAdd(HWND dialog, int id, void *data, char *desc);
	void ComboBoxSelect(HWND dialog, int id, int index);
	void ComboBoxSelect(HWND dialog, int id, void *data);
	void *ComboBoxSelected(HWND dialog, int id);
	bool ComboBoxSomethingSelected(HWND dialog, int id);
	int ComboBoxCount(HWND dialog, int id);
	bool ComboBoxContainsText(HWND dialog, int id, char *text);

public:
	INT_PTR enumerate(IDirect3D9 *d3d);
	INT_PTR settingsDialogProc(HWND dialog, UINT uiMsg, WPARAM wParam, LPARAM lParam);

	D3DDISPLAYMODE getSelectedDisplayMode();
	bool isWindowed();		// ������� ��� Full Screen �����
	bool isVSynced();		// true - ������� ���������� ����� = ������� ���������� ��������
};

// ������� ���������
extern DeviceEnumeration *g_deviceEnumeration;

#endif
