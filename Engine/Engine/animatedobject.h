// ������� �����, �������������� �������� �����
#ifndef ANIMATED_OBJECT_H
#define ANIMATED_OBJECT_H
#include "sceneobject.h"


class AnimatedObject :public SceneObject, public ID3DXAnimationCallbackHandler
{
private:
	ID3DXAnimationController *m_animationController;	// ���������� ���������� ������������ ��������
	unsigned int m_currentTrack;						// ����, �� ������� � ������ ������ ������������� ��������
	float m_currentTime;								// ������ ��� ��������

	virtual HRESULT CALLBACK HandleCallback(THIS_ UINT Track, LPVOID pCallbcakData);

public:
	AnimatedObject(char *meshName, char *meshPath = "./", unsigned long type = TYPE_ANIMATED_OBJECT);
	virtual ~AnimatedObject();

	virtual void update(float elapsed, bool addVelocity = true);
	void playAnimation(unsigned int animation, float transitionTime, bool loop = true);
	ID3DXAnimationController *getAnimationController();

};
#endif