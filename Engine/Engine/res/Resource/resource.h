#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDD_GRAPHICS_SETTINGS                   100
#define IDC_DISPLAY_ADAPTER                     40000
#define IDC_DRIVER_VERSION                      40001
#define IDC_WINDOWED                            40002
#define IDC_FULLSCREEN                          40003
#define IDC_VSYNC                               40004
#define IDC_COLOUR_DEPTH                        40005
#define IDC_RESOLUTION                          40006
#define IDC_REFRESH_RATE                        40007
