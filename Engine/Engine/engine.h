/*
	������� ������������ ���� ������. ���� ���� ��������� ������ �������. 
	�������� ������������ ����������, ������� ���������� ����� �������� � ���� ������.
*/
#ifndef ENGINE_H
#define ENGINE_H

// System includes
#include <stdio.h>
#include <tchar.h>
#include <windowsx.h>
#include <direct.h>

// ������������� 8 ������ DirectInput (��� ����������� ������)
#define DIRECTINPUT_VERSION 0x0800

// DirectX includes
#include <d3dx9.h>
#include <dinput.h>

// �������
#include "delete.h"

// Engine includes
#include "linkedList.h"
#include "resourcemanagement.h"
#include "geometry.h"
#include "font.h"
#include "scripting.h"
#include "input.h"
#include "boundingvolume.h"
#include "material.h"
#include "mesh.h"
#include "sceneobject.h"
#include "animatedobject.h"
#include "spawnerobject.h"
#include "rendercache.h"
#include "state.h"
#include "deviceenumeration.h"
#include "label.h"

#include <iostream>

// Engine setup stricture
struct EngineSetup
{
	HINSTANCE instance;		// ���������� ���������� ����������
	char *name;				// �������� ����������
	float scale;			// �������. ������� ����� 1 ����
	unsigned char totalBackBuffers;	// ����� ������������ ���-��������
	void(*stateSetup)();	// ������� ���������� ������
	void(*createMaterialResource)(Material **resource, char *name, char *path);	// ������� �������� ���������
	char *spawnerPath;		// ���� ��� ����� �������� ������
	EngineSetup()
	{
		instance = NULL;
		name = "Application";
		scale = 1.0f;			// 1 Unit = 1 meter
		totalBackBuffers = 1;
		stateSetup = NULL;
		createMaterialResource = NULL;

	}
};


class Engine
{
private:
	bool m_loaded;					// �������� �� ������ ��� ���
	HWND m_window;					// ���������� �������� ����
	bool m_deactive;				// ������� ���������� ��� ��� 
	EngineSetup *m_setup;			// ����� ��������������� ���������
	IDirect3DDevice9 *m_device;		// ��������� ����������
	D3DDISPLAYMODE m_displayMode;	// ����� ������� 
	ID3DXSprite *m_sprite;			// ��������� ������� ��� ����������
	unsigned char m_currentBackBuffer;	// ���������� ���-��������

	LinkedList<State> *m_states;
	State *m_currentState;	// ������� �����
	bool m_stateChanged;	// ������� �� ����� � ������� �����
	Input *m_input;
	ResourceManager<Script>		*m_scriptManager;		// �������� ��������
	ResourceManager<Material>	*m_materialManager;		// �������� ����������
	ResourceManager<Mesh>		*m_meshManager;			// �������� �����

public:
	Engine(EngineSetup *setup = NULL);
	virtual ~Engine();

	void run();

	HWND getWindow();
	void setDeactiveFlag(bool deactive);

	float getScale();
	void  setScale(float s);
	IDirect3DDevice9*	getDevice();
	D3DDISPLAYMODE*		getDisplayMode();
	ID3DXSprite*		getSprite();

	void addState(State *state, bool change = true);
	void removeState(State *state);
	void changeState(unsigned long id);
	State *getCurrentState();
	Input* getInput();
	ResourceManager<Script>		*getScriptManager();
	ResourceManager<Material>	*getMaterialManager();
	ResourceManager<Mesh>		*getMeshManager();

	// ��������� � ����������� �����
	bool keyPress(char key, bool ignorePressStamp = false);
	bool mouseClick(char button, bool ignorePressStamp = false);
	long mousePosX();
	long mousePosY();
	long mouseDeltaX();
	long mouseDeltaY();
	long mouseDeltaWheel();


	int test()
	{
		char buf[100];
		_getcwd(buf, 100);

		char *filename = "script.txt";
		m_scriptManager->add(filename);
		Script *s = m_scriptManager->getElement(filename);
		int a = *s->getNumberData("a");
		double b = *s->getFloatData("b");
		Vector3 v = *s->getVectorData("c");
	

		return 0;
	}
}; 

// Externals
extern Engine *g_engine;
// ��� ����� �������� ���������
#define ENGINE g_engine

#endif