// ��������� ��������������� ������. ��������� + ��������� �����
#ifndef RENDERCACHE_H
#define RENDERCACHE_H

#include <d3d9.h>
#include "material.h"
#include "delete.h"

class RenderCache
{
private:
	IDirect3DDevice9		*m_device;		// ���������� Direct3D
	Material				*m_material;	// ��������, ������������ ������ �����
		
	IDirect3DIndexBuffer9	*m_indexBuffer;	// ��������� �����, ����������� �� �������, ���������� ����������
	unsigned short			*m_indexPointer;// ��������� ��� ������� � ���������� ������ �� ������ ������� � ������� 
	unsigned long			m_totalIndices;	// ����� ��������, �������������� ������ ��������� �������
	unsigned long			m_faces;		// ����� ������, ����������� � ����������

	unsigned long m_totalVertices;			// ����� ������
public:
	RenderCache(IDirect3DDevice9 *device, Material *material);
	virtual ~RenderCache();

	void addFace();
	void prepare(unsigned long totalVertices);

	void begin();
	void renderFace(unsigned short vertex0, unsigned short vertex1, unsigned short vertex2);
	void end();

	Material *getMaterial();
};

#endif
