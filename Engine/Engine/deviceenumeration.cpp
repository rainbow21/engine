#include "deviceenumeration.h"

DeviceEnumeration *g_deviceEnumeration = NULL;

// "��������" ��� "�����������" ��� ����������� ��������� ����������� ���� ��������
BOOL CALLBACK SettingsDialogProcDirector(HWND hDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	return g_deviceEnumeration->settingsDialogProc(hDlg, uiMsg, wParam, lParam);
}
#include "engine.h"
// ��� ��������� ���������� Direct3D �� �������� �������
// ��������� ����������� ������
INT_PTR DeviceEnumeration::enumerate(IDirect3D9 *d3d)
{
	// ������� ������� ������ ������������
	m_displayModes = new LinkedList<DisplayMode>;

	// ��������� ������ � ����������� ������
	m_settingsScript = new Script("display.txt");

	// �������� ���������� �� �������� 
	d3d->GetAdapterIdentifier(D3DADAPTER_DEFAULT, 0, &m_adapter);

	// ������ ������ ���������� �������� �������
	D3DFORMAT allowedFormats[6];
	allowedFormats[0] = D3DFMT_X1R5G5B5;
	allowedFormats[1] = D3DFMT_A1R5G5B5;
	allowedFormats[2] = D3DFMT_R5G6B5;
	allowedFormats[3] = D3DFMT_X8R8G8B8;
	allowedFormats[4] = D3DFMT_A8R8G8B8;
	allowedFormats[5] = D3DFMT_A2R10G10B10;


	for (__int8 af = 0; af < 6; af++)
	{
		unsigned long totalAdapterModes = d3d->GetAdapterModeCount(D3DADAPTER_DEFAULT, allowedFormats[af]);
		for (unsigned long m = 0; m < totalAdapterModes; m++)
		{
			// ����������� � ������ �����������
			D3DDISPLAYMODE mode;
			d3d->EnumAdapterModes(D3DADAPTER_DEFAULT, allowedFormats[af], m, &mode);

			// ��������� ����������� � ������ �����������
			if (mode.Height < 480)
				continue;

			// ������� ����� �����
			DisplayMode *displayMode = new DisplayMode;
			memcpy(&displayMode->mode, &mode, sizeof(D3DDISPLAYMODE));
			if (af < 3)
				strcpy(displayMode->bpp, "16 bpp");
			else
				strcpy(displayMode->bpp, "32 bpp");

			m_displayModes->add(displayMode);
		}
	}

	return DialogBox(NULL, MAKEINTRESOURCE(IDD_GRAPHICS_SETTINGS), NULL, SettingsDialogProcDirector);
}

// ��������� ������� ��������� ���� ����������� ��������
INT_PTR DeviceEnumeration::settingsDialogProc(HWND dialog, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
	case WM_INITDIALOG:
	{
		char version[16];
		sprintf(version, "%d", LOWORD(m_adapter.DriverVersion.LowPart));
		Edit_SetText(GetDlgItem(dialog, IDC_DISPLAY_ADAPTER), m_adapter.Description);
		Edit_SetText(GetDlgItem(dialog, IDC_DRIVER_VERSION), version);

		// ���� �� � ������� ����������� ����������
		if (m_settingsScript->getBoolData(SCNAME_WINDOWED) == NULL)
		{
			CheckDlgButton(dialog, IDC_WINDOWED, m_windowed = true);
		}
		else
		{
			// ��������� ����� �������� ������
			CheckDlgButton(dialog, IDC_WINDOWED, m_windowed = *m_settingsScript->getBoolData("windowed"));
			CheckDlgButton(dialog, IDC_FULLSCREEN, !m_windowed);
			// ����� ������ Full Screen
			if (!m_windowed)
			{
				// �������� �������� ����������, ����������� � Full Screen
				EnableWindow(GetDlgItem(dialog, IDC_VSYNC), true);
				EnableWindow(GetDlgItem(dialog, IDC_COLOUR_DEPTH), true);
				EnableWindow(GetDlgItem(dialog, IDC_RESOLUTION), true);
				EnableWindow(GetDlgItem(dialog, IDC_REFRESH_RATE), true);

				// ���� v-sync
				CheckDlgButton(dialog, IDC_VSYNC, m_vsync = *m_settingsScript->getBoolData("vsync"));

				// �����-���� ������� �������
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_COLOUR_DEPTH));
				m_displayModes->iterate(true);
				while (m_displayModes->iterate())
					if (!ComboBoxContainsText(dialog, IDC_COLOUR_DEPTH, m_displayModes->getCurrent()->bpp))
						ComboBoxAdd(dialog, IDC_COLOUR_DEPTH, (void*)m_displayModes->getCurrent()->mode.Format, m_displayModes->getCurrent()->bpp);
				ComboBoxSelect(dialog, IDC_COLOUR_DEPTH, *m_settingsScript->getNumberData(SCNAME_BPP));

				char text[16];
				// ��������� Combo-Box ��������� ����������
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_RESOLUTION));
				m_displayModes->iterate(true);
				while (m_displayModes->iterate())
				{
					if (m_displayModes->getCurrent()->mode.Format == (D3DFORMAT)PtrToUlong(ComboBoxSelected(dialog, IDC_COLOUR_DEPTH)))
					{
						D3DDISPLAYMODE mode = m_displayModes->getCurrent()->mode;
						sprintf(text, "%d x %d", mode.Width, mode.Height);
						if (!ComboBoxContainsText(dialog, IDC_RESOLUTION, text))
							ComboBoxAdd(dialog, IDC_RESOLUTION, (void*)MAKELONG(mode.Width, mode.Height), text);
					}
				}
				ComboBoxSelect(dialog, IDC_RESOLUTION, *m_settingsScript->getNumberData(SCNAME_RESOLUTION));

				// ��������� �����-���� ������� ������
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_REFRESH_RATE));
				m_displayModes->iterate(true);
				while (m_displayModes->iterate())
				{
					DisplayMode *mode = m_displayModes->getCurrent();
					if ((DWORD)MAKELONG(mode->mode.Width, mode->mode.Height) == (DWORD)PtrToUlong(ComboBoxSelected(dialog, IDC_RESOLUTION)))
					{
						sprintf(text, "%d Hz", mode->mode.RefreshRate);
						if (!ComboBoxContainsText(dialog, IDC_REFRESH_RATE, text))
							ComboBoxAdd(dialog, IDC_REFRESH_RATE, (void*)mode->mode.RefreshRate, text);
					}
				}
				ComboBoxSelect(dialog, IDC_REFRESH_RATE, *m_settingsScript->getNumberData(SCNAME_REFRESH));
			}
		}
		return true;
	}

	case WM_COMMAND:
	{
		switch (LOWORD(wparam))
		{
		case IDOK:
		{
			m_selectedMode.Width = LOWORD(PtrToUlong(ComboBoxSelected(dialog, IDC_RESOLUTION)));
			m_selectedMode.Height = HIWORD(PtrToUlong(ComboBoxSelected(dialog, IDC_RESOLUTION)));
			m_selectedMode.RefreshRate = PtrToUlong(ComboBoxSelected(dialog, IDC_REFRESH_RATE));
			m_selectedMode.Format = (D3DFORMAT)PtrToUlong(ComboBoxSelected(dialog, IDC_COLOUR_DEPTH));
			m_windowed = IsDlgButtonChecked(dialog, IDC_WINDOWED);
			m_vsync = IsDlgButtonChecked(dialog, IDC_VSYNC);
			

			// ���������� ������ ������� 
			SAFE_DELETE(m_displayModes);

			// �������� �������� �� �����-������
			long bpp = ComboBox_GetCurSel(GetDlgItem(dialog, IDC_COLOUR_DEPTH));
			long resolution = ComboBox_GetCurSel(GetDlgItem(dialog, IDC_RESOLUTION));
			long refresh = ComboBox_GetCurSel(GetDlgItem(dialog, IDC_RESOLUTION));

			// ���� �� � ������� �����-�� �� ����������
			if (!m_settingsScript->getBoolData(SCNAME_WINDOWED))
			{
				// ��������� ��� ��������� � ������
				m_settingsScript->addVariable(SCNAME_WINDOWED, VARIABLE_BOOL, &m_windowed);
				m_settingsScript->addVariable(SCNAME_VSYNC, VARIABLE_BOOL, &m_vsync);
				m_settingsScript->addVariable(SCNAME_BPP, VARIABLE_NUMBER, &bpp);
				m_settingsScript->addVariable(SCNAME_RESOLUTION, VARIABLE_NUMBER, &resolution);
				m_settingsScript->addVariable(SCNAME_REFRESH, VARIABLE_NUMBER, &refresh);
			}
			else
			{
				// ������������� ����� ���������
				m_settingsScript->setVariable(SCNAME_WINDOWED, &m_windowed);
				m_settingsScript->setVariable(SCNAME_VSYNC, &m_vsync);
				m_settingsScript->setVariable(SCNAME_BPP, &bpp);
				m_settingsScript->setVariable(SCNAME_RESOLUTION, &resolution);
				m_settingsScript->setVariable(SCNAME_REFRESH, &refresh);
			}

			// ��������� ��� ��������� � ���� ������� ����������� ��������
			m_settingsScript->save();
			// � ������� ��� �� ��
			SAFE_DELETE(m_settingsScript);

			EndDialog(dialog, IDOK);
			return true;
		}
		case IDCANCEL:
		{
			SAFE_DELETE(m_displayModes);
			SAFE_DELETE(m_settingsScript);
			EndDialog(dialog, IDCANCEL);
			return true;
		}
		case IDC_COLOUR_DEPTH:
		{
			if (CBN_SELCHANGE == HIWORD(wparam))
			{
				char res[16];
				DWORD selectedRes = (DWORD)PtrToUlong(ComboBoxSelected(dialog, IDC_RESOLUTION));
				// ��������� �����-���� ������ ���������� ������
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_RESOLUTION));
				m_displayModes->iterate(true);
				while (m_displayModes->iterate())
				{
					DisplayMode *mode = m_displayModes->getCurrent();
					if (mode->mode.Format == (D3DFORMAT)PtrToUlong(ComboBoxSelected(dialog, IDC_COLOUR_DEPTH)))
					{
						sprintf(res, "%d x %d", mode->mode.Width, mode->mode.Height);
						if (!ComboBoxContainsText(dialog, IDC_RESOLUTION, res))
						{
							ComboBoxAdd(dialog, IDC_RESOLUTION, (void*)MAKELONG(mode->mode.Width, mode->mode.Height), res);
							if (selectedRes == (DWORD)MAKELONG(mode->mode.Width, mode->mode.Height))
								ComboBoxSelect(dialog, IDC_RESOLUTION, (void*)selectedRes);
						}
					}
				}
				if (!ComboBoxSelected(dialog, IDC_RESOLUTION))
					ComboBoxSelect(dialog, IDC_RESOLUTION, 0);
			}
			return true;
		}
		case IDC_RESOLUTION:
		{
			if (CBN_SELCHANGE == HIWORD(wparam))
			{
				char refresh[16];
				DWORD selectedRefresh = (DWORD)PtrToUlong(ComboBoxSelected(dialog, IDC_REFRESH_RATE));

				// ��������� �����-��� ������ ������� ����������
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_REFRESH_RATE));
				m_displayModes->iterate(true);
				while (m_displayModes->iterate())
				{
					DisplayMode *mode = m_displayModes->getCurrent();
					if ((DWORD)MAKELONG(mode->mode.Width, mode->mode.Height) == (DWORD)PtrToUlong(ComboBoxSelected(dialog, IDC_RESOLUTION)))
					{
						sprintf(refresh, "%d Hz", mode->mode.RefreshRate);
						if (!ComboBoxContainsText(dialog, IDC_REFRESH_RATE, refresh))
						{
							ComboBoxAdd(dialog, IDC_REFRESH_RATE, (void*)mode->mode.RefreshRate, refresh);
							if (selectedRefresh == mode->mode.RefreshRate)
								ComboBoxSelect(dialog, IDC_REFRESH_RATE, (void*)selectedRefresh);
						}
					}
				}
				if (!ComboBoxSelected(dialog, IDC_REFRESH_RATE))
					ComboBoxSelect(dialog, IDC_REFRESH_RATE, 0);
			}
			return true;
		}
		case IDC_WINDOWED:
		case IDC_FULLSCREEN:
		{
			// ���������� �� �������� ����� �������������� ������
			if (IsDlgButtonChecked(dialog, IDC_WINDOWED))
			{
				// �������� � ������� ������������ ��� �������� ���������� Full Screen
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_COLOUR_DEPTH));
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_RESOLUTION));
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_REFRESH_RATE));
				CheckDlgButton(dialog, IDC_VSYNC, false);
				EnableWindow(GetDlgItem(dialog, IDC_COLOUR_DEPTH), false);
				EnableWindow(GetDlgItem(dialog, IDC_RESOLUTION), false);
				EnableWindow(GetDlgItem(dialog, IDC_REFRESH_RATE), false);
				EnableWindow(GetDlgItem(dialog, IDC_VSYNC), false);
			}
			else
			{
				// ������������ �������� ����������
				EnableWindow(GetDlgItem(dialog, IDC_VSYNC), true);
				EnableWindow(GetDlgItem(dialog, IDC_COLOUR_DEPTH), true);
				EnableWindow(GetDlgItem(dialog, IDC_RESOLUTION), true);
				EnableWindow(GetDlgItem(dialog, IDC_REFRESH_RATE), true);
				// ��������� �����-���� � ��������� ��������� ������
				ComboBox_ResetContent(GetDlgItem(dialog, IDC_COLOUR_DEPTH));
				m_displayModes->iterate(true);
				while (m_displayModes->iterate())
				{
					DisplayMode *mode = m_displayModes->getCurrent();
					if (!ComboBoxContainsText(dialog, IDC_COLOUR_DEPTH, mode->bpp))
						ComboBoxAdd(dialog, IDC_COLOUR_DEPTH, (void*)mode->mode.Format, mode->bpp);
				}
				ComboBoxSelect(dialog, IDC_COLOUR_DEPTH, 0);
			}
			return true;
		}
		}
	}
	}
	return false;
}

// ��������� ����� �������
D3DDISPLAYMODE DeviceEnumeration::getSelectedDisplayMode()
{
	return m_selectedMode;
}

// ���� ��� Full Screen
bool DeviceEnumeration::isWindowed()
{
	return m_windowed;
}
// ������������ �������������
bool DeviceEnumeration::isVSynced()
{
	return m_vsync;
}

// �������� ������ � �����-����
void DeviceEnumeration::ComboBoxAdd(HWND dialog, int id, void *data, char *desc)
{
	HWND control = GetDlgItem(dialog, id);
	int i = ComboBox_AddString(control, desc);
	ComboBox_SetItemData(control, i, data);
}

// ������� ������ � �����-����� �� �������
void DeviceEnumeration::ComboBoxSelect(HWND dialog, int id, int index)
{
	HWND control = GetDlgItem(dialog, id);
	ComboBox_SetCurSel(control, index);
	PostMessage(dialog, WM_COMMAND, MAKEWPARAM(id, CBN_SELCHANGE), (LPARAM)control);
}

// ������� ������ �� ������������ ������
void DeviceEnumeration::ComboBoxSelect(HWND dialog, int id, void *data)
{
	HWND control = GetDlgItem(dialog, id);
	int n = ComboBoxCount(dialog, id);
	for (int i = 0; i < n; i++)
		if ((void*)ComboBox_GetItemData(control, i) == data)
		{
			ComboBox_SetCurSel(control, i);
			PostMessage(dialog, WM_COMMAND, MAKEWPARAM(id, CBN_SELCHANGE), (LPARAM)control);
			break;
		}
}

// ������ ��������� ������
void* DeviceEnumeration::ComboBoxSelected(HWND dialog, int id)
{
	HWND control = GetDlgItem(dialog, id);
	int index = ComboBox_GetCurSel(control);
	if (index < 0)
		return NULL;
	return (void*)ComboBox_GetItemData(control, index);
}

// ��������� �� ������, ��������� � �����-�����
bool DeviceEnumeration::ComboBoxSomethingSelected(HWND dialog, int id)
{
	HWND control = GetDlgItem(dialog, id);
	int index = ComboBox_GetCurSel(control);
	return (index >= 0);
}

// ���������� ������� � �����-�����
int DeviceEnumeration::ComboBoxCount(HWND dialog, int id)
{
	HWND control = GetDlgItem(dialog, id);
	return ComboBox_GetCount(control);
}

// ���������� �� ��������� ������ � �����-�����
bool DeviceEnumeration::ComboBoxContainsText(HWND dialog, int id, char *text)
{
	char item[MAX_PATH];
	HWND control = GetDlgItem(dialog, id);
	int n = ComboBoxCount(dialog, id);
	for (int i = 0; i < n; i++)
	{
		ComboBox_GetLBText(control, i, item);
		if (!lstrcmp(item, text))
			return true;
	}
	return false;
}