// ������� ������ �����. ����������� ������� �����������
#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "boundingvolume.h"
#include "vector.h"
#include "matrix.h"
#include "mesh.h"
#include "objecttype.h"


class SceneObject :public BoundingVolume
{
private:
public:
	SceneObject(unsigned long type = TYPE_SCENE_OBJECT, char *meshName = NULL, char *mashPath = "./", bool sharedMesh = true);
	virtual ~SceneObject();

	virtual void update(float elapsed, bool addVelocity = true);
	virtual void render(Matrix4x4 *world = NULL);

	virtual void collisionOccurred(SceneObject *object, unsigned long collisionStamp);

	void drive	(float force, bool lockYAxis = true);
	void strafe	(float force, bool lockYAxis = true);
	void stop();

	void setTranslation(float x, float y, float z);
	void setTranslation(Vector3 translation);
	void addTranslation(float x, float y, float z);
	void addTranslation(Vector3 translation);
	Vector3 getTranslation();

	void setRotation(float x, float y, float z);
	void setRotation(Vector3 rotation);
	void addRotation(float x, float y, float z);
	void addRotation(Vector3 rotation);
	Vector3 getRotation();

	void setVelocity(float x, float y, float z);
	void setVelocity(Vector3 velocity);
	void addVelocity(float x, float y, float z);
	void addVelocity(Vector3 velocity);
	Vector3 getVelocity();

	void setSpin(float x, float y, float z);
	void setSpin(Vector3 spin);
	void addSpin(float x, float y, float z);
	void addSpin(Vector3 spin);
	Vector3 getSpin();

	Vector3 getForwardVector();
	Vector3 getRightVector();
	void setForwardVector(Vector3 v);
	void setRightVector(Vector3 v);

	Matrix4x4 *getTranslationMatrix();
	Matrix4x4 *getRotationMatrix();
	Matrix4x4 *getWorldMatrix();
	Matrix4x4 *getViewMatrix();

	void setType(unsigned long type);
	unsigned long getType();

	void setFriction(float friction);
	float getFriction();
	unsigned long getCollisionStamp();

	void setVisible(bool visible);
	bool isVisible();

	void setEnabled(bool enabled);
	bool isEnabled();

	void setGhost(bool ghost);
	bool isGhost();

	void setIgnoreCollisions(bool ignoreCollisions);
	bool isIgnoreCollisions();

	void setTouchingGroundFlag(bool touchingGround);
	bool isTouchingGround();

	void setMesh(char *meshName = NULL, char *meshPath = "./", bool sharedMesh = true);
	Mesh* getMesh();

protected:
	Vector3		m_forward;			// ������ �������, ������������ ������
	Vector3		m_right;			// ������ �������, ������������ ������

	Matrix4x4	m_worldMatrix;		// ������� �������
	Matrix4x4	m_viewMatrix;		// ������� ����

protected:
	Vector3		m_translation;		// ����������, �����������
	Vector3		m_rotation;			// ��������	rad
	Vector3		m_velocity;			// �������� unit/s
	Vector3		m_spin;				// �������� rad/s

	Matrix4x4	m_translationMatrix;// ������� ����������� ������� �������
	Matrix4x4	m_rotationMatrix;	// ������� ��������

	unsigned long m_type;			// �������������� ������������ ����� ������� �����
	float m_friction;				// ����������� ������ (���������), ����������� � ��������� �������� � ��������
	unsigned long m_collisionStamp;	// �������� ��������� ����, � ������� ��������� ������������
	bool m_visible;					// ��������� �������. ��������� ��� ���
	bool m_enabled;					// "���������" ������� � �����. ��������� ��� ���
	bool m_ghost;					// ���� "��������". �������-�������� �������������� � ����������
	bool m_ignoreCollisions;		// ������������� ������������. ������������ �� ����� ����������������
	bool m_touchingGround;			// ������� ����
	bool m_sharedMesh;				// ������������� ���� ��������� � ������� ��������� ��� �����������

	Mesh *m_mesh;					// ��������� �� 3D ���
};


#endif