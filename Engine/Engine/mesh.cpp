#include "mesh.h"
#include "engine.h"
HRESULT AllocateHierarchy::CreateFrame(THIS_ LPCSTR Name, LPD3DXFRAME *ppnNewFrame)
{
	// ������� ����� ������ � �������� ���
	Frame *frame = new Frame;
	ZeroMemory(frame, sizeof(Frame));

	// �������� ��� �����
	if (!Name)
	{
		// ��� �����������, ������ ������� ���������� ���
		static unsigned long nameCount = 0;
		char newName[32];
		sprintf(newName, "unkown_frame_%d", nameCount);
		nameCount++;

		frame->Name = new char[strlen(newName) + 1];
		strcpy(frame->Name, newName);
	}
	else
	{
		frame->Name = new char[strlen(Name) + 1];
		strcpy(frame->Name, Name);
	}

	*ppnNewFrame = frame;
	return S_OK;
}

HRESULT AllocateHierarchy::CreateMeshContainer(
	THIS_ LPCSTR Name, CONST D3DXMESHDATA *pMeshData, CONST D3DXMATERIAL *pMaterials, CONST D3DXEFFECTINSTANCE *pEffectInstances,
	DWORD NumMaterials, CONST DWORD *pAdjacency, LPD3DXSKININFO pSkinInfo, LPD3DXMESHCONTAINER *ppNewMeshContainer)
{
	// ������� ����� ���-��������� � ��������
	MeshContainer *meshContainer = new MeshContainer;
	ZeroMemory(meshContainer, sizeof(MeshContainer));

	// �������� ��� ����
	if (!Name)
	{
		// ���� ��� �����������, ������� ����������
		static unsigned long nameCount = 0;
		char newName[32];
		sprintf(newName, "unkown_mesh_%d", nameCount);
		nameCount++;

		meshContainer->Name = new char[strlen(newName) + 1];
		strcpy(meshContainer->Name, newName);
	}
	else
	{
		meshContainer->Name = new char[strlen(Name) + 1];
		strcpy(meshContainer->Name, Name);
	}

	// ���� �� � ���� �����-���� ���������
	if ((meshContainer->NumMaterials = NumMaterials) > 0)
	{
		int n = meshContainer->NumMaterials;
		meshContainer->materials = new Material*[n];
		meshContainer->materialNames = new char*[n];

		// ��������� ��� ����� ����������
		for (unsigned long m = 0; m < NumMaterials; m++)
		{
			if (pMaterials[m].pTextureFilename)
			{
				meshContainer->materialNames[m] = new char[strlen(pMaterials[m].pTextureFilename) + 1];
				memcpy(meshContainer->materialNames[m], pMaterials[m].pTextureFilename, (strlen(pMaterials[m].pTextureFilename) + 1) * sizeof(char));
			}
			else
				meshContainer->materialNames[m] = NULL;
			meshContainer->materials[m] = NULL;
		}
	}

	// ��������� ���������� � ������� ���������� ����
	meshContainer->pAdjacency = new DWORD[pMeshData->pMesh->GetNumFaces() * 3];
	memcpy(meshContainer->pAdjacency, pAdjacency, sizeof(DWORD)*pMeshData->pMesh->GetNumFaces() * 3);

	// ��������� ������ ����
	meshContainer->MeshData.pMesh = meshContainer->originalMesh = pMeshData->pMesh;
	meshContainer->MeshData.Type = D3DXMESHTYPE_MESH;
	pMeshData->pMesh->AddRef();
	pMeshData->pMesh->AddRef();

	// �������� �� ��� �������������
	if (pSkinInfo != NULL)
	{
		// ��������� ���������� � ����� � � ����
		meshContainer->pSkinInfo = pSkinInfo;
		pSkinInfo->AddRef();

		// ��������� �������� ��� ��� �������� �������������� ����
		meshContainer->originalMesh->CloneMeshFVF(D3DXMESH_MANAGED, meshContainer->originalMesh->GetFVF(), 
			g_engine->getDevice(), &meshContainer->MeshData.pMesh);

		// ��������� ������� ���������
		meshContainer->MeshData.pMesh->GetAttributeTable(NULL, &meshContainer->totalAttributeGroups);
		meshContainer->attributeTable = new D3DXATTRIBUTERANGE[meshContainer->totalAttributeGroups];
		meshContainer->MeshData.pMesh->GetAttributeTable(meshContainer->attributeTable, NULL);
	}

	*ppNewMeshContainer = meshContainer;
	return S_OK;
}

// ���������� �����
HRESULT AllocateHierarchy::DestroyFrame(THIS_ LPD3DXFRAME pFrameToFree)
{
	SAFE_DELETE_ARRAY(pFrameToFree->Name);
	SAFE_DELETE(pFrameToFree);

	return S_OK;
}

// ���������� �����-���������
HRESULT AllocateHierarchy::DestroyMeshContainer(THIS_ LPD3DXMESHCONTAINER pMeshContainerToFree)
{
	MeshContainer *meshContainer = (MeshContainer*)pMeshContainerToFree;

	// ������� ��� ��������� ���� �� ��������� 
	for (unsigned long m = 0; m < meshContainer->NumMaterials; m++)
		if (meshContainer->materials)
			g_engine->getMaterialManager()->remove(&meshContainer->materials[m]);

	// ���������� ���-���������
	SAFE_DELETE_ARRAY(meshContainer->Name);
	SAFE_DELETE_ARRAY(meshContainer->pAdjacency);
	SAFE_DELETE_ARRAY(meshContainer->pMaterials);
	SAFE_DELETE_ARRAY(meshContainer->materialNames);
	SAFE_DELETE_ARRAY(meshContainer->materialNames);
	SAFE_DELETE_ARRAY(meshContainer->boneMatrixPointers);
	SAFE_DELETE_ARRAY(meshContainer->attributeTable);

	SAFE_RELEASE(meshContainer->MeshData.pMesh);
	SAFE_RELEASE(meshContainer->pSkinInfo);
	SAFE_RELEASE(meshContainer->originalMesh);

	return S_OK;
}

Mesh::Mesh(char *name, char *path) :Resource<Mesh>(name, path)
{
	m_frames = new LinkedList<Frame>;
	m_refPoints = new LinkedList<Frame>;

	// ��������� �������� ����. D3DXFRAME - ���� ����� � ������� ������
	AllocateHierarchy ah;
	D3DXLoadMeshHierarchyFromX((LPCSTR)getFilename(), D3DXMESH_MANAGED, g_engine->getDevice(), &ah, NULL,
		(D3DXFRAME**)&m_firstFrame, &m_animationController);

	// ���������� ��������� ��� ����� ��������
	if (m_animationController)
		for (unsigned long t = 0; t < m_animationController->GetMaxNumTracks(); t++)
			m_animationController->SetTrackEnable(t, false);

	// �������� ������� ������������� ������
	m_boneMatrices = NULL;
	m_totalBoneMatrices = 0;

	// �������������� �������� �������
	prepareFrame(m_firstFrame);

	// ������ ��� ������ ������
	m_boneMatrices = new Matrix4x4[m_totalBoneMatrices];

	// ������� ��������� ������ ���
	m_staticMesh = new MeshContainer;
	ZeroMemory(m_staticMesh, sizeof(MeshContainer));

	// ��������� ���
	ID3DXBuffer *materialBuffer, *adjacencyBuffer;
	D3DXLoadMeshFromX(getFilename(), D3DXMESH_MANAGED, g_engine->getDevice(),
		&adjacencyBuffer, &materialBuffer, NULL, &m_staticMesh->NumMaterials, &m_staticMesh->originalMesh);

	// ������������ ��� ��� ������ ������������������ ����������
	m_staticMesh->originalMesh->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE,
		(DWORD*)adjacencyBuffer->GetBufferPointer(), NULL, NULL, NULL);

	// ��������� �������� � ������� ���������� (adjacency)
	SAFE_RELEASE(adjacencyBuffer);

	// ���� �� � ���� ���������
	if (m_staticMesh->NumMaterials)
	{
		m_staticMesh->materials = new Material*[m_staticMesh->NumMaterials];
		// �������� ������ ���������� �� ������ ����������
		D3DXMATERIAL *materials = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();

		// ��������� ������ �������� � ������ ����� �������� ����������
		for (unsigned long m = 0; m < m_staticMesh->NumMaterials; m++)
		{
			// �������� ����� ��������
			if (materials[m].pTextureFilename)
			{
				char *name = new char[strlen(materials[m].pTextureFilename) + 5];
				sprintf(name, "%s.txt", materials[m].pTextureFilename);
				m_staticMesh->materials[m] = g_engine->getMaterialManager()->add(name, getPath());
				SAFE_DELETE_ARRAY(name);
			}
			else
				m_staticMesh->materials[m] = NULL;
		}
	}

	// ������� �������������� �����
	boundingVolumeFromMesh(m_staticMesh->originalMesh);
	// ������ ���������� ������ �� �����
	SAFE_RELEASE(materialBuffer);

	// ������� ������ ������ � ������ �������� � ���
	m_vertices = new Vertex[m_staticMesh->originalMesh->GetNumVertices()];
	m_indices = new unsigned short[m_staticMesh->originalMesh->GetNumFaces() * 3];

	// ���������� ������� ��� ���������� ��������� ����� ������� ������ ���������� ���� �
	// ��������, ����� ����� �� ����� ���� ������������ � ��������� ����� "�� ����"
	Vertex *verticesPtr;
	m_staticMesh->originalMesh->LockVertexBuffer(0, (void**)&verticesPtr);
	unsigned short *indicesPtr;
	m_staticMesh->originalMesh->LockIndexBuffer(0, (void**)&indicesPtr);

	memcpy(m_vertices, verticesPtr, VERTEX_FVF_SIZE*m_staticMesh->originalMesh->GetNumVertices());
	memcpy(m_indices, indicesPtr, sizeof(unsigned short) * m_staticMesh->originalMesh->GetNumFaces() * 3);

	m_staticMesh->originalMesh->UnlockVertexBuffer();
	m_staticMesh->originalMesh->UnlockIndexBuffer();
}

Mesh::~Mesh()
{
	// ���������� �������� �������
	AllocateHierarchy ah;
	D3DXFrameDestroy(m_firstFrame, &ah);

	// ���������� ������ ������� � ������ ������� �����
	m_frames->clearPointers();
	SAFE_DELETE(m_frames);
	m_refPoints->clearPointers();
	SAFE_DELETE(m_refPoints);

	// ����������� ���������� ��������
	SAFE_RELEASE(m_animationController);

	// ���������� ������� ������
	SAFE_DELETE_ARRAY(m_boneMatrices);

	// ���������� ��������� ���
	if (m_staticMesh)
	{
		// ������� ��� ��������
		for (unsigned long m = 0; m < m_staticMesh->NumMaterials; m++)
			if (m_staticMesh->materials)
				g_engine->getMaterialManager()->remove(&m_staticMesh->materials[m]);

		SAFE_DELETE_ARRAY(m_staticMesh->materials);
		SAFE_RELEASE(m_staticMesh->originalMesh);
		SAFE_DELETE(m_staticMesh);
	}

	// ���������� ������� ������ � ��������
	SAFE_DELETE_ARRAY(m_vertices);
	SAFE_DELETE_ARRAY(m_indices);
}

// ����������
void Mesh::update()
{
	updateFrame(m_firstFrame);
}

// ���������
void Mesh::render()
{
	renderFrame(m_firstFrame);
}

// ���� ����������� ��������
void Mesh::cloneAnimationController(ID3DXAnimationController **animationController)
{
	if (m_animationController)
		m_animationController->CloneAnimationController(
			m_animationController->GetMaxNumAnimationOutputs(),
			m_animationController->GetMaxNumAnimationSets(),
			m_animationController->GetMaxNumTracks(),
			m_animationController->GetMaxNumEvents(),
			&*animationController);
	else
		*animationController = NULL;
}

// �������� ��������� ������ ����
MeshContainer *Mesh::getStaticMesh()
{
	return m_staticMesh;
}

// ��������� ������� ����
Vertex* Mesh::getVertices()
{
	return m_vertices;
}

// ���������� ������� ������
unsigned short* Mesh::getIndices()
{
	return m_indices;
}

// ������ �������
LinkedList<Frame>* Mesh::getFrameList()
{
	return m_frames;
}

// ������� ����� �� �����
Frame* Mesh::getFrame(char *name)
{
	m_frames->iterate(true);
	while (m_frames->iterate())
		if (strcmp(m_frames->getCurrent()->Name, name) == 0)
			return m_frames->getCurrent();
	
	return NULL;
}

// ������� ����� � ������� ������
Frame* Mesh::getReferencePoint(char *name)
{
	m_refPoints->iterate(true);
	while (m_refPoints->iterate())
		if (strcmp(m_refPoints->getCurrent()->Name, name) == 0)
			return m_refPoints->getCurrent();

	return NULL;
}

// �������������� ������ �����
void Mesh::prepareFrame(Frame *frame)
{
	m_frames->add(frame);

	// �������� �� �� ������� ������
	if (strncmp("rp_", frame->Name, 3) == 0)
		m_refPoints->add(frame);

	// ������������� �������� ����������
	frame->finalTransformationMatrix = frame->TransformationMatrix;

	// �������������� ���-�������� ������, ���� �� ����
	if (frame->pMeshContainer)
	{
		MeshContainer *meshContainer = (MeshContainer*)frame->pMeshContainer;

		// �������� �� ��� ������������� (� �������)
		if (meshContainer->pSkinInfo)
		{
			meshContainer->boneMatrixPointers = new Matrix4x4*[meshContainer->pSkinInfo->GetNumBones()];

			// ������������� ��������� �� ������� ������������� ������
			for (unsigned long b = 0; b < meshContainer->pSkinInfo->GetNumBones(); b++)
			{
				Frame *bone = (Frame*)D3DXFrameFind(m_firstFrame, meshContainer->pSkinInfo->GetBoneName(b));
				if (!bone)
					continue;
				meshContainer->boneMatrixPointers[b] = &bone->finalTransformationMatrix;
			}

			// ����������� ������� ������ ���������� �� ���� ���-�����������
			if (m_totalBoneMatrices < meshContainer->pSkinInfo->GetNumBones())
				m_totalBoneMatrices = meshContainer->pSkinInfo->GetNumBones();
		}

		// ��������� ���� �� ��������
		if (meshContainer->NumMaterials)
		{
			// ��������� ��� ��������� ����� �������� ����������
			for (unsigned long m = 0; m < meshContainer->NumMaterials; m++)
			{
				if (meshContainer->materialNames[m])
				{
					char *name = new char[strlen(meshContainer->materialNames[m]) + 5];
					sprintf(name, "%s.txt", meshContainer->materialNames[m]);
					meshContainer->materials[m] = g_engine->getMaterialManager()->add(name, getPath());
					SAFE_DELETE_ARRAY(name);
				}
			}
		}
	}

	// �������������� �������������� ������
	if (frame->pFrameSibling)
		prepareFrame((Frame*)frame->pFrameSibling);

	// �������������� �������� (�����������) ������ 
	if (frame->pFrameFirstChild)
		prepareFrame((Frame*)frame->pFrameFirstChild);
}

// ���������� ������� ��������������
void Mesh::updateFrame(Frame *frame, Matrix4x4 *parentTransformationMatrix)
{
	if (parentTransformationMatrix)
		D3DXMatrixMultiply(&frame->finalTransformationMatrix, &frame->TransformationMatrix, parentTransformationMatrix);
	else
		frame->finalTransformationMatrix = frame->TransformationMatrix;

	if (frame->pFrameSibling)
		updateFrame((Frame*)frame->pFrameSibling, parentTransformationMatrix);

	if (frame->pFrameFirstChild)
		updateFrame((Frame*)frame->pFrameFirstChild, &frame->finalTransformationMatrix);
}

// ���������� ���-����������� ������� ������, ���� ��� ����
void Mesh::renderFrame(Frame *frame)
{
	MeshContainer *meshContainer = (MeshContainer*)frame->pMeshContainer;

	// �������� ��� �������� ������, ���� ����
	if (frame->pMeshContainer)
	{
		// ���������, �������� �� �� �������������
		if (meshContainer->pSkinInfo)
		{
			// ������� ������������� ������ � �������������� ������ �������������� ����
			for (unsigned long b = 0; b < meshContainer->pSkinInfo->GetNumBones(); b++)
				D3DXMatrixMultiply(&m_boneMatrices[b], meshContainer->pSkinInfo->GetBoneOffsetMatrix(b),
					meshContainer->boneMatrixPointers[b]);

			// ��������� ������� ���� � ������ ������ ��������������
			PBYTE sourceVertices, destinationVertices;
			meshContainer->originalMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&sourceVertices);
			meshContainer->MeshData.pMesh->LockVertexBuffer(0, (void**)&destinationVertices);
			meshContainer->pSkinInfo->UpdateSkinnedMesh(m_boneMatrices, NULL, sourceVertices, destinationVertices);
			meshContainer->originalMesh->UnlockVertexBuffer();
			meshContainer->originalMesh->UnlockIndexBuffer();

			// �������� ��� �� ������� ���������
			for (unsigned long a = 0; a < meshContainer->totalAttributeGroups; a++)
			{
				g_engine->getDevice()->SetMaterial(meshContainer->materials[meshContainer->attributeTable[a].AttribId]->getLighting());
				g_engine->getDevice()->SetTexture(0, meshContainer->materials[meshContainer->attributeTable[a].AttribId]->getTexture());
				// �������� ������ ������ ������ (a)
				meshContainer->MeshData.pMesh->DrawSubset(meshContainer->attributeTable[a].AttribId);
			}
		}
		else
		{
			// ��� �� ������������� ���, ������� ��� �������� ��� ���������
			for (unsigned long m = 0; m < meshContainer->NumMaterials; m++)
			{
				if (meshContainer->materials[m])
				{
					g_engine->getDevice()->SetMaterial(meshContainer->materials[m]->getLighting());
					g_engine->getDevice()->SetTexture(0, meshContainer->materials[m]->getTexture());
				}
				else
					g_engine->getDevice()->SetTexture(0, NULL);

				meshContainer->MeshData.pMesh->DrawSubset(m);
			}
		}
	}

	// �������� �������������
	if (frame->pFrameSibling)
		renderFrame((Frame*)frame->pFrameSibling);

	// �������� ��������
	if (frame->pFrameFirstChild)
		renderFrame((Frame*)frame->pFrameFirstChild);
}