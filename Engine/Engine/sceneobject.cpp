#include "sceneobject.h"
#include "engine.h"

SceneObject::SceneObject(unsigned long type, char *meshName, char *meshPath, bool sharedMesh)
{
	// ������������� ��� �������
	setType(type);

	// �������� ���������� � ��������
	setTranslation(0.0f, 0.0f, 0.0f);
	setRotation(0.0f, 0.0f, 0.0f);

	// ���������� ������ � ��������� �����
	setVelocity(0.0f, 0.0f, 0.0f);
	setSpin(0.0f, 0.0f, 0.0f);

	// ���������� ������ ������� �� Oz
	m_forward = Vector3(0.0f, 0.0f, 0.0f);
	m_right = Vector3(1.0f, 0.0f, 0.0f);

	// ��������� ������
	m_friction = 0.0f;

	// ������� ������� � ������������
	m_collisionStamp = -1;

	// ������ "�����", "�������", "�����" (�� �������), ������������ ������������
	setVisible(true);
	setEnabled(true);
	setGhost(false);
	setIgnoreCollisions(false);

	// ���������� �� �������� �����
	setTouchingGroundFlag(false);

	// ��������� ����
	m_mesh = NULL;
	setMesh(meshName, meshPath, sharedMesh);
}

SceneObject::~SceneObject()
{
	if (m_sharedMesh)
		g_engine->getMeshManager()->remove(&m_mesh);
	else
		SAFE_DELETE(m_mesh);		
}

// ��������� ��������� �������
void SceneObject::update(float elapsed, bool addVelocity)
{
	// ������������ ������ ��� ������� �������� (���� ����)
	float friction = 1.0f - m_friction * elapsed;
	if (friction < 0)
		friction = 0;

	// ���������� ������
	m_velocity *= friction;
	if (addVelocity)
	{
		Vector3 velocity = m_velocity *elapsed;
		addTranslation(velocity);
	}

	// ������� ������
	m_spin *= friction;
	if (addVelocity)
	{
		Vector3 spin = m_spin *elapsed;
		addRotation(spin);
	}
	// ��������� ������� �������
	D3DXMatrixMultiply(&m_worldMatrix, &m_rotationMatrix, &m_translationMatrix);
	
	// ������� ������� ���� ��� �������. ����� �������� ������� �������
	D3DXMatrixInverse(&m_viewMatrix, NULL, &m_worldMatrix);

	// �������� ������ �������, ������������ ������
	m_forward.x = (float)sin(m_rotation.y);
	m_forward.y = (float)-tan(m_rotation.x);
	m_forward.z = (float)cos(m_rotation.y);
	D3DXVec3Normalize(&m_forward, &m_forward);

	// ��������� ������ �������, ������������ ������
	m_right.x = (float)cos(m_rotation.y);
	m_right.y = (float)tan(m_rotation.z);
	m_right.z = (float)-sin(m_rotation.y);
	D3DXVec3Normalize(&m_right, &m_right);

	// ��������� �������������� �����, � ����������� ������ ������� ����������
	// ��� ��������� ���, ����������� � ��������������� �������������� � ������� ������������
	// ������ ���������� ������������ �������
	// ������� �������, ����� � ����. ��������� ���������, � ������� �������� � ��������� ���������,
	// ����� ���� �������. � ��� ��� ���������� �� ���������
	respositionBoundingVolume(&m_translationMatrix);
}

// ��������� �������
void SceneObject::render(Matrix4x4 *world)
{
	// ���������� ������, ���� � ���� ��� ����
	if (!m_mesh)
		return;
	
	// ���������, ���� �� �������������� ������� �������
	if (!world)
		g_engine->getDevice()->SetTransform(D3DTS_WORLD, &m_worldMatrix);
	else
		g_engine->getDevice()->SetTransform(D3DTS_WORLD, world);

	// �������� ������
	m_mesh->render();
}

// ����������, ���� ���-�� ��������� �������
void SceneObject::collisionOccurred(SceneObject *object, unsigned long collisionStamp)
{
	// ������ ������� � ������������
	m_collisionStamp = collisionStamp;
}

// ���������� ���� � ����������� ������/�����
void SceneObject::drive(float force, bool lockYAxis)
{
	Vector3 realForce = m_forward * force;

	m_velocity.x += realForce.x;
	m_velocity.z += realForce.z;

	if (!lockYAxis)
		m_velocity.y += realForce.y;
}

// ���������� ���� � ����������� ������/�����
void SceneObject::strafe(float force, bool lockYAxis)
{
	Vector3 realForce = m_right * force;
	m_velocity.x += realForce.x;
	m_velocity.z += realForce.z;

	if (!lockYAxis)
		m_velocity.y += realForce.y;
}

// ���������� �������� �������
void SceneObject::stop()
{
	setVelocity(0.0f, 0.0f, 0.0f);
	setSpin(0.0f, 0.0f, 0.0f);
}

// ������������� ����� ����������
void SceneObject::setTranslation(float x, float y, float z)
{
	m_translation.x = x;
	m_translation.y = y;
	m_translation.z = z;

	D3DXMatrixTranslation(&m_translationMatrix, m_translation.x, m_translation.y, m_translation.z);
}
void SceneObject::setTranslation(Vector3 translation)
{
	setTranslation(translation.x, translation.y, translation.z);
}

// ���������� ����� ����������
void SceneObject::addTranslation(float x, float y, float z)
{
	m_translation.x += x;
	m_translation.y += y;
	m_translation.z += z;

	D3DXMatrixTranslation(&m_translationMatrix, m_translation.x, m_translation.y, m_translation.z);
}
void SceneObject::addTranslation(Vector3 translation)
{
	addTranslation(translation.x, translation.y, translation.z);
}

// ������� ����������
Vector3 SceneObject::getTranslation()
{
	return m_translation;
}

// ������������� ��������
void SceneObject::setRotation(float x, float y, float z)
{
	m_rotation.x = x;
	m_rotation.y = y;
	m_rotation.z = z;

	Matrix4x4 rotationX, rotationY;
	D3DXMatrixRotationX(&rotationX, m_rotation.x);
	D3DXMatrixRotationY(&rotationY, m_rotation.y);
	D3DXMatrixRotationZ(&m_rotationMatrix, m_rotation.z);

	D3DXMatrixMultiply(&m_rotationMatrix, &m_rotationMatrix, &rotationX);
	D3DXMatrixMultiply(&m_rotationMatrix, &m_rotationMatrix, &rotationY);
}
void SceneObject::setRotation(Vector3 rotation)
{
	setRotation(rotation.x, rotation.y, rotation.z);
}

// ��������� ��������
void SceneObject::addRotation(float x, float y, float z)
{
	m_rotation.x += x;
	m_rotation.y += y;
	m_rotation.z += z;

	Matrix4x4 rotationX, rotationY;
	D3DXMatrixRotationX(&rotationX, m_rotation.x);
	D3DXMatrixRotationY(&rotationY, m_rotation.y);
	D3DXMatrixRotationZ(&m_rotationMatrix, m_rotation.z);

	D3DXMatrixMultiply(&m_rotationMatrix, &m_rotationMatrix, &rotationX);
	D3DXMatrixMultiply(&m_rotationMatrix, &m_rotationMatrix, &rotationY);
}
void SceneObject::addRotation(Vector3 rotation)
{
	addRotation(rotation.x, rotation.y, rotation.z);
}

// ������� �������� �������
Vector3 SceneObject::getRotation()
{
	return m_rotation;
}

// ���������� �������� ��������
void SceneObject::setVelocity(float x, float y, float z)
{
	m_velocity.x = x;
	m_velocity.y = y;
	m_velocity.z = z;
}
void SceneObject::setVelocity(Vector3 velocity)
{
	m_velocity = velocity;
}

// ��������� ��������
void SceneObject::addVelocity(float x, float y, float z)
{
	m_velocity.x += x;
	m_velocity.y += y;
	m_velocity.z += z;
}
void SceneObject::addVelocity(Vector3 velocity)
{
	m_velocity += velocity;
}

// ������� ��������
Vector3 SceneObject::getVelocity()
{
	return m_velocity;
}

// ���������� ��������
void SceneObject::setSpin(float x, float y, float z)
{
	m_spin.x = x;
	m_spin.y = y;
	m_spin.z = z;
}
void SceneObject::setSpin(Vector3 spin)
{
	m_spin = spin;
}

// ��������� ��������
void SceneObject::addSpin(float x, float y, float z)
{
	m_spin.x += x;
	m_spin.y += y;
	m_spin.z += z;
}
void SceneObject::addSpin(Vector3 spin)
{
	m_spin += spin;
}

// ������� ��������
Vector3 SceneObject::getSpin()
{
	return m_spin;
}

// ������ �������, ������������ ������
Vector3 SceneObject::getForwardVector()
{
	return m_forward;
}

// ������ �������, ������������ ������
Vector3 SceneObject::getRightVector()
{
	return m_right;
}

void SceneObject::setForwardVector(Vector3 v)
{
	m_forward = v;
}

void SceneObject::setRightVector(Vector3 v)
{
	m_right = v;
}

// ������� ������� ���������� 
Matrix4x4*	SceneObject::getTranslationMatrix()
{
	return &m_translationMatrix;
}

// ������� ������� ��������
Matrix4x4*	SceneObject::getRotationMatrix()
{
	return &m_rotationMatrix;
}

// ������� ������� �������
Matrix4x4* SceneObject::getWorldMatrix()
{
	return &m_worldMatrix;
}

// ������� ������� ����
Matrix4x4* SceneObject::getViewMatrix()
{
	return &m_viewMatrix;
}

// ������ ��� �������
void SceneObject::setType(unsigned long type)
{
	m_type = type;
}

// ��� �������
unsigned long SceneObject::getType()
{
	return m_type;
}

// ������ ������
void SceneObject::setFriction(float friction)
{
	m_friction = friction;
}
// ������ ������
float SceneObject::getFriction()
{
	return m_friction;
}

// ������ ������� � ������������
unsigned long SceneObject::getCollisionStamp()
{
	return m_collisionStamp;
}

// ���� ���������
void SceneObject::setVisible(bool visible)
{
	m_visible = visible;
}
bool SceneObject::isVisible()
{
	return m_visible;
}

// ���� "���������" ������� � �����
void SceneObject::setEnabled(bool enabled)
{
	m_enabled = enabled;
}
bool SceneObject::isEnabled()
{
	return m_enabled;
}

// ���� "������������"
void SceneObject::setGhost(bool ghost)
{
	m_ghost = ghost;
}
bool SceneObject::isGhost()
{
	return m_ghost;
}

// ���� ������������� ������������ � ������ ��������
void SceneObject::setIgnoreCollisions(bool ignoreCollisions)
{
	m_ignoreCollisions = ignoreCollisions;
}
bool SceneObject::isIgnoreCollisions()
{
	return m_ignoreCollisions;
}

// ���� ������� ������� � ������
void SceneObject::setTouchingGroundFlag(bool touchingGround)
{
	m_touchingGround = touchingGround;
}
bool SceneObject::isTouchingGround()
{
	return m_touchingGround;
}

// ���������� 3D-���
void SceneObject::setMesh(char *meshName, char *meshPath, bool sharedMesh)
{
	// ���������� ������� ���, ���� ����
	if (m_sharedMesh)
		g_engine->getMeshManager()->remove(&m_mesh);
	else
		SAFE_DELETE(m_mesh);

	// ���� ����������� �������������
	m_sharedMesh = sharedMesh;

	// ���������,��� ��� ������
	if (meshName != NULL && meshPath != NULL)
	{
		// ��������� ��� �������
		if (m_sharedMesh)
			m_mesh = g_engine->getMeshManager()->add(meshName, meshPath);
		else
			m_mesh = new Mesh(meshName, meshPath);
		// ��������� �������������� ����� ����. �� ����� �������������� ��� ������������ ���, 
		// ����������� ����� ��������������� ������ � ������� ������������
		cloneBoundingVolume(m_mesh->getBoundingBox(), m_mesh->getBoundingSphere());
	}
	
	
}

// ��� ������
Mesh* SceneObject::getMesh()
{
	return m_mesh;
}