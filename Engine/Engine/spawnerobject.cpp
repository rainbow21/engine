#include "spawnerobject.h"
#include "engine.h"

SpawnerObject::SpawnerObject(char*name, char *path, unsigned long type) :SceneObject(type)
{
	//���������� ������-����� ��� �������
	setGhost(true);

	// ��������� ������������ ������
 	Script *script = new Script(name, path);

	// ��������� ������� ���������� ������
	m_frequency = *script->getFloatData(SPWNNAME_FREQ);

	// ������� ������ �������-������
	m_spawnerTimer = 0.0f;

	// ��������� ������ �������, ������� ����� �������������� �����
	m_script = g_engine->getScriptManager()
		->add(script->getStringData(SPWNNAME_OBJECT), script->getStringData(SPWNNAME_PATH));

	// �������� ��� ������� ������
	char *n = m_script->getStringData(MDNAME_NAME);
	m_name = new char[strlen(n) + 1];
	strcpy(m_name, n);

	// ��������� ��� "��������������" �������
	setMesh(
		m_script->getStringData(MDNAME_MESH),
		m_script->getStringData(MDNAME_PATH)
	);

	// ������� ������� �������� ��������
	setSpin(0.0f, 1.0f, 0.0f);

	// �������� ������ ������. ���� 0.0, ������ ������ ��������
	// �� �������������� �������
	float radius = *script->getFloatData(SPWNNAME_RADIUS);
	if (fabs(radius) > 0.00001)	// != 0
		setBoundingSphere(Vector3(0.0f, 0.0f, 0.0f), radius);
	else if (getMesh())
		setEllipsoidRadius(*m_script->getVectorData(MDNAME_ELL_RAD));

	// ���������� ������ �������-������
	SAFE_DELETE(script);
}

SpawnerObject::~SpawnerObject()
{
	// ���������� ��� �������������� �������
	SAFE_DELETE_ARRAY(m_name);

	// ���������� ������ "��������������" �������
	g_engine->getScriptManager()->remove(&m_script);
}

// ��������� �����
void SpawnerObject::update(float elapsed, bool addVelocity)
{
	// ����������� ���������� �������� ������
	SceneObject::update(elapsed, addVelocity);

	setSpin(0, 1.0, 0);

	// ������� �� �����. �.�. ������� �� �� �����-�� ����������
	// ���� �� �����, �� ��� ��� �������. ������ ���� �������
	if (!isVisible())
	{
		// ���������� ������� ����� ��������� �������������� ���������� �������
		m_spawnerTimer += elapsed;
		// "������" �������
		if (m_spawnerTimer >= m_frequency)
		{
			setVisible(true);
			setIgnoreCollisions(false);
			m_spawnerTimer = 0.0f;
		}
	}
}

// ������� �� ������������
void SpawnerObject::collisionOccurred(SceneObject *object, unsigned long collisionStamp)
{
	// ��������� �������� ������� ������������� �� ������������
	SceneObject::collisionOccurred(object, collisionStamp);

	// ������ ����� ���������. �� �������� ������ ������ ������������ �����
	setVisible(false);
	setIgnoreCollisions(true);
}

// ������ �������������� �������
Script* SpawnerObject::getObjectScript()
{
	return m_script;
}