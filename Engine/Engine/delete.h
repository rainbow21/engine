#ifndef DELETE_H
#define DELETE_H
#define NULL 0
#define SAFE_DELETE(p)			{if (p) {delete (p);		(p)=NULL;}}
#define SAFE_DELETE_ARRAY(p)	{if (p) {delete[] (p);		(p) = NULL;}}
#define SAFE_RELEASE(p)			{if (p) { (p)->Release();	(p) = NULL;}}
#endif
