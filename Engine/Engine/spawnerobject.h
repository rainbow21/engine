// �����-������. ����������� �� ������� ����� ������ �������
#ifndef SPAWNEROBJECT_H
#define SPAWNEROBJECT_H
#include "sceneobject.h"
#include "define.h"


// �����-�������
class SpawnerObject :public SceneObject
{
private:
	char	*m_name;		// ��� �������, ������� ����� �������� ������ ���������
	float	m_frequency;	// ��� ����� ������� ����� ����������� �������
	float	m_spawnerTimer;	// ������ ��� ������������ ��������
	Script *m_script;		// ������ ������� ��������
public:
	SpawnerObject(char *name, char *path = "./", unsigned long type = TYPE_SPAWNER_OBJECT);
	virtual ~SpawnerObject();

	virtual void update(float elapsed, bool addVelocity = true);
	virtual void collisionOccurred(SceneObject *object, unsigned long collisionStamp);
	Script *getObjectScript();
};


#endif