// �������������� �����
#ifndef BOUNDINGVOLUME_H
#define BOUNDINGVOLUME_H

#include "matrix.h"
#include "vector.h"
#include "delete.h"

// Bounding Box
struct BoundingBox
{
	Vector3 min;	// ����������� ������
	Vector3 max;	// ������������ ������
	float halfSize;		// ���������� �� ������ ������ �� �������� ��������� ����� �� ���� ����
};

// Bounding Sphere
struct BoundingSphere
{
	Vector3 center;
	float radius;
};

// Bounding Volume. ��������� ���� ����� �������������� �������: ���, �����, ���������
class BoundingVolume
{
private:
	BoundingBox		*m_box;		// ������������� ��������������� ������ � ���� ����
	BoundingSphere	*m_sphere;	// � ���� �����

	Vector3 m_originalMin;		// �������� ����������� ������ ����
	Vector3 m_originalMax;		// �������� ������������ ������ ����
	Vector3 m_originalCenter;	// �������� ����� �����

	Vector3 m_ellipsoidRadius;	// ������ ����������
public:
	BoundingVolume();
	virtual ~BoundingVolume();

	void boundingVolumeFromMesh(ID3DXMesh *mesh, Vector3 ellipsoidRadius = Vector3(1.0f, 1.0f, 1.0f));
	void boundingVolumeFromVertices(Vector3 *vertices, unsigned long totalVertices, unsigned long vertexStride, Vector3 ellipsoidRadius = Vector3(1.0f, 1.0f, 1.0f));
	void cloneBoundingVolume(BoundingBox box, BoundingSphere sphere, Vector3 ellipsoidRadius = Vector3(1.0f, 1.0f, 1.0f));
	void respositionBoundingVolume(Matrix4x4 *location);
	// BoundingBox
	void setBoundingBox(Vector3 min, Vector3 max);
	BoundingBox getBoundingBox();
	// Bounding Sphere
	void setBoundingSphere(Vector3 center, float radius, Vector3 ellipsoidRadius = Vector3(1.0f, 1.0f, 1.0f));
	BoundingSphere getBoundingSphere();

	void setEllipsoidRadius(Vector3 sllipsoidRadius);
	Vector3 getEllipsoidRadius();
};

#endif