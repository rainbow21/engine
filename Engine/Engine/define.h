#ifndef DEFINE_H
#define DEFINE_H

#define COUNT_KEY 256
#define COUNT_MOUSE_KEY 3

// ������ ��� ����������� ��������
// ����� ���������� � ������� ����������� ��������
#define SCNAME_WINDOWED "windowed"
#define SCNAME_VSYNC "vsync"
#define SCNAME_BPP	"bpp"
#define SCNAME_RESOLUTION "resolution"
#define SCNAME_REFRESH "refresh"

// ����� ���������� � ������� ���������
#define TXNAME_TRANS	"transparency"
#define TXNAME_DIFF		"diffuse"
#define TXNAME_AMB		"ambient"
#define TXNAME_SPEC		"specular"
#define TXNAME_EMIS		"emissive"
#define TXNAME_POWER	"power"
#define TXNAME_FILENAME	"texture"
// �������������
#define TXNAME_IGFACE	"ignore_face"
#define TXNAME_IGFOG	"ignore_fog"
#define TXNAME_IGRAY	"ignore_ray"

// ������ �������-������
#define SPWNNAME_FREQ	"frequency"
#define SPWNNAME_RADIUS	"radius"
#define SPWNNAME_OBJECT	"object"
#define SPWNNAME_PATH	"object_path"

// ������ ������ (gun.txt)
#define MDNAME_TYPE		"type"
#define MDNAME_NAME		"name"
#define MDNAME_ELL_RAD	"ellipse_radius"
#define MDNAME_MESH		"mesh"
#define MDNAME_PATH		"mesh_path"

#define PI	3.141592654f

#define LEFT_MOUSE_BUTTON	0
#define RIGHT_MOUSE_BUTTON	1
#define MIDLE_MOUSE_BUTTON	2

#endif
