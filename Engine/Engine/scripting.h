#ifndef SCRIPTING_H
#define SCRIPTING_H
#include <stdio.h>
#include "linkedlist.h"
#include <string>
#include "resourcemanagement.h"
#include "vector.h"


// ������� ��������. ������ - ������ ��������� ����������
enum {
	VARIABLE_BOOL,
	VARIABLE_COLOUR,
	VARIABLE_FLOAT,
	VARIABLE_NUMBER,
	VARIABLE_STRING,
	VARIABLE_VECTOR,
	VARIABLE_UNKOWN
};

class Variable
{
private:
	char m_type;	// ��� ������
	char *m_name;	// ���
	void *m_data;	// ������
public:
	Variable(char *name, FILE *file);
	Variable(char *name, char type, void *value);
	~Variable();

	char getType();
	char *getName();
	void *getData();
};
class Script :public Resource<Script>
{
private:
	LinkedList<Variable> *m_variables;
public:
	Script(char *name, char *path = "./");	// �� ��������� ������� ������ � �������� ����
	virtual ~Script();

	void addVariable(char *name, char type, void *value);
	void setVariable(char *name, void *value);

	void save(char *filename = NULL);

	bool* getBoolData(char *var);
	D3DCOLORVALUE* getColorData(char *var);
	float* getFloatData(char *var);
	long*  getNumberData(char *var);
	char* getStringData(char *var);
	Vector3* getVectorData(char *var);
	void* getUnkownData(char *var);
};

#endif