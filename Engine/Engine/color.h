#ifndef COLOR_H
#define COLOR_H

#define CL_WHITE	RGB(255,255,255)
#define CL_BLACK	RGB(0,0,0)
#define CL_RED		RGB(255,0,0)
#define CL_GREEN	RGB(0,255,0)
#define CL_BLUE		RGB(0,0,255)

#endif
