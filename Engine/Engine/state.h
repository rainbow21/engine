/*
	������������ ������� ���������� ������� �������.
	���������� ������ ��������� �������� ������, �� State, ��� ���������� ����� �����������
*/
#ifndef STATE_H
#define STATE_H
#include "sceneobject.h"
// ��������� ������������ ������������
// ��� ������ ������������ �� ������� � ������ �����
// �������������� ������ � ����������� ���������, ����������� �������
struct ViewerSetup
{
	unsigned long viewClearFlags;	// ����� ��� �������� �������
	SceneObject		*viewer;		// ������� ������, ���������� �������

	ViewerSetup()
	{
		viewClearFlags = 0;
	}
};

class State
{
protected:
	unsigned long m_id;	// id - ����������� �����������. ������ ���� ����������
public:
	State(unsigned long id = 0);

	virtual void load() = 0;
	virtual void close() = 0;

	virtual void requestViewer(ViewerSetup *viewer) = 0;
	virtual void update(float elapsed) = 0;
	virtual void render() = 0;

	unsigned long getID();
};

#endif
