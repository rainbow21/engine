#ifndef RESOURCE_MANAGEMENT_H
#define RESOURCE_MANAGEMENT_H
#include "linkedlist.h"
template <class Type>
class Resource
{
private:
	char *m_name;
	char *m_path;
	char *m_filename;
	unsigned long m_refCount;
public:
	Resource(char *name, char *path = "./")
	{
		// ��������� ���
		if (name != NULL)
		{
			m_name = new char[strlen(name) + 1];
			memcpy(m_name, name, (strlen(name) + 1) * sizeof(char));
		}
		// ��������� ����
		if (path != NULL)
		{
			m_path = new char[strlen(path) + 1];
			memcpy(m_path, path, (strlen(path) + 1) * sizeof(char));
		}

		// ������� ��� �����
		if (name != NULL && path != NULL)
		{
			m_filename = new char[strlen(name) + strlen(path) + 1];
			sprintf(m_filename, "%s%s", path, name);
		}
		// �������� ������� ������
		m_refCount = 1;
	}

	virtual ~Resource()
	{
		SAFE_DELETE_ARRAY(m_name);
		SAFE_DELETE_ARRAY(m_path);
		SAFE_DELETE_ARRAY(m_filename);
	}

	char* getName()
	{
		return m_name;
	}

	char* getPath()
	{
		return m_path;
	}

	char* getFilename()
	{
		return m_filename;
	}

	void incRef()
	{
		m_refCount++;
	}
	void decRef()
	{
		m_refCount--;
	}

	unsigned long getRefCount()
	{
		return m_refCount;
	}
};

template <class Type>
class ResourceManager
{
private:
	LinkedList<Type> *m_list;
	void(*CreateResource)(Type **resource, char *name, char *path);

public:
	ResourceManager(void(*CreateResourceFunction)(Type **resource, char *name, char *path) = NULL)
	{
		m_list = new LinkedList<Type>;
		CreateResource = CreateResourceFunction;
	}

	~ResourceManager()
	{
		SAFE_DELETE(m_list);
	}

	// �������� ����� ������
	Type* add(char *name, char *path = "./")
	{
		if (m_list == NULL || name == NULL || path == NULL)
			return NULL;
		Type *element = getElement(name, path);
		if (element != NULL)
		{
			element->incRef();
			return element;
		}

		Type *resource = NULL;
		if (CreateResource != NULL)
			CreateResource(&resource, name, path);
		else
			resource = new Type(name, path);

		return m_list->add(resource);
	}

	void remove(Type **resource)
	{
		if (*resource == NULL || m_list == NULL)
			return;

		(*resource)->decRef();

		if ((*resource)->getRefCount() == 0)
			m_list->remove(resource);
	}

	void clearList()
	{
		if (m_list != NULL)
			m_list->clear();
	}

	LinkedList<Type> *getList()
	{
		return m_list;
	}

	// ������ NULL, ���� ������ �� ������
	Type* getElement(char *name, char *path = "./")
	{
		if (name == NULL || path == NULL || m_list == NULL)
			return NULL;
		if (m_list->getFirst() == NULL)
			return NULL;

		m_list->iterate(true);
		while (m_list->iterate())
			if (strcmp(m_list->getCurrent()->getName(), name) == 0)
				if (strcmp(m_list->getCurrent()->getPath(), path) == 0)
					return m_list->getCurrent();

		return NULL;
	}
};

#endif

