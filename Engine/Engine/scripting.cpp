#include "scripting.h"

Variable::Variable(char *name, FILE *file)
{
	// ��������� ��� ����������
	m_name = new char[strlen(name) + 1];
	strcpy(m_name, name);
	if (file == NULL)
		return;

	// ������ ���
	char buffer[MAX_PATH];
	fscanf(file, "%s", buffer);
	if (strcmp(buffer, "bool") == 0)
	{
		m_type = VARIABLE_BOOL;

		bool value;
		fscanf(file, "%s", buffer);
		if (strcmp(buffer, "true") == 0)
			value = true;
		else
			value = false;
		m_data = new bool;
		memcpy(m_data, &value, sizeof(bool));
	}
	else if (strcmp(buffer, "colour") == 0)
	{
		m_type = VARIABLE_COLOUR;
		D3DCOLORVALUE color;
		fscanf(file, "%s", buffer);
		color.r = (float)atof(buffer);

		fscanf(file, "%s", buffer);
		color.g = (float)atof(buffer);

		fscanf(file, "%s", buffer);
		color.b = (float)atof(buffer);

		fscanf(file, "%s", buffer);
		color.a = (float)atof(buffer);

		m_data = new D3DCOLORVALUE;
		memcpy(m_data, &color, sizeof(D3DCOLORVALUE));
	}
	else if (strcmp(buffer, "float") == 0)
	{
		m_type = VARIABLE_FLOAT;

		float value;
		fscanf(file, "%s", buffer);
		value = (float)atof(buffer);

		m_data = new float;
		memcpy(m_data, &value, sizeof(float));
	}
	else if (strcmp(buffer, "number") == 0)
	{
		m_type = VARIABLE_NUMBER;

		long value;
		fscanf(file, "%s", buffer);
		value = atol(buffer);
		m_data = new long;
		memcpy(m_data, &value, sizeof(long));
	}
	else if (strcmp(buffer, "string") == 0)
	{
		m_type = VARIABLE_STRING;
		bool commasFound = false;
		ZeroMemory(buffer, MAX_PATH * sizeof(char));
		fscanf(file, "%c", buffer);
		while (true)
		{
			if (strcmp(buffer, "\"") == 0)
			{
				commasFound = true;
				break;
			}
			if (strcmp(buffer, " ") != 0)
			{
				fpos_t pos;
				fgetpos(file, &pos);
				fsetpos(file, &--pos);
				break;
			}
			fscanf(file, "%c", buffer);
		}

		char completeString[MAX_PATH];
		ZeroMemory(completeString, MAX_PATH * sizeof(char));
		bool addSpacing = false;
		do
		{
			fscanf(file, "%s", buffer);
			if (strcmp(&buffer[strlen(buffer) - 1], "\"") == 0)
			{
				buffer[strlen(buffer) - 1] = 0;
				commasFound = false;
			}
			if (!addSpacing)
				addSpacing = true;
			else
				strcat(completeString, " ");

			strcat(completeString, buffer);
		} while (commasFound);

		m_data = new char[strlen(completeString) + 1];
		strcpy((char*)m_data, completeString);
	}
	else if (strcmp(buffer, "vector") == 0)
	{
		m_type = VARIABLE_VECTOR;

		Vector3 vector;
		fscanf(file, "%s", buffer);
		vector.x = (float)atof(buffer);
		fscanf(file, "%s", buffer);
		vector.y = (float)atof(buffer);
		fscanf(file, "%s", buffer);
		vector.z = (float)atof(buffer);

		m_data = new Vector3;
		memcpy(m_data, &vector, sizeof(Vector3));
	}
	else
	{
		m_type = VARIABLE_UNKOWN;
		fscanf(file, "%s", buffer);
		m_data = new char[strlen(buffer) + 1];
		strcpy((char*)m_data, buffer);
	}
}	

Variable::Variable(char *name, char type, void *value)
{
	m_name = new char[strlen(name) + 1];
	strcpy(m_name, name);

	m_type = type;

	switch (m_type)
	{
	case VARIABLE_BOOL:
		m_data = new bool;
		memcpy(m_data, (bool*)value, sizeof(bool));
		return;
	case VARIABLE_COLOUR:
		m_data = new D3DCOLORVALUE;
		memcpy(m_data, (D3DCOLORVALUE*)value, sizeof(D3DCOLORVALUE));
		return;
	case VARIABLE_FLOAT:
		m_data = new float;
		memcpy(m_data, (float*)value, sizeof(float));
		return;
	case VARIABLE_NUMBER:
		m_data = new long;
		memcpy(m_data, (long*)value, sizeof(long));
		return;
	case VARIABLE_STRING:
		m_data = new char[strlen((char*)value) + 1];
		strcpy((char*)m_data, (char*)value);
		return;
	case VARIABLE_VECTOR:
		m_data = new Vector3;
		memcpy(m_data, (Vector3*)value, sizeof(Vector3));
		return;
	default:
		m_data = new char[strlen((char*)value) + 1];
		strcpy((char*)m_data, (char*)value);
	}
}

Variable::~Variable()
{
	SAFE_DELETE_ARRAY(m_name);
	SAFE_DELETE(m_data);
}

char Variable::getType()
{
	return m_type;
}

char* Variable::getName()
{
	return m_name;
}

void* Variable::getData()
{
	switch (m_type)
	{
	case VARIABLE_BOOL:
		return (bool*)m_data;
	case VARIABLE_COLOUR:
		return (D3DCOLORVALUE*)m_data;
	case VARIABLE_FLOAT:
		return (float*)m_data;
	case VARIABLE_NUMBER:
		return (long*)m_data;
	case VARIABLE_STRING:
		return (char*)m_data;
	case VARIABLE_VECTOR:
		return (Vector3*)m_data;
	default:
		return m_data;
	}
}

// ---------------- //
//		Script		//
// ---------------- //
Script::Script(char *name, char *path) :Resource<Script>(name, path)
{
	m_variables = new LinkedList<Variable>;
	// ��������� ������
	char *filename = getFilename();
	FILE *file = fopen(filename, "r");
	if (file == NULL)
		return;

	// ������ ���� �� EOF
	bool read = false;
	char buf[MAX_PATH];
	fscanf(file, "%s", buf);
	while (!feof(file))
	{
		// �������� �� �� ����� #begin/#end
		if (read)
		{
			if (strcmp(buf, "#end") == 0)
				read = false;
			else
				m_variables->add(new Variable(buf, file));
		}
		else if (strcmp(buf, "#begin") == 0)
			read = true;

		fscanf(file, "%s", buf);
	}

	fclose(file);
}

Script::~Script()
{
	SAFE_DELETE(m_variables);
}

// �������� ����� ����������
void Script::addVariable(char *name, char type, void *value)
{
	m_variables->add(new Variable(name, type, value));
}

// ������ �������� ����������
void Script::setVariable(char *name, void *value)
{
	Variable *variable = NULL;
	m_variables->iterate(true);

	while (m_variables->iterate() != NULL)
	{
		if (strcmp(m_variables->getCurrent()->getName(), name) == 0)
		{
			variable = m_variables->getCurrent();
			break;
		}
	}

	// ���� ���������� �� ���� �������
	if (!variable)
		return;

	char type = variable->getType();
	// ������� � �������� ��������
	m_variables->remove(&variable);
	addVariable(name, type, value);
}

void Script::save(char *filename)
{
	FILE *file = NULL;
	char out[MAX_PATH];
	if (filename != NULL)
	{
		if ((file = fopen(filename, "w")) == NULL)
			return;
	}
	else
	{
		if ((file = fopen(getFilename(), "w")) == NULL)
			return;
	}

	fputs("#begin\n", file);
	// ���������� ��� ���������� � ����
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
	{
		Variable *var = m_variables->getCurrent();
		switch (var->getType())
		{
		case VARIABLE_BOOL:
			if (*((bool*)var->getData()) == true)
				sprintf(out, "%s bool true", var->getName());
			else
				sprintf(out, "%s bool false", var->getName());
			break;
		case VARIABLE_COLOUR:
			
			sprintf(out, "%s colour %f %f %f %f", var->getName(), 
				((D3DCOLORVALUE*)var->getData())->r,
				((D3DCOLORVALUE*)var->getData())->g,
				((D3DCOLORVALUE*)var->getData())->b,
				((D3DCOLORVALUE*)var->getData())->a);
			break;
		case VARIABLE_FLOAT:
			sprintf(out, "%s float %f", var->getName(), *(float*)var->getData());
			break;
		case VARIABLE_NUMBER:
			sprintf(out, "%s number %d", var->getName(), *(long*)var->getData());
			break;
		case VARIABLE_STRING:
			sprintf(out, "%s string \"%s\"", var->getName(), (char*)var->getData());
			break;
		case VARIABLE_VECTOR:
			sprintf(out, "%s vector %f %f %f", var->getName(), 
				((Vector3*)var->getData())->x,
				((Vector3*)var->getData())->y,
				((Vector3*)var->getData())->z);
			break;
		default:
			sprintf(out, "%s unkown %s", var->getName(), (char*)var->getData());
		}
		fputs(out, file);
		fputs("\n", file);
	}

	fputs("#end", file);
	fclose(file);
}

bool *Script::getBoolData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return (bool*)m_variables->getCurrent()->getData();
	return NULL;
}

D3DCOLORVALUE *Script::getColorData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return (D3DCOLORVALUE*)m_variables->getCurrent()->getData();
	return NULL;
}

float *Script::getFloatData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return (float*)m_variables->getCurrent()->getData();
	return NULL;
}

long *Script::getNumberData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return (long*)m_variables->getCurrent()->getData();
	return NULL;
}

char *Script::getStringData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return (char*)m_variables->getCurrent()->getData();
	return NULL;
}

Vector3 *Script::getVectorData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return (Vector3*)m_variables->getCurrent()->getData();
	return NULL;
}

void *Script::getUnkownData(char *var)
{
	m_variables->iterate(true);
	while (m_variables->iterate() != NULL)
		if (strcmp(m_variables->getCurrent()->getName(), var) == 0)
			return m_variables->getCurrent()->getData();
	return NULL;
}

