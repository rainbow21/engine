#include "boundingvolume.h"


BoundingVolume::BoundingVolume()
{
	m_box = new BoundingBox;
	m_sphere = new BoundingSphere;

	m_ellipsoidRadius = Vector3(0.0f, 0.0f, 0.0f);
}

BoundingVolume::~BoundingVolume()
{
	SAFE_DELETE(m_box);
	SAFE_DELETE(m_sphere);
}

// ������ �������������� ����� �� 3D-����
void BoundingVolume::boundingVolumeFromMesh(ID3DXMesh *mesh, Vector3 ellipsoidRadius)
{
	Vector3 *vertices;
	if (SUCCEEDED(mesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&vertices)))
	{
		D3DXComputeBoundingBox(vertices, mesh->GetNumVertices(), D3DXGetFVFVertexSize(mesh->GetFVF()), &m_box->min, &m_box->max);
		D3DXComputeBoundingSphere(vertices, mesh->GetNumVertices(), D3DXGetFVFVertexSize(mesh->GetFVF()), &m_sphere->center, &m_sphere->radius);
		mesh->UnlockVertexBuffer();
	}

	m_sphere->center.x =float(m_box->min.x + ((m_box->max.x - m_box->min.x) / 2.0));
	m_sphere->center.y = float(m_box->min.y + ((m_box->max.y - m_box->min.y) / 2.0));
	m_sphere->center.z = float(m_box->min.z + ((m_box->max.z - m_box->min.z) / 2.0));

	m_box->halfSize = (float)max(fabs(m_box->max.x), max(fabs(m_box->max.y), fabs(m_box->max.z)));
	m_box->halfSize = (float)max(m_box->halfSize, max(fabs(m_box->min.x), max(fabs(m_box->min.y), fabs(m_box->min.z))));

	m_originalMin = m_box->min;
	m_originalMax = m_box->max;
	m_originalCenter = m_sphere->center;

	setEllipsoidRadius(ellipsoidRadius);
}

// ������ �������������� �����, �� ������ ������
void BoundingVolume::boundingVolumeFromVertices(Vector3 *vertices, unsigned long totalVertices, unsigned long vertexStride, Vector3 ellipsoidRadius)
{
	D3DXComputeBoundingBox(vertices, totalVertices, vertexStride, &m_box->min, &m_box->max);
	D3DXComputeBoundingSphere(vertices, totalVertices, vertexStride, &m_sphere->center, &m_sphere->radius);

	m_sphere->center.x = float(m_box->min.x + ((m_box->max.x - m_box->min.x) / 2.0));
	m_sphere->center.y = float(m_box->min.y + ((m_box->max.y - m_box->min.y) / 2.0));
	m_sphere->center.z = float(m_box->min.z + ((m_box->max.z - m_box->min.z) / 2.0));

	m_box->halfSize = (float)max(fabs(m_box->max.x), max(fabs(m_box->max.y), fabs(m_box->max.z)));
	m_box->halfSize = (float)max(m_box->halfSize, max(fabs(m_box->min.x), max(fabs(m_box->min.y), fabs(m_box->min.z))));

	m_originalMin = m_box->min;
	m_originalMax = m_box->max;
	m_originalCenter = m_sphere->center;

	setEllipsoidRadius(ellipsoidRadius);
}

// �������������� ����� �� ��� ����������
void BoundingVolume::cloneBoundingVolume(BoundingBox box, BoundingSphere sphere, Vector3 ellipsoidRadius)
{
	m_box->max = box.max;
	m_box->min = box.min;
	m_sphere->center = sphere.center;
	m_sphere->radius = sphere.radius;

	m_box->halfSize = (float)max(fabs(m_box->max.x), max(fabs(m_box->max.y), fabs(m_box->max.z)));
	m_box->halfSize = (float)max(m_box->halfSize, max(fabs(m_box->min.x), max(fabs(m_box->min.y), fabs(m_box->min.z))));

	m_originalMin = m_box->min;
	m_originalMax = m_box->max;
	m_originalCenter = m_sphere->center;

	setEllipsoidRadius(ellipsoidRadius);
}

// ��������������� �������������� �����
void BoundingVolume::respositionBoundingVolume(Matrix4x4 *location)
{
	D3DXVec3TransformCoord(&m_box->min, &m_originalMin, location);
	D3DXVec3TransformCoord(&m_box->max, &m_originalMax, location);
	D3DXVec3TransformCoord(&m_sphere->center, &m_originalCenter, location);
}

// �������� ��������������� ��������������
void BoundingVolume::setBoundingBox(Vector3 min, Vector3 max)
{
	m_originalMin = m_box->min = min;
	m_originalMax = m_box->max = max;

	m_box->halfSize = (float)max(fabs(m_box->max.x), max(fabs(m_box->max.y), fabs(m_box->max.z)));
	m_box->halfSize = (float)max(m_box->halfSize, max(fabs(m_box->min.x), max(fabs(m_box->min.y), fabs(m_box->min.z))));
}

// ������� �������������� �������������
BoundingBox BoundingVolume::getBoundingBox()
{
	return *m_box;
}

// ������������� �������� �������������� �����
void BoundingVolume::setBoundingSphere(Vector3 center, float radius, Vector3 ellipsoidRadius)
{
	m_originalCenter = m_sphere->center = center;
	m_sphere->radius = radius;

	setEllipsoidRadius(ellipsoidRadius);
}

// ������� �������������� �����
BoundingSphere BoundingVolume::getBoundingSphere()
{
	return *m_sphere;
}

// ������������� ������� ���������� � ���������� �����������
void BoundingVolume::setEllipsoidRadius(Vector3 ellipsoidRadius)
{
	m_ellipsoidRadius = ellipsoidRadius;
	m_ellipsoidRadius.x *= m_sphere->radius;
	m_ellipsoidRadius.y *= m_sphere->radius;
	m_ellipsoidRadius.z *= m_sphere->radius;
}

// ������ ����������
Vector3 BoundingVolume::getEllipsoidRadius()
{
	return m_ellipsoidRadius;
}