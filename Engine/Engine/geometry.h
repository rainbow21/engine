#ifndef GEOMETRY_H
#define GEOMETRY_H
#include "vector.h"

struct Vertex
{
	Vector3 translation;	// ���������� ������� � ������� ������������
	Vector3 normal;			// ������ ������� �������
	float tu, tv;				// u,v - ���������� ��������

	Vertex()
	{
		translation = Vector3(0.0f, 0.0f, 0.0f);
		normal = Vector3(0.0f, 0.0f, 0.0f);
		tu = 0.0f;
		tv = 0.0f;
	}

	Vertex(Vector3 t, Vector3 n, float u, float v)
	{
		translation = t;
		normal = n;
		tu = u;
		tv = v;
	}
};

#define VERTEX_FVF (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)
#define VERTEX_FVF_SIZE D3DXGetFVFVertexSize(VERTEX_FVF)

// ��������� ���������� �������
struct LVertex
{
	Vector3 translation;
	D3DCOLOR diffuse;
	float tu, tv;

	LVertex()
	{
		translation = Vector3(0.0f, 0.0f, 0.0f);
		diffuse = 0xFFFFFFFF;
		tu = 0.0f;
		tv = 0.0f;
	}

	LVertex(Vector3 t, D3DCOLOR d, float u, float v)
	{
		translation = t;
		diffuse = d;
		tu = u;
		tv = v;
	}
};

#define L_VERTEX_FVF (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define L_VERTEX_FVF_SIZE D3DXGetFVFVertexSize(L_VERTEX_FVF)

// ��������� ������������������ � ���������� �������
struct TLVertex
{
	D3DXVECTOR4 translation;	// � �������� �����������
	D3DCOLOR diffuse;
	float tu, tv;

	TLVertex()
	{
		translation = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);
		diffuse = 0xFFFFFFFF;
		tu = 0.0f;
		tv = 0.0f;
	}

	TLVertex(D3DXVECTOR4 t, D3DCOLOR d, float u, float v)
	{
		translation = t;
		diffuse = d;
		tu = u;
		tv = v;
	}
};

#define TL_VERTEX_FVF (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define TL_VERTEX_FVF_SIZE D3DXGetFVFVertexSize (TL_VERTEX_FVF)

struct Edge
{
	Vertex *vertex0;
	Vertex *vertex1;

	Edge(Vertex *v0, Vertex *v1)
	{
		vertex0 = v0;
		vertex1 = v1;
	}
};

// ��������� ���������������� �����
struct IndexedEdge
{
	unsigned short vertex0;
	unsigned short vertex1;
};

// ��������� �����
struct Face
{
	Vertex *vertex0;
	Vertex *vertex1;
	Vertex *vertex2;

	Face(Vertex *v0, Vertex *v1, Vertex *v2)
	{
		vertex0 = v0;
		vertex1 = v1;
		vertex2 = v2;
	}
};

// ��������� ��������������� �����
struct IndexedFace
{
	unsigned short vertex0;
	unsigned short vertex1;
	unsigned short vertex2;
};

// ���������� true, ���� 2 ����� ����� ���� �� 1 ����� ����� 
inline bool isBoxInBox(Vector3 box1Min, Vector3 box1Max, Vector3 box2Min, Vector3 box2Max)
{
	if (box1Min.x > box2Max.x)
		return false;
	if (box1Min.y > box2Max.y)
		return false;
	if (box1Min.z > box2Max.z)
		return false;

	if (box1Max.x < box2Min.x)
		return false;
	if (box1Max.y < box2Min.y)
		return false;
	if (box1Max.z < box2Min.z)
		return false;

	return true;
}

// ����� �� ����� ������ �����
inline bool isFaceInBox(Vertex *vertex0, Vertex *vertex1, Vertex *vertex2, Vector3 boxMin, Vector3 boxMax)
{
	float minX = min(vertex0->translation.x, min(vertex1->translation.x, vertex2->translation.x));
	float maxX = max(vertex0->translation.x, max(vertex1->translation.x, vertex2->translation.x));
	if (maxX < boxMin.x)
		return false;
	if (minX > boxMax.x)
		return false;

	float minY = min(vertex0->translation.y, min(vertex1->translation.y, vertex2->translation.y));
	float maxY = max(vertex0->translation.y, max(vertex1->translation.y, vertex2->translation.y));
	if (maxY < boxMin.y)
		return false;
	if (minY > boxMax.y)
		return false;

	float minZ = min(vertex0->translation.z, min(vertex1->translation.z, vertex2->translation.z));
	float maxZ = max(vertex0->translation.z, max(vertex1->translation.z, vertex2->translation.z));
	if (maxZ < boxMin.z)
		return false;
	if (minZ > boxMax.z)
		return false;

	return true;
}

// ���� ��������� ������ ������� (volume)
inline bool isBoxEnclosedByVolume(LinkedList<D3DXPLANE> *planes, Vector3 min, Vector3 max)
{
	// D3DXPlaneDotCoord - ���������� ���������� �� ��������� �� �����, �� ������� �������
	planes->iterate(true);
	while (planes->iterate())
	{
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(min.x, min.y, min.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(min.x, min.y, max.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(min.x, max.y, min.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(min.x, max.y, max.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(max.x, min.y, min.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(max.x, min.y, max.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(max.x, max.y, min.z)) < 0.0f)
			return false;
		if (D3DXPlaneDotCoord(planes->getCurrent(), &Vector3(max.x, max.y, max.z)) < 0.0f)
			return false;
	}

	return true;
}

// ����� �������� ����������� ������ �������
inline bool isSphereOverlappingVolume(LinkedList<D3DXPLANE> *planes, Vector3 translation, float radius)
{
	planes->iterate(true);
	while (planes->iterate())
		if (D3DXPlaneDotCoord(planes->getCurrent(), &translation) < -radius)
			return false;
	return true;
}

// ������������ 2-�� ����
inline bool isSphereCollidingWithSphere(float collisionDistance, Vector3 translation1, Vector3 translation2, Vector3 velocitySum, float radiiSum)
{
	Vector3 v = translation1 - translation2;
	// ���������� ����� �������
	float distanceBetween = D3DXVec3Length(&v);
	distanceBetween -= radiiSum;
	// ����� ����� �������� ���������
	float velocityLength = D3DXVec3Length(&velocitySum);
	
	// ���� ����� ����� �� ������������� � ����� �� ����� �������� ������ ���������� ����� ����, �� ��� �� ������������
	if (distanceBetween > 0.0f && velocityLength < distanceBetween)
		return false;

	// ��������������� ����� �������� �������� ���� ����
	Vector3 normalizedVelocity;
	D3DXVec3Normalize(&normalizedVelocity, &velocitySum);

	// ������ �������� ������ ����� � ������
	Vector3 direction = translation1 - translation2;

	// ���� ����� ���������������� ��������� �������� � ����������� ��������
	float angleBetween = D3DXVec3Dot(&normalizedVelocity, &direction);

	// �� ��������� �� ����� ���� �� �����
	if (angleBetween <= 0.0f)
	{
		// ����������� �������� �� ������������
		if (distanceBetween < 0.0f)
		{
			if (velocityLength > -distanceBetween)
				return false;
		}
		else
			return false;
	}

	// ����� ������� ����������� ��������
	float directionLength = D3DXVec3Length(&direction);

	// ������ ����� ����� �������
	float hypotenuse = (directionLength * directionLength) - (angleBetween*angleBetween);

	// �� ������������ �� ����� ���������� ������?
	float radiiSumSquared = radiiSum * radiiSum;
	if (hypotenuse >= radiiSumSquared)
		return false;

	// ���������� �� ������� ��������, ��� ������� ����� ����������
	// ������ ���������� �� ������������
	float distance = radiiSumSquared - hypotenuse;
	collisionDistance = angleBetween - (float)sqrt(distance);

	// ��������, ��� ����� �� ��������� � ������������ �������, ��� ��������� ��� ��������
	if (velocityLength < collisionDistance)
		return false;

	return true;
}

#endif
