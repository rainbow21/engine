#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include "delete.h"
template <class Type> class LinkedList
{
public:
	struct Element
	{
		Type *data;
		Element *next;
		Element *prev;

		Element(Type *element)
		{
			data = element;
			next = prev = NULL;
		}

		~Element()
		{
			SAFE_DELETE(data);
			if (next)
				next->prev = prev;
			if (prev)
				prev->next = next;
		}
	};
private:
	Element *m_first;
	Element *m_last;
	Element *m_iterate;
	Element *m_temp;

	unsigned long m_totalElements;
public:
	LinkedList()
	{
		m_first = m_last = m_iterate = m_temp = NULL;
		m_totalElements = 0;
	}
	~LinkedList()
	{
		clear();
	}
	// �������� � �����
	Type *add(Type *element)
	{
		if (element == NULL)
			return NULL;
		if (m_first == NULL)
		{
			m_first = new Element(element);
			m_last = m_first;
		}
		else
		{
			m_last->next = new Element(element);
			m_last->next->prev = m_last;
			m_last = m_last->next;
		}

		m_totalElements++;
		return m_last->data;
	}
	// �������� �����
	Type* insertBefore(Type *element, Element *nextElement)
	{
		m_temp = nextElement->prev;
		m_totalElements++;

		if (m_temp == NULL)
		{
			m_first = new Element(element);
			m_first->next = nextElement;
			nextElement->prev = m_first;

			return m_first->data;
		}
		else
		{
			m_tem->next = new Element(element);
			m_temp->next->prev = m_temp;
			m_temp->next->next = nextElement;
			nextElement->prev = m_temp->next;
			return m_temp->next->data;
		}
	}

	// ������� ������� �� ������
	void remove(Type **element)
	{
		m_temp = m_first;
		while (m_temp != NULL)
		{
			if (m_temp->data == *element)
			{
				if (m_temp == m_first)
				{
					m_first = m_first->next;
					if (m_first)
						m_first->prev = NULL;
				}
				if (m_temp == m_last)
				{
					m_last = m_last->prev;
					if (m_last)
						m_last->next = NULL;
				}

				SAFE_DELETE(m_temp);
				*element = NULL;
				m_totalElements--;
				return;
			}
			m_temp = m_temp->next;
		}
	}

	// ������� ��� ��������, ������ � �������
	void clear()
	{
		while (m_last != NULL)
		{
			m_temp = m_last;
			m_last = m_last->prev;
			SAFE_DELETE(m_temp);
		}
		m_first = m_last = m_iterate = m_temp = NULL;
		m_totalElements = 0;
	}

	// ������� (�������) ��� �������� �������� ������ ������ � �����������.
	// ��������: ��� ������� �� ���������� ������, ������. � ���������.
	void clearPointers()
	{
		while (m_last != NULL)
		{
			m_temp = m_last;
			m_temp->data = NULL;
			m_last = m_last->prev;
			SAFE_DELETE(m_temp);
		}
		m_first = m_last = m_iterate = m_temp = NULL;
		m_totalElements = 0;
	}

	// ������� ��� �������� ������ � �����������
	void clearPointer(Type **element)
	{
		m_temp = m_first;
		while (m_temp != NULL)
		{
			if (m_temp->data == *element)
			{
				if (m_temp == m_first)
				{
					m_first = m_first->next;
					if (m_first)
						m_first->prev = NULL;
				}
				if (m_temp == m_last)
				{
					m_last = m_last->prev;
					if (m_last)
						m_last->next = NULL;
				}
				
				m_temp->data = NULL;
				SAFE_DELETE(m_temp);
				*element = NULL;

				m_totalElements--;
				return;
			}
			m_temp = m_temp->next;
		}
	}

	//-------------------------------------------------------------------------
	// �������� (������������) ������� �������� � ������
	//-------------------------------------------------------------------------
	Type *iterate(bool restart = false)
	{
		if (restart)
			m_iterate = NULL;
		else
		{
			if (m_iterate == NULL)
				m_iterate = m_first;
			else
				m_iterate = m_iterate->next;
		}

		if (m_iterate == NULL)
			return NULL;
		else
			return m_iterate->data;
	}

	//-------------------------------------------------------------------------
	// ���������� ������� �������� ������, ����������� � ������ ������.
	//-------------------------------------------------------------------------
	Type *getCurrent()
	{
		if (m_iterate)
			return m_iterate->data;
		else
			return NULL;
	}

	//-------------------------------------------------------------------------
	// ���������� ������ ������� �������� ������.
	//-------------------------------------------------------------------------
	Type *getFirst()
	{
		if (m_first)
			return m_first->data;
		else
			return NULL;
	}

	//-------------------------------------------------------------------------
	// ���������� ��������� ������� �������� ������.
	//-------------------------------------------------------------------------
	Type *getLast()
	{
		if (m_last)
			return m_last->data;
		else
			return NULL;
	}

	//-------------------------------------------------------------------------
	// ���������� ������� �������� ������, ��������� �� ������.
	//-------------------------------------------------------------------------
	Type *getNext(Type *element)
	{
		m_temp = m_first;
		while (m_temp != NULL)
		{
			if (m_temp->data == element)
			{
				if (m_temp->next == NULL)
					return NULL;
				else
					return m_temp->next->data;
			}

			m_temp = m_temp->next;
		}

		return NULL;
	}

	//-------------------------------------------------------------------------
	// ���������� ��������� ������� �������� ������.
	//-------------------------------------------------------------------------
	Type *getRandom()
	{
		if (m_totalElements == 0)
			return NULL;
		else if (m_totalElements == 1)
			return m_first->data;

		unsigned long element = rand() * m_totalElements / RAND_MAX;

		m_temp = m_first;
		for (unsigned long e = 0; e < element; e++)
			m_temp = m_temp->next;

		return m_temp->data;
	}

	//-------------------------------------------------------------------------
	// ���������� ������ ������� ������
	//	(������� ��������� �� ���������� � ����. ��������).
	//-------------------------------------------------------------------------
	Element *getCompleteElement(Type *element)
	{
		m_temp = m_first;
		while (m_temp != NULL)
		{
			if (m_temp->data == element)
				return m_temp;

			m_temp = m_temp->next;
		}

		return NULL;
	}

	Type*	get(int i)
	{
		if (i < 0 || i >= m_totalElements)
			return NULL;

		m_temp = m_first;
		for (int j = 0; j < i && m_temp; j++)
			m_temp = m_temp->next;
		return m_temp->data;
	}

	//-------------------------------------------------------------------------
	// ���������� ����� ��������� � ������� ������.
	//-------------------------------------------------------------------------
	unsigned long getTotalElements()
	{
		return m_totalElements;
	}
};

#endif

