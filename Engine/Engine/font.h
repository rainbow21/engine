// ��������� �������
// ��������� � ������� ���������������� ������
#ifndef FONT_H
#define FONT_H

#include <Windows.h>
#include <d3dx9.h>
#include "color.h"
#include "delete.h"
#include "label.h"

class Font
{
private:
	IDirect3DStateBlock9 *m_states;	// ���������, ��� �������������� 
	IDirect3DVertexBuffer9 *m_vb;	// ��������� ����� ��� ������
	IDirect3DTexture9 *m_texture;	// Direct3D-�������� ��� ������
	unsigned long m_textureWidth;
	unsigned long m_textureHeight;
	float m_textureCoords[96][4];	// ���������� ����������, ��� ������������� �������� �� ����� ��������
	short m_spacing;				// ���������� ����� ���������

	bool prepareFont(HDC hdc, bool measure = false);
public:
	Font(char *name = "Arial", short size = 10, unsigned long bold = FW_NORMAL, bool italic = false);
	virtual ~Font();

	void render(char *text, float x, float y, D3DCOLOR color = D3DCOLOR_COLORVALUE(1.0f, 1.0f, 1.0f, 1.0f));
	void render(Label l);
};

#endif