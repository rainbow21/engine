#include "engine.h"

Engine *g_engine = NULL;

// ������� ���������
LRESULT CALLBACK WindowProc(HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_ACTIVATEAPP:
		g_engine->setDeactiveFlag(!wParam);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(wnd, msg, wParam, lParam);
	}
}

// Constructor
Engine::Engine(EngineSetup *setup)
{
	m_loaded = false;
	
	m_setup = new EngineSetup;
	if (setup != NULL)
		memcpy(m_setup, setup, sizeof(EngineSetup));
	g_engine = this;

	// ���������� �������� ������
	WNDCLASSEX wClass;
	wClass.cbSize = sizeof(WNDCLASSEX);
	wClass.style = CS_CLASSDC;
	wClass.lpfnWndProc = WindowProc;
	wClass.cbClsExtra = 0;
	wClass.cbWndExtra = 0;
	wClass.hInstance = m_setup->instance;
	wClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wClass.hbrBackground = NULL;
	wClass.lpszMenuName = NULL;
	wClass.lpszClassName = "WindowClass";
	wClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	RegisterClassEx(&wClass);

	// �������������� COM
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// �������������� ��������� Direct3D
	IDirect3D9 *d3d = Direct3DCreate9(D3D_SDK_VERSION);
	// ���������� ������������ ���������� Direct3D �� �������� �� ���������
	g_deviceEnumeration = new DeviceEnumeration;
	if (g_deviceEnumeration->enumerate(d3d) != IDOK)
	{
		SAFE_RELEASE(d3d);
		return;
	}

	// �������� ����. ���� ������ ���� Windowed, �� ��������� ��� ����. ����� Full Screen (WS_POPUP)
	m_window = CreateWindowEx(WS_EX_TOPMOST, "WindowClass", m_setup->name, g_deviceEnumeration->isWindowed() ? WS_OVERLAPPED : WS_POPUP, 0, 0, 800, 600, NULL, NULL, m_setup->instance, NULL);

	// ��������� ������������� ������� ����������
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.BackBufferWidth = g_deviceEnumeration->getSelectedDisplayMode().Width;
	d3dpp.BackBufferHeight = g_deviceEnumeration->getSelectedDisplayMode().Height;
	d3dpp.BackBufferFormat = g_deviceEnumeration->getSelectedDisplayMode().Format;
	d3dpp.BackBufferCount = m_setup->totalBackBuffers;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;		// ������ �������� ��� ����� �������. DISCARD - �������������� ����� ���������� ��������
	d3dpp.hDeviceWindow = m_window;		
	d3dpp.Windowed = g_deviceEnumeration->isWindowed();
	d3dpp.EnableAutoDepthStencil = true;			// ��������� ������ ��������� ������� (Z-�����)
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;		// ������ ������ ��������� �������. 16 ��� �� ����� ������� (Z-�����) � 0 �� ��������
	d3dpp.FullScreen_RefreshRateInHz = g_deviceEnumeration->getSelectedDisplayMode().RefreshRate;	// ������� ����������
	if (g_deviceEnumeration->isVSynced())
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
	else
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	// ���������� ������ ����������
	SAFE_DELETE(g_deviceEnumeration);

	// ������� Direct3D
	// ������� �������, ��� ���������� (D3DDEVTYPE_HAL - ��������� ���� HAL), ����, ����� �������� �������� ����������, ��������� ���������, ����� ��� ���������� ����������
	HRESULT hr = d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_window, D3DCREATE_MIXED_VERTEXPROCESSING, &d3dpp, &m_device);
	int r = HRESULT_CODE(hr);
	if (FAILED(hr))
		return;
	// �� �������:
	// ��������� �������, ���������� ��������� ����������, ����, ����� ������������ ��� �����������, ��� � ���������� ���������, ���������
	// D3DCREATE_HARDWARE_VERTEXPROCESSING - ������� �������������� ���������, ��������� �������
	// D3DCREATE_SOFTWARE_VERTEXPROCESSING - ������� �������������� ������������ ���������� DirectX
	// D3DCREATE_MIXED_VERTEXPROCESSING	   - ����� � ��� � ���

	// ����������� ��������� Direct3D
	SAFE_RELEASE(d3d);

	// ��������� ��������� �� ���������
	m_device->SetRenderState(D3DRS_LIGHTING, false);

	// ������� �������. ��������, ����� �������� �� �������� �� ������� � �����
	m_device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);	// ������������
	m_device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);	// �����������
	m_device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);		// �������� ����� ����� ����������
	// D3DTEXF_POINT		- ��������� ������� � ��������� �������� �������.
	// D3DTEXF_LINEAR		- ���������� ������������, ��� ����������������� ���� �������� 2�2 ����������� � ��������� �������
	// D3DTEXF_ANISOTROPIC	- ������������. ����������� ���� ����� ������ � �������� ����������.


	// ������������� ������� ��������. ������� ������ ���� 4�4
	// Fov - ���� ���������, LH - ���������� ������� ��������� (��� Z �� ���)
	Matrix4x4 projMatrix;
	
	D3DXMatrixPerspectiveFovLH(
		&projMatrix,					// ������� ��� ���������� ����������
		D3DX_PI / 4,					// ���� ���������, � ����� ����������� ��� ����� 90 ��������
		(float)d3dpp.BackBufferWidth / (float)d3dpp.BackBufferHeight,	// ������/������
		0.1f / m_setup->scale,			// ������� ��������� ���������	(Near plane). ���������� �� Near plane
		1000.0f / m_setup->scale);		// �������� ��������� ��������� (Far plane)	���������� �� Far plane
	// ������� �������, ������ ������� �� Direct3D ����������
	m_device->SetTransform(D3DTS_PROJECTION, &projMatrix);

	// ��������� ��������� ��������� �����������
	m_displayMode.Width = d3dpp.BackBufferWidth;
	m_displayMode.Height = d3dpp.BackBufferHeight;
	m_displayMode.RefreshRate = d3dpp.FullScreen_RefreshRateInHz;
	m_displayMode.Format = d3dpp.BackBufferFormat;

	// ����-������� ���������� � ������� ���������
	m_currentBackBuffer = 0;
	
	// ��������� �������
	// ������ ����������, � ������� �������� ��������� � �����, ��� ����� ��������� ��������� ���������� ID3DXSprite
	D3DXCreateSprite(m_device, &m_sprite);

	// ������ �������
	m_states = new LinkedList<State>;
	m_currentState = NULL;
	// ������� ��������� ��������
	m_scriptManager = new ResourceManager<Script>;
	m_materialManager = new ResourceManager<Material>(m_setup->createMaterialResource);
	m_meshManager = new ResourceManager<Mesh>;

	// ���������� �����
	m_input = new Input(m_window);

	// ������� ��������� ��������� �����
	srand(timeGetTime());
	
	// ��������� ����������� �������
	// ���� ��������� ����������� ������� ��� �� ����������, �� ��� ����������
	if (m_setup->stateSetup != NULL)
		m_setup->stateSetup();

	m_loaded = true;
}

// Desctructor 
Engine::~Engine()
{
	if (m_loaded)
	{
		// ����������� ���������
		if (m_currentState != NULL)
			m_currentState->close();
		SAFE_DELETE(m_states);

		SAFE_DELETE(m_input);
		SAFE_RELEASE(m_sprite);
		SAFE_DELETE(m_meshManager);
		SAFE_DELETE(m_materialManager);
		SAFE_DELETE(m_scriptManager);
		SAFE_RELEASE(m_device);
	}
	// ��������������� COM
	CoUninitialize();

	UnregisterClass("WindowClass", m_setup->instance);
	SAFE_DELETE(m_setup);

	
	
}

// ������� ���� ��������� ���������
void Engine::run()
{
	if (m_loaded)
	{
		ShowWindow(m_window, SW_NORMAL);	// ���������� ����

		// ��������� �������� �������
		ViewerSetup viewer;

		MSG msg;
		ZeroMemory(&msg, sizeof(MSG));
		
		while (msg.message != WM_QUIT)
		{
			//Sleep(10);
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else if (!m_deactive)
			{
				unsigned long currentTime = timeGetTime();	// ������������ ����������� �����
				static unsigned long lastTime = currentTime;
				float elapsed = (currentTime - lastTime) / 1000.0f;
				lastTime = currentTime;
				
				// ������ ���� � ����������
				m_input->update();

				// ���� ������ ������� ESC, �� ������������� ��������� ������
				if (m_input->getKeysPress(DIK_ESCAPE))
					PostQuitMessage(0);

				// ������ ������� �������� ������ (���� �� ����)
				if (m_currentState != NULL)
				{
					m_currentState->requestViewer(&viewer);

					if (viewer.viewer)
						// ������������� ������������� ����
						m_device->SetTransform(D3DTS_VIEW, viewer.viewer->getViewMatrix());
				}


				// �������� ������� �����
				m_stateChanged = false;
				if (m_currentState != NULL)
					m_currentState->update(elapsed);
				if (m_stateChanged == true)
					continue;

				// �������������� �����
				// ������� ������� ��
				// ...,..., �����, ����� ������ ����������� �������, ���� ����������, �������� Z-������, �������� ���������
				// ��������� �����:
				// D3DCLEAR_STENCIL	- ������� ��������, ������������ �������� ���������� ���������
				// D3DCLEAR_TARGET	- ������� ���� ����������, ������������ ���������� ����
				// D3DCLEAR_ZBUFFER	- ������� Z-�����, ������������ ���������� ��������
				m_device->Clear(0, NULL, viewer.viewClearFlags, 0, 1.0f, 0);

				if (SUCCEEDED(m_device->BeginScene()))
				{
					// �������� ������� �����, ���� ������� �������
					if (m_currentState != NULL)
						m_currentState->render();

					// ����������� �������� ����� � ���������� ��
					m_device->EndScene();
					m_device->Present(NULL, NULL, NULL, NULL);

					// ����������� ������ �������� ���������
					if (++m_currentBackBuffer == m_setup->totalBackBuffers + 1)
						m_currentBackBuffer = 0;
				}
			}
		}
	}
	SAFE_DELETE(g_engine);
}

// �������� ���������� ����
HWND Engine::getWindow()
{
	return m_window;
}

// ���������� ������������
void Engine::setDeactiveFlag(bool deactive)
{
	m_deactive = deactive;
}

// �������� ��������
float Engine::getScale()
{
	return m_setup->scale;
}

void Engine::setScale(float s)
{
	m_setup->scale = s;
}

// ���������� Direct3D
IDirect3DDevice9* Engine::getDevice()
{
	return m_device;
}

// ������� ����� ������� ���������� Direct3D
D3DDISPLAYMODE* Engine::getDisplayMode()
{
	return &m_displayMode;
}

// ��������� �������
ID3DXSprite* Engine::getSprite()
{
	return m_sprite;
}

// ��������� ����� �����. ���� ���������� ����, �� ������ ��� �������.
void Engine::addState(State *state, bool change)
{
	m_states->add(state);

	if (change == false)
		return;

	if (m_currentState != NULL)
		m_currentState->close();

	m_currentState = m_states->getLast();
	m_currentState->load();
}

void Engine::removeState(State *state)
{
	m_states->remove(&state);
}

void Engine::changeState(unsigned long id)
{
	m_states->iterate(true);
	while (m_states->iterate() != NULL)
	{
		if (m_states->getCurrent()->getID() == id)
		{
			// ��������� ������ �����
			if (m_currentState != NULL)
				m_currentState->close();

			// ������������� �����
			m_currentState = m_states->getCurrent();
			m_currentState->load();

			// ������� ���-������ �� ��� ���, ���� ������ ��� ����� �� ������ �����������
			while (m_currentBackBuffer)
			{
				m_device->Present(NULL, NULL, NULL, NULL);
				if (++m_currentBackBuffer == m_setup->totalBackBuffers + 1)
					m_currentBackBuffer = 0;
			}

			// ��������� ���� ���������
			m_stateChanged = true;
			break;
		}
	}
}

State *Engine::getCurrentState()
{
	return m_currentState;
}

ResourceManager<Script>* Engine::getScriptManager()
{
	return m_scriptManager;
}
ResourceManager<Material>* Engine::getMaterialManager()
{
	return m_materialManager;
}
ResourceManager<Mesh> *Engine::getMeshManager()
{
	return m_meshManager;
}

Input* Engine::getInput()
{
	return m_input;
}

bool Engine::keyPress(char key, bool ignorePressStamp)
{
	return m_input->getKeysPress(key, ignorePressStamp);
}
bool Engine::mouseClick(char button, bool ignorePressStamp)
{
	return m_input->getButtonPress(button, ignorePressStamp);
}
long Engine::mousePosX()
{
	return m_input->getPosX();
}
long Engine::mousePosY()
{
	return m_input->getPosY();
}
long Engine::mouseDeltaX()
{
	return m_input->getDeltaX();
}
long Engine::mouseDeltaY()
{
	return m_input->getDeltaY();
}
long Engine::mouseDeltaWheel()
{
	return m_input->getDeltaWheel();
}