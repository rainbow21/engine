// ��������� ����������������� �����
#ifndef INPUT_H
#define INPUT_H

#include <Windows.h>
#include <d3dx9.h>
#include <dinput.h>
#include "define.h"
#include "delete.h"

class Input
{
private:
	HWND m_window;				// ������������ ����
	IDirectInput8 *m_di;		// ���������� �����
	unsigned long m_pressStamp; // ������� � �������

	IDirectInputDevice8 *m_keyboard;		// ���������� - ����������
	char m_keyState[256];					// ��������� ������
	unsigned long m_keyPressStamp[COUNT_KEY];		// ������� � ������� ������ �� ������ ����������

	IDirectInputDevice8 *m_mouse;			// ���� - ����������
	DIMOUSESTATE m_mouseState;				// ��������� ������ ����
	unsigned long m_buttonPressStamp[COUNT_MOUSE_KEY];	// ������� � ������� ������ �� ������ ���� � ���������� �����
	POINT m_position;						// ������� ������� �������

public:
	Input(HWND window);
	virtual ~Input();

	void update();

	bool getKeysPress(char key, bool ignorePressStamp = false);

	bool getButtonPress(char button, bool ignorePressStamp = false);
	long getPosX();
	long getPosY();
	long getDeltaX();
	long getDeltaY();
	long getDeltaWheel();
};

#endif
