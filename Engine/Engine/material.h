// �������� - �������� + �������� ������� ���������
#ifndef MATERIAL_H
#define MATERIAL_H
#include "resourcemanagement.h"
#include <d3d9.h>
#include <d3dx9tex.h>
#include "scripting.h"
#include "define.h"


class Material :public Resource<Material>
{
private:
	IDirect3DTexture9 *m_texture;	// ��������
	D3DMATERIAL9 m_lighting;		// �������� ���������
	unsigned long m_width;
	unsigned long m_height;
	bool m_ignoreFace;	// ������������� �����
	bool m_ignoreFog;	// ������������� ������
	bool m_ignoreRay;	// ������������� �������� �����
public:
	Material(char *name, char *path = "./");
	virtual ~Material();
	
	IDirect3DTexture9* getTexture();
	D3DMATERIAL9* getLighting();
	unsigned long getWidth();
	unsigned long getHeight();
	bool getIgnoreFace();
	bool getIgnoreFog();
	bool getIgnoreRay();
};

#endif