#include "animatedobject.h"

AnimatedObject::AnimatedObject(char *meshName, char *meshPath, unsigned long type) :SceneObject(type, meshName, meshPath, false)
{
	// ������� ���� ����������� ��������
	if (getMesh())
		getMesh()->cloneAnimationController(&m_animationController);
	else
		m_animationController = NULL;

	// ������������� �������� ����� ������ �� ������
	if (m_animationController)
	{
		m_animationController->SetTrackSpeed(0, 1.0f);
		m_animationController->SetTrackSpeed(1, 1.0f);
	}

	// ������� ����������, ������������ � ��������
	m_currentTrack = 0;
	m_currentTime = 0.0f;
}

AnimatedObject::~AnimatedObject()
{
	SAFE_RELEASE(m_animationController);
}

// ��������� ��������� �������
void AnimatedObject::update(float elapsed, bool addVelocity)
{
	// ��������� ���������� �������� ������� �����
	SceneObject::update(elapsed, addVelocity);

	// ���������, ���� �� � ���� ������� ���������� ��������
	if (m_animationController)
	{
		// ����������� ����� �������� ����������� �������� �������
		// ���������������, ��������� �� ��������� ��������� ������
		m_animationController->AdvanceTime(elapsed, this);

		// ������ �� ������� �������� ��� ���������� ��������
		m_currentTime += elapsed;
	}

	// ��������� ���
	if (getMesh())
		getMesh()->update();
}

// ����������� ������ �������� � ������ �������� ��������
void AnimatedObject::playAnimation(unsigned int animation, float transitionTime, bool loop)
{
	// ��������� ������� �����������
	if (!m_animationController)
		return;

	// �������, ��� ����� �������� ������ > 0
	if (transitionTime <= 0.0f)
		transitionTime = 0.000001f;

	// ������� �� ����� ����� �������������� ����� ��������
	unsigned int newTrack = (m_currentTrack == 0 ? 1 : 0);

	// �������� ��������� �� ����� �������� ��� ����������������
	ID3DXAnimationSet *as;
	m_animationController->GetAnimationSet(animation, &as);

	// ������������� ����� �������� �� ����� ����
	m_animationController->SetTrackAnimationSet(newTrack, as);

	// ������� ��� �������, ������� � ������ ������ ����������� �� ������
	m_animationController->UnkeyAllTrackEvents(m_currentTrack);
	m_animationController->UnkeyAllTrackEvents(newTrack);

	// ��������� ������, ����� �������� ���������
	if (loop)
	{
		// ��������� �� ����� ���� � �������� ������� ��������
		// �������������� ���������� ����� ���������� ������� (����� ��������� transitionTime)
		m_animationController->KeyTrackEnable(m_currentTrack, false, m_currentTime + transitionTime);
		m_animationController->KeyTrackWeight(m_currentTrack, 0.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR);
		m_animationController->SetTrackEnable(newTrack, true);
		m_animationController->KeyTrackWeight(newTrack, 1.0f, m_currentTime, transitionTime, D3DXTRANSITION_LINEAR);
	}
	else
	{
		// ������������ ������� ���� � �������� �������������� ����� ��� ��������
		m_animationController->SetTrackEnable(m_currentTrack, false);
		m_animationController->SetTrackWeight(m_currentTrack, 0.0f);
		m_animationController->SetTrackEnable(newTrack, true);
		m_animationController->SetTrackWeight(newTrack, 1.0f);
		// ����� �������� ������������� � ������ ������
		m_animationController->SetTrackPosition(newTrack, 0.0f);
		// ��������� ��������, ����� �� ���������� (as->GetPeriod())
		m_animationController->KeyTrackEnable(newTrack, false, m_currentTime + as->GetPeriod());
	}
	// ������� ��������� �� ����� ��������
	as->Release();
	// ����� ���� ���������� �������
	m_currentTrack = newTrack;
}

// ���������� �������� �������
ID3DXAnimationController* AnimatedObject::getAnimationController()
{
	return m_animationController;
}

// ������� ��������� ������ ��������
HRESULT CALLBACK AnimatedObject::HandleCallback(THIS_ UINT Track, LPVOID pCallbackData)
{
	return S_OK;
}