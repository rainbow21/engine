#include "input.h"

Input::Input(HWND window)
{
	m_window = window;

	// ������� ��������� DirectInput
	// ���������� ����������, ������ DirectInput, ID ���������� ����������, ��������� �� ���������, ��� �����������
	DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_di, NULL);

	// �������, �������������� � ����������� ����������
	// GUID ������������ ����������, ��������� ��� ��������� � ����������, ��� �����������
	// GUID - Globally Unique ID
	m_di->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL);
	m_keyboard->SetDataFormat(&c_dfDIKeyboard);
	m_keyboard->SetCooperativeLevel(m_window, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	m_keyboard->Acquire();

	// �������, �������������� � ����������� ����
	m_di->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
	m_mouse->SetDataFormat(&c_dfDIMouse);
	m_mouse->SetCooperativeLevel(m_window, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	m_mouse->Acquire();

	// ����� ������� � �������
	// ����� - ������ ��� ����������� � ����� ������ ����� ��������� ������� � ��������� ���
	m_pressStamp = 0;
}

Input::~Input()
{
	SAFE_RELEASE(m_di);
	SAFE_RELEASE(m_keyboard);
	SAFE_RELEASE(m_mouse);
}

// �������� ��������� ���������
void Input::update()
{
	static HRESULT result;

	// ����������� ����������
	while (true)
	{
		m_keyboard->Poll();
		if (SUCCEEDED(result = m_keyboard->GetDeviceState(COUNT_KEY, (LPVOID)&m_keyState)))
			break;
		// ���� ���������� �������� �� ������� (�.�. ����� �� ��� �������), �� �������� �������� �������
		// 2 �������� ��������� �������: ���������� ������� ��� ������ �� ���� ���������
		// ���� ���-�� �� �����, �� ���������� ���������� ���������!
		if (result != DIERR_INPUTLOST && result != DIERR_NOTACQUIRED)
			return;

		// ���� ����� ��� ������, ��������������� ����������
		if (FAILED(m_keyboard->Acquire()))
			return;
	}

	// ���������� ����
	while (true)
	{
		m_mouse->Poll();
		if (SUCCEEDED(result = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), &m_mouseState)))
			break;
		if (result != DIERR_INPUTLOST && result != DIERR_NOTACQUIRED)
			return;

		if (FAILED(m_mouse->Acquire()))
			return;
	}

	// ���� �� �����, ������ ��� ���������� ��������� � ����������

	// ������� ���� �� ������
	GetCursorPos(&m_position);
	// ������� ���� �� ������� ����
	ScreenToClient(m_window, &m_position);
	// ����������� ������� � �������
	m_pressStamp++;
}

// ������� �� ����������
// ���� ���� ����������, �� �������� false ��� ������� ��������
bool Input::getKeysPress(char key, bool ignorePressStamp)
{
	if ((m_keyState[key] & 0x80) == false)
		return false;

	bool pressed = true;
	if (!ignorePressStamp)
		if (m_keyPressStamp[key] == m_pressStamp - 1 || m_keyPressStamp[key] == m_pressStamp)
			pressed = false;
	m_keyPressStamp[key] = m_pressStamp;
	
	return pressed;
}

// ������� �� �����
// ���� ���� ����������, �� �������� false ��� ������� ��������
bool Input::getButtonPress(char button, bool ignorePressStamp)
{
	if (!(m_mouseState.rgbButtons[button] & 0x80))
		return false;

	bool pressed = true;
	if (!ignorePressStamp)
		if (m_buttonPressStamp[button] == m_pressStamp - 1 || m_buttonPressStamp[button] == m_pressStamp)
			pressed = false;
	
	m_buttonPressStamp[button] = m_pressStamp;
	return pressed;
}

// ��������� ����
long Input::getPosX()
{
	return m_position.x;
}
long Input::getPosY()
{
	return m_position.y;
}

// ��������� ����
long Input::getDeltaX()
{
	return m_mouseState.lX;
}
long Input::getDeltaY()
{
	return m_mouseState.lY;
}

long Input::getDeltaWheel()
{
	return m_mouseState.lZ;
}