#include "rendercache.h"

RenderCache::RenderCache(IDirect3DDevice9 *device, Material *material)
{
	m_device = device;
	m_material = material;

	m_indexBuffer = NULL;
	m_totalIndices = 0;
}

RenderCache::~RenderCache()
{
	SAFE_RELEASE(m_indexBuffer);
}

// ����������� ������ ������-���� ��� ���������� ��� ����� ������
void RenderCache::addFace()
{
	m_totalIndices += 3;
}

// ���������� ���� � �������������. ����� ��������� �������!
void RenderCache::prepare(unsigned long totalVertices)
{
	// ������������� ����� ����� ������, ����������� � ���������
	m_totalIndices = totalVertices;

	m_device->CreateIndexBuffer(m_totalIndices * sizeof(unsigned short), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_indexBuffer, NULL);
}

// ����������� ��� � ���, ��� ���������� ��������
void RenderCache::begin()
{
	m_indexBuffer->Lock(0, 0, (void**)&m_indexPointer, 0);
	m_faces = 0;
}

// �������� ������� ������ ������ �����, ����������� � ���������
void RenderCache::renderFace(unsigned short vertex0, unsigned short vertex1, unsigned short vertex2)
{
	*m_indexPointer++ = vertex0;
	*m_indexPointer++ = vertex1;
	*m_indexPointer++ = vertex2;

	m_faces++;
}

// ����������� ���� � ���, ��� ��������� �������. ����� �������� �����
void RenderCache::end()
{
	m_indexBuffer->Unlock();

	// ���� �� �����, ����������� � ���������
	if (!m_faces)
		return;

	// ������ �� �������� ������������ �����
	if (m_material->getIgnoreFog())
		m_device->SetRenderState(D3DRS_FOGENABLE, false);

	// ��������� �������� � ��������
	m_device->SetMaterial(m_material->getLighting());
	m_device->SetTexture(0, m_material->getTexture());

	// ��������� ������� ������ ������ ��� ���������� ���������� ������
	m_device->SetIndices(m_indexBuffer);

	// ������ ��� �����
	m_device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_totalVertices, 0, m_faces);

	// ��������������� ��������� ������, ���� ���� ��������
	if (m_material->getIgnoreFog())
		m_device->SetRenderState(D3DRS_FOGENABLE, true);
}

// ������ ������������ ��������
Material* RenderCache::getMaterial()
{
	return m_material;
}