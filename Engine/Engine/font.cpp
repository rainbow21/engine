#include "font.h"
#include "engine.h"
Font::Font(char* name, short size, unsigned long bold, bool italic)
{
	HBITMAP bitmap = NULL;
	HGDIOBJ oldBitmap = NULL;
	HFONT oldFont = NULL;
	BYTE *dstRow = NULL;
	unsigned long x, y;

	HDC hdc = CreateCompatibleDC(NULL);
	SetMapMode(hdc, MM_TEXT);

	// ������� �����
	char height = -MulDiv(size, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	HFONT font = CreateFont(height, 0, 0, 0, bold, italic, false, false, 
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, name);
	if (font)
	{
		oldFont = (HFONT)SelectObject(hdc, font);

		// ���� ������� ���������� ��������, ���������� �������
		m_textureWidth = m_textureHeight = 128;
		while (!prepareFont(hdc, true))
		{
			m_textureWidth *= 2;
			m_textureHeight *= 2;
		}

		// ������� ����� �������� ��� ������
		if (SUCCEEDED(g_engine->getDevice()->CreateTexture(m_textureWidth, m_textureHeight, 1, 0, D3DFMT_A4R4G4B4, D3DPOOL_MANAGED, &m_texture, NULL)))
		{ 
			// ������� ��������� �����������
			unsigned long *bitmapBits;
			BITMAPINFO bmi;
			ZeroMemory(&bmi.bmiHeader, sizeof(BITMAPINFOHEADER));
			bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
			bmi.bmiHeader.biWidth = (int)m_textureWidth;
			bmi.bmiHeader.biHeight = -(int)m_textureHeight;
			bmi.bmiHeader.biPlanes = 1;
			bmi.bmiHeader.biCompression = BI_RGB;
			bmi.bmiHeader.biBitCount = 32;

			// ��������� ����������� ��� ������
			bitmap = CreateDIBSection(hdc, &bmi, DIB_RGB_COLORS, (void**)&bitmapBits, NULL, 0);
			oldBitmap = SelectObject(hdc, bitmap);

			// ������������ �������� ������
			SetTextColor(hdc, CL_WHITE);
			SetBkColor(hdc, CL_BLACK);
			SetTextAlign(hdc, TA_TOP);
		
			// ������� ����� � ���������� �������� �� �������
			if (prepareFont(hdc))
			{
				// ��������� ����������� � ���������� �������� �����-������ ��� ��������
				D3DLOCKED_RECT d3dlr;
				m_texture->LockRect(0, &d3dlr, 0, 0);
				dstRow = (BYTE*)d3dlr.pBits;
				WORD *dst16;
				BYTE alpha;

				for (y = 0; y < m_textureHeight; y++)
				{
					dst16 = (WORD*)dstRow;
					for (x = 0; x < m_textureWidth; x++)
					{

						alpha = (BYTE)((bitmapBits[m_textureWidth*y + x] & 0xff) >> 4);
						if (alpha > 0)
							*dst16++ = (WORD)((alpha << 12) | 0x0fff);
						else
							*dst16++ = 0x0000;
					}
					dstRow += d3dlr.Pitch;
				}

				// ������� ��������� ����� ��� ��������
				g_engine->getDevice()->CreateVertexBuffer(1020 * sizeof(TLVertex), D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC, 0, D3DPOOL_DEFAULT, &m_vb, NULL);

				// ������� ������-�������� ��� ���������� ��������
				g_engine->getDevice()->SetRenderState(D3DRS_ALPHAREF, 0x08);
				g_engine->getDevice()->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

				// ������� ���� �������� ��� ������� ����������� (������������) ������-�������
				g_engine->getDevice()->BeginStateBlock();
				g_engine->getDevice()->SetRenderState(D3DRS_LIGHTING, false);
				g_engine->getDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, false);
				g_engine->getDevice()->SetRenderState(D3DRS_FOGENABLE, false);
				g_engine->getDevice()->EndStateBlock(&m_states);
			}
		}
	}

// End:
	if (m_texture)
		m_texture->UnlockRect(0);

	SelectObject(hdc, oldBitmap);
	SelectObject(hdc, oldFont);
	DeleteObject(bitmap);
	DeleteObject(font);
	DeleteDC(hdc);
}

Font::~Font()
{
	SAFE_RELEASE(m_states);
	SAFE_RELEASE(m_vb);
	SAFE_RELEASE(m_texture);
}

// ���������� ���� �������� � ��������� ����������
bool Font::prepareFont(HDC hdc, bool measure)
{
	SIZE size;
	char string[2];

	// ������� ����� ���������
	if (!GetTextExtentPoint32(hdc, string, 1, &size))
		return false;
	m_spacing = (short)ceil(size.cy*0.1f);

	// ������� ������ ���������
	unsigned long x = m_spacing;
	unsigned long y = 0;

	// ������� ������ ������ � �������� ���������� � ������������ �� ��� ������
	for (char c = 32; c < 127; c++)
	{
		string[0] = c;
		if (!GetTextExtentPoint32(hdc, string, 1, &size))
			return false;
		if ((unsigned long)(x + size.cx + m_spacing) > m_textureWidth)
		{
			x = m_spacing;
			y += size.cy + 1;
		}

		// ���������, �������� �� ��� ����� ��� ����������
		if (y + size.cy > m_textureHeight)
			return false;

		// ������� ������ ���� �� ��������
		if (!measure)
		{
			if (!ExtTextOut(hdc, x + 0, y + 0, ETO_OPAQUE, NULL, string, 1, NULL))
				return false;
			m_textureCoords[c - 32][0] = (float)(x - m_spacing) / m_textureWidth;
			m_textureCoords[c - 32][1] = (float)(y) / m_textureHeight;
			m_textureCoords[c - 32][2] = (float)(x + size.cx + m_spacing) / m_textureWidth;
			m_textureCoords[c - 32][3] = (float)(y + size.cy) / m_textureHeight;
		}

		x += size.cx + (2 * m_spacing);
	}
	return true;
}

// ������ ��������� ����� �� �����, ��������� ������ �����
void Font::render(char *text, float x, float y, D3DCOLOR color)
{
	//����������� ������� ������������ ������-������
	m_states->Capture();

	// ������������� ������������ ������
	IDirect3DDevice9 *device = g_engine->getDevice();
	device->SetRenderState(D3DRS_LIGHTING, false);
	device->SetRenderState(D3DRS_ALPHATESTENABLE, true);
	device->SetRenderState(D3DRS_FOGENABLE, false);

	// ������������� ���������� ������-������
	device->SetTexture(0, m_texture);
	device->SetFVF(TL_VERTEX_FVF);
	device->SetStreamSource(0, m_vb, 0, TL_VERTEX_FVF_SIZE);

	// ���������� ����� ���������
	x -= m_spacing;
	float startX = x;

	// ��������� ��������� �����
	TLVertex *vertices = NULL;
	unsigned long totalTriangles = 0;
	m_vb->Lock(0, 0, (void**)&vertices, D3DLOCK_DISCARD);
	
	// ��� ������ ����� ��������� ���������������� ���� (��� ����� = �������������) � �����
	while (*text)
	{
		char c = *text++;

		if (c == _T('\n'))
		{
			x = startX;
			y += (m_textureCoords[0][3] - m_textureCoords[0][1]) * m_textureHeight;
		}

		if ((c - 32) < 0 || (c - 32) >= 96)
			continue;

		float tx1 = m_textureCoords[c - 32][0];
		float ty1 = m_textureCoords[c - 32][1];
		float tx2 = m_textureCoords[c - 32][2];
		float ty2 = m_textureCoords[c - 32][3];

		float w = (tx2 - tx1)*m_textureWidth;
		float h = (ty2 - ty1)*m_textureHeight;

		if (c != _T(' '))
		{
			*vertices++ = TLVertex(D3DXVECTOR4(x - 0.5f,		y + h - 0.5f,	0.0f, 1.0f), color, tx1, ty2);
			*vertices++ = TLVertex(D3DXVECTOR4(x - 0.5f,		y - 0.5f,		0.0f, 1.0f), color, tx1, ty1);
			*vertices++ = TLVertex(D3DXVECTOR4(x + w - 0.5f,	y + h - 0.5f,	0.0f, 1.0f), color, tx2, ty2);
			*vertices++ = TLVertex(D3DXVECTOR4(x + w - 0.5f,	y - 0.5f,		0.0f, 1.0f), color, tx2, ty1);
			*vertices++ = TLVertex(D3DXVECTOR4(x + w - 0.5f,	y + h - 0.5f,	0.0f, 1.0f), color, tx2, ty2);
			*vertices++ = TLVertex(D3DXVECTOR4(x - 0.5f,		y - 0.5f,		0.0f, 1.0f), color, tx1, ty1);
			totalTriangles += 2;

			if (totalTriangles == 340)
			{
				// ������������ ������ � ����� ����������� ��������� �����
				m_vb->Unlock();
				device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, totalTriangles);
				vertices = NULL;
				m_vb->Lock(0, 0, (void**)&vertices, D3DLOCK_DISCARD);
				totalTriangles = 0;
			}
		}
		x += w - (2 * m_spacing);
	}

	// ������������ � �������� ���������� ���������� ������
	m_vb->Unlock();
	if (totalTriangles > 0)
		device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, totalTriangles);
	// ��������������� ������������ ������-������
	m_states->Apply();
}

void Font::render(Label l)
{
	render(l.text, l.x, l.y, l.color);
}