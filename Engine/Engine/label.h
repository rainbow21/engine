#ifndef LABEL_H
#define LABEL_H
#include <Windows.h>
#include "color.h"
struct Label
{
	char *text;
	int x;
	int y;
	DWORD color;

	Label(char *txt, int _x = 0.0f,int _y = 0.0f, DWORD c = CL_WHITE)
	{
		text = txt;
		x = _x;
		y = _y;
		color = c;
	}

};

#endif
