#include "material.h"
#include "engine.h"

Material::Material(char *name, char *path) :Resource<Material>(name, path)
{
	D3DXIMAGE_INFO	info;

	// ��������� ������ ��� ������� ���������
	Script *script = new Script(name, path);

	// ����� �� �������� ������� ������������
	// D3DXCreateTextureFromFileEx:
	// ���������� ������� Direct3D, ������ ��� ����� ��������, ������, ������, ����� mip-�������, ����� ������������� �������, 
	// �������� ������ ��������, ����� ������, ����� ����������, ����� MipMap-����������, ���� ������������, �������� ������ ��������, 256 �������, ����� ��� ��������
	// Pool:
	// D3DPOOL_DEFAULT		-	������������� ������ ���, ����� �������� Direct3D. ������ � �����������
	// D3DPOOL_MANAGED		-	������������� �� ����. �������� ����� ���� �������� � ��������� ������ � ������������� �� ���������
	// D3DPOOL_SYSTEMMEM	-	�������� ������ � ��������� ������, ������������� ��� ����� Direct3D. ���������� ���������� ���������
	// Filter � MipFilter
	// D3DX_FILTER_POIN�
	// D3DX_FILTER_LINEAR
	// D3DX_FILTER_TRIANGLE
	if (script->getColorData(TXNAME_TRANS)->a == 0.0f)
	{
		// ��������� �������� ��� ������������
		D3DXCreateTextureFromFileEx(g_engine->getDevice(), script->getStringData(TXNAME_FILENAME),
			D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_FILTER_TRIANGLE, D3DX_FILTER_TRIANGLE, 0, &info, NULL, &m_texture);
	}
	else
	{
		D3DCOLORVALUE *color = script->getColorData(TXNAME_TRANS);
		D3DCOLOR transparency = D3DCOLOR_COLORVALUE(color->r, color->g, color->b, color->a);	// D3DCOLORVALUE -> DWORD D3DCOLOR
		D3DXCreateTextureFromFileEx(g_engine->getDevice(), script->getStringData(TXNAME_FILENAME),
			D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_FILTER_TRIANGLE, D3DX_FILTER_TRIANGLE, transparency, &info, NULL, &m_texture);
	}

	m_width = info.Width;
	m_height = info.Height;

	// �������� ��������� ���������
	m_lighting.Diffuse = *script->getColorData(TXNAME_DIFF);
	m_lighting.Ambient = *script->getColorData(TXNAME_AMB);
	m_lighting.Specular = *script->getColorData(TXNAME_SPEC);
	m_lighting.Emissive = *script->getColorData(TXNAME_EMIS);
	m_lighting.Power = *script->getFloatData(TXNAME_POWER);

	// ���� ������������� �����, ������, �������� �����
	m_ignoreFace = *script->getBoolData(TXNAME_IGFACE);
	m_ignoreFog = *script->getBoolData(TXNAME_IGFOG);
	m_ignoreRay = *script->getBoolData(TXNAME_IGRAY);

	SAFE_DELETE(script);
}

Material::~Material()
{
	SAFE_RELEASE(m_texture);
}

// ������ ��������
IDirect3DTexture9* Material::getTexture()
{
	return m_texture;
}

// �������� ���������
D3DMATERIAL9* Material::getLighting()
{
	return &m_lighting;
}

// ������, ������
unsigned long Material::getWidth()
{
	return m_width;
}
unsigned long Material::getHeight()
{
	return m_height;
}

// ������ ����� �������������
bool Material::getIgnoreFace()
{
	return m_ignoreFace;
}
bool Material::getIgnoreFog()
{
	return m_ignoreFog;
}
bool Material::getIgnoreRay()
{
	return m_ignoreRay;
}