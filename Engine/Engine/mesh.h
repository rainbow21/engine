// �������� � ���������� ���������� � �������������� 3D-������
#ifndef MESH_H
#define MESH_H

#include <d3d9.h>
//#include <d3dx9anim.h>
#include <d3dx9math.h>
#include "material.h"
#include "boundingvolume.h"
#include "geometry.h"

struct Frame :public D3DXFRAME
{
	Matrix4x4	finalTransformationMatrix; // ��������� ������������ (� ���������� � ������������)

	// ���������� ���������� ������. ���������� ��������� ������ � 3D ������������
	Vector3 getTranslation()
	{
		return Vector3(finalTransformationMatrix._41, finalTransformationMatrix._42, finalTransformationMatrix._43);
	}
};

// Mesh-���������
struct MeshContainer :public D3DXMESHCONTAINER
{
	char				**materialNames;		// ������ ���� ��������� (������ ����)
	Material			**materials;			// ������ ����������, ������������ ���-����������� (�������� ��������)
	ID3DXMesh			*originalMesh;			// �������� ���
	D3DXATTRIBUTERANGE	*attributeTable;		// ������� ���������
	unsigned long		totalAttributeGroups;	// ����� ���������� ����� ���������
	Matrix4x4			**boneMatrixPointers;	// ������� ������������� ������
};

// Allocate Hierarchy
class AllocateHierarchy :public ID3DXAllocateHierarchy
{
	STDMETHOD(CreateFrame)			(THIS_ LPCSTR Name, LPD3DXFRAME *ppNewFrame);
	STDMETHOD(CreateMeshContainer)	(THIS_ LPCSTR Name, CONST D3DXMESHDATA *pMeshData, 
			CONST D3DXMATERIAL *pMaterials, CONST D3DXEFFECTINSTANCE *pEffectInstances, DWORD NumMaterials, CONST DWORD *pAdjacency,
		LPD3DXSKININFO pSkinInfo, LPD3DXMESHCONTAINER *ppNewMeshContainer);
	STDMETHOD(DestroyFrame)			(THIS_ LPD3DXFRAME pFrameToFree);
	STDMETHOD(DestroyMeshContainer)	(THIS_ LPD3DXMESHCONTAINER pMeshContainerToFree);
};

class Mesh :public BoundingVolume, public Resource<Mesh>
{
private:
	Frame						*m_firstFrame;			// ������ ����� � �������� ����
	ID3DXAnimationController	*m_animationController;	// ���������� ��������
	Matrix4x4					*m_boneMatrices;		// ������ ������ ������������� ������
	unsigned long				m_totalBoneMatrices;	// ����� ������ � �������

	MeshContainer				*m_staticMesh;			// ��������� ���
	Vertex						*m_vertices;			// ������ ������ �� ������������ ����
	unsigned short				*m_indices;				// ������ �������� � ������� ������

	LinkedList<Frame>			*m_frames;				// ��������� �� ��� ������ ����
	LinkedList<Frame>			*m_refPoints;			// ��������� �� ��� ����������� ����� ����

	void prepareFrame(Frame *frame);
	void updateFrame(Frame *frame, Matrix4x4 *parentTransformationMAtrix = NULL);
	void renderFrame(Frame *frame);
public:
	Mesh(char *name, char *path = "./");
	virtual ~Mesh();

	void update();
	void render();

	void cloneAnimationController(ID3DXAnimationController **animationController);

	MeshContainer *getStaticMesh();
	Vertex *getVertices();
	unsigned short *getIndices();

	LinkedList<Frame>* getFrameList();
	Frame* getFrame(char *name);
	Frame* getReferencePoint(char *name);

};

#endif
