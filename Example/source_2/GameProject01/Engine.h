//-----------------------------------------------------------------------------
// ������� ������������ ���� ������. ���� ���� ��������� ������ �������,
// � �������� ������������ ������������ ������, ������� ������� ��������� � �������,
// ����������� �� ���� ������� ������.
//
//	Original Source code:
//  Programming a Multiplayer First Person Shooter in DirectX
//  Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef ENGINE_H
#define ENGINE_H

//-----------------------------------------------------------------------------
// System Includes
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <tchar.h>
#include <windowsx.h>

//-----------------------------------
// ��������� �� ������������� 8 ������ DirectInput (����������� 8 ������ ����� �������������)
//-----------------------------------
#define DIRECTINPUT_VERSION 0x0800

//-----------------------------------------------------------------------------
// DirectX Includes
//-----------------------------------------------------------------------------
#include <d3dx9.h>
#include <dinput.h>

//-----------------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------------
#define SAFE_DELETE( p )       { if( p ) { delete ( p );     ( p ) = NULL; } }
#define SAFE_DELETE_ARRAY( p ) { if( p ) { delete[] ( p );   ( p ) = NULL; } }
#define SAFE_RELEASE( p )      { if( p ) { ( p )->Release(); ( p ) = NULL; } }

//-----------------------------------------------------------------------------
// Engine Includes
//-----------------------------------------------------------------------------
#include "Resource.h"
#include "LinkedList.h"
#include "ResourceManagement.h"
#include "Geometry.h"
#include "Font.h"
#include "Scripting.h"
#include "DeviceEnumeration.h"
#include "Input.h"
#include "State.h"

//-----------------------------------------------------------------------------
// Engine Setup Structure
//-----------------------------------------------------------------------------
struct EngineSetup
{
	HINSTANCE instance; // ���������� ���������� ����������.
	char *name; // �������� ����������.
	float scale; // ������� (scale) � ������/�����.
	unsigned char totalBackBuffers; // ����� ������������ ����������.
	void (*StateSetup)(); // ������� ���������� ������.

	//-------------------------------------------------------------------------
	// The engine setup structure constructor.
	//-------------------------------------------------------------------------
	EngineSetup()
	{
		instance = NULL;
		name = "Application"; // �������� �������� ����������
		scale = 1.0f;
		totalBackBuffers = 1;
		StateSetup = NULL;
	}
};

//-----------------------------------------------------------------------------
// Engine Class
//-----------------------------------------------------------------------------
class Engine
{
public:
	Engine( EngineSetup *setup = NULL );
	virtual ~Engine();

	void Run();

	HWND GetWindow();
	void SetDeactiveFlag( bool deactive );

	float GetScale(); 
	IDirect3DDevice9 *GetDevice(); 
	D3DDISPLAYMODE *GetDisplayMode(); 
	ID3DXSprite *GetSprite();

	void AddState( State *state, bool change = true );
	void RemoveState( State *state );
	void ChangeState( unsigned long id );
	State *GetCurrentState();

	ResourceManager< Script > *GetScriptManager();

	Input *GetInput();

private:
	bool m_loaded; // ���� ����������, �������� �� ������ ��� ���.
	HWND m_window; // ���������� �������� ����.
	bool m_deactive; // ���� ����������, ������� ���������� ��� ���.

	EngineSetup *m_setup; // ����� (�������) ��������� EngineSetup.

	IDirect3DDevice9 *m_device; 
	D3DDISPLAYMODE m_displayMode; 
	ID3DXSprite *m_sprite; 
	unsigned char m_currentBackBuffer;

	LinkedList< State > *m_states; // ������� ������ (Linked list) �������.
	State *m_currentState; // ��������� �� ������� �����.
	bool m_stateChanged; // ���� ����������, ������� �� ����� � ������� �����.

	ResourceManager< Script > *m_scriptManager;

	Input *m_input; // Input object.
};

//-----------------------------------------------------------------------------
// Externals
//-----------------------------------------------------------------------------
extern Engine *g_engine;

#endif