//-----------------------------------------------------------------------------
// ����������� ��� �������� � ���������� ��������� (static) �
// ������������� (animated) 3D-�����.
//
// Original Sourcecode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef MESH_H
#define MESH_H

//-----------------------------------------------------------------------------
// Frame Structure
//-----------------------------------------------------------------------------
struct Frame : public D3DXFRAME
{
	D3DXMATRIX finalTransformationMatrix; // ��������� ������������� ������ ����� ���, ��� �� ����� ������������� �� ����� ���������.

	//-------------------------------------------------------------------------
	// ���������� ���������� (translation) ������.
	//-------------------------------------------------------------------------
	D3DXVECTOR3 GetTranslation()
	{
		return D3DXVECTOR3( finalTransformationMatrix._41, finalTransformationMatrix._42, finalTransformationMatrix._43 );
	}
};

//-----------------------------------------------------------------------------
// Mesh Container Structure
//-----------------------------------------------------------------------------
struct MeshContainer : public D3DXMESHCONTAINER
{
	char **materialNames; // ��������� ������ ��� ��������� (��������).
	Material **materials; // ������ ����������, ������������ ���-����������� (mesh container).
	ID3DXMesh *originalMesh; // �������� (original) ���.
	D3DXATTRIBUTERANGE *attributeTable; // ������� ��������� ����.
	unsigned long totalAttributeGroups; // ����� ���������� ����� ���������.
	D3DXMATRIX **boneMatrixPointers; // ������ ���������� �� ������� ������������� ������ (bone transformation matrices).
};

//-----------------------------------------------------------------------------
// Allocate Hierarchy Class
//-----------------------------------------------------------------------------
class AllocateHierarchy : public ID3DXAllocateHierarchy
{
	STDMETHOD( CreateFrame )( THIS_ LPCSTR Name, LPD3DXFRAME *ppNewFrame );
	STDMETHOD( CreateMeshContainer )( THIS_ LPCSTR Name, CONST D3DXMESHDATA *pMeshData, CONST D3DXMATERIAL *pMaterials, CONST D3DXEFFECTINSTANCE *pEffectInstances, DWORD NumMaterials, CONST DWORD *pAdjacency, LPD3DXSKININFO pSkinInfo, LPD3DXMESHCONTAINER *ppNewMeshContainer );
	STDMETHOD( DestroyFrame )( THIS_ LPD3DXFRAME pFrameToFree );
	STDMETHOD( DestroyMeshContainer )( THIS_ LPD3DXMESHCONTAINER pMeshContainerToFree );
};

//-----------------------------------------------------------------------------
// Mesh Class
//-----------------------------------------------------------------------------
class Mesh : public BoundingVolume, public Resource< Mesh >
{
public:
	Mesh( char *name, char *path = "./" );
	virtual ~Mesh();

	void Update();
	void Render();

	void CloneAnimationController( ID3DXAnimationController **animationController );

	MeshContainer *GetStaticMesh();
	Vertex *GetVertices();
	unsigned short *GetIndices();

	LinkedList< Frame > *GetFrameList();
	Frame *GetFrame( char *name );
	Frame *GetReferencePoint( char *name );

private:
	void PrepareFrame( Frame *frame );
	void UpdateFrame( Frame *frame, D3DXMATRIX *parentTransformationMatrix = NULL );
	void RenderFrame( Frame *frame );

private:
	Frame *m_firstFrame; // ������ (�������) ����� � �������� ������� ����.
	ID3DXAnimationController *m_animationController; // ���������� ��������.

	D3DXMATRIX *m_boneMatrices; // ������ ������ ������������� ������ (bone transformation matrices).
	unsigned long m_totalBoneMatrices; // ����� ������ � �������.

	MeshContainer *m_staticMesh; // ��������� (���������������) ������ ����.
	Vertex *m_vertices; // ������ ������ �� ���������� ����.
	unsigned short *m_indices; // ������ �������� � ������� ������.

	LinkedList< Frame > *m_frames; // ������� ������ (linked list) ���������� �� ��� ������ ����.
	LinkedList< Frame > *m_refPoints; // ������� ������ (linked list) ���������� �� �� ��� ����������� ����� (reference points) ����.
};

#endif