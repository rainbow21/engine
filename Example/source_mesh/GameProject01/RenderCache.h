//-----------------------------------------------------------------------------
// File: RenderCache.h
//
// Version: 1.0
//
// Description: Manages the rendering of indexed faces from a set of vertices.
// ��������� ����������� ��������������� ������ �� ������ ������.
//
// Change History
// ~~~~~~~~~~~~~~
// v1.0 (17/02/04) - Inception.
//
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef RENDER_CACHE_H
#define RENDER_CACHE_H

//-----------------------------------------------------------------------------
// Render Cache Class
//-----------------------------------------------------------------------------
class RenderCache
{
public:
	RenderCache( IDirect3DDevice9 *device, Material *material );
	virtual ~RenderCache();

	void AddFace();
	void Prepare( unsigned long totalVertices );

	void Begin();
	void RenderFace( unsigned short vertex0, unsigned short vertex1, unsigned short vertex2 );
	void End();

	Material *GetMaterial();

private:
	IDirect3DDevice9 *m_device; // ��������� �� ������ ���������� Direct3D.
	Material *m_material; // ��������� �� ��������, ������������ ������ ������-�����.

	IDirect3DIndexBuffer9 *m_indexBuffer; // ��������� �����, ����������� �� �������, ���������� ����������.
	unsigned short *m_indexPointer; // ��������� ��� ������� � ���������� ������ �� ������ ������� � �������.
	unsigned long m_totalIndices; // ����� ����� ��������, ������� ������������ ������ ��������� �����.
	unsigned long m_faces; // ����� ����� ������, ����������� � ����������.

	unsigned long m_totalVertices; // ����� ����� ������.
};

#endif