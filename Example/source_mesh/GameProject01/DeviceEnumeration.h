//-----------------------------------------------------------------------------
// File: DeviceEnumeration.h
// �������� ����� ������������ ��������� Direct3D.
//
// Original sourcecode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------

#ifndef DEVICE_ENUMERATION_H
#define DEVICE_ENUMERATION_H

//-----------------------------------------------------------------------------
// ��������� DisplayMode (����� �������)
//-----------------------------------------------------------------------------
struct DisplayMode
{
	D3DDISPLAYMODE mode; // ����� ������� Direct3D.
	char bpp[6]; // ������� ������������� (�.�. ������� ���������),
				//���������� � ���� ������ �������� ��� �������.
};

//-----------------------------------------------------------------------------
// ����� ������������ ��������� (Device Enumeration)
//-----------------------------------------------------------------------------
class DeviceEnumeration
{
public:
	INT_PTR Enumerate( IDirect3D9 *d3d );

	INT_PTR SettingsDialogProc( HWND dialog, UINT uiMsg, WPARAM wParam, LPARAM lParam );

	D3DDISPLAYMODE *GetSelectedDisplayMode();
	bool IsWindowed();
	bool IsVSynced();

private:
	void ComboBoxAdd( HWND dialog, int id, void *data, char *desc );
	void ComboBoxSelect( HWND dialog, int id, int index );
	void ComboBoxSelect( HWND dialog, int id, void *data );
	void *ComboBoxSelected( HWND dialog, int id );
	bool ComboBoxSomethingSelected( HWND dialog, int id );
	int ComboBoxCount( HWND dialog, int id );
	bool ComboBoxContainsText( HWND dialog, int id, char *text );

private:
	Script *m_settingsScript; // ������, � ������� �������� ��������� �������.

	D3DADAPTER_IDENTIFIER9 m_adapter; // ������������� �������� Direct3D.
	LinkedList< DisplayMode > *m_displayModes; // ������� ������ (Linked list) ������������� ������� �������.
	D3DDISPLAYMODE m_selectedDisplayMode; // ����� �������, ��������� �������������.
	bool m_windowed; // ����, �����������, ������ �� ���������� ����������� � ������� ������.
	bool m_vsync; // ����, �����������, ������� �� v-sync (������������ �������������) ��� ���.
};

//-----------------------------------------------------------------------------
// ������� ���������
//-----------------------------------------------------------------------------
extern DeviceEnumeration *g_deviceEnumeration;

#endif