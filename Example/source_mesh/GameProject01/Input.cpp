//-----------------------------------------------------------------------------
// ���������� ������� ����������������� �����, ����������� � Input.h.
// �������� � ����������� Input.h �� ����� ��������� �����������.
//
// Original SourceCode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#include "Engine.h"

//-----------------------------------------------------------------------------
// The input class constructor.
//-----------------------------------------------------------------------------
Input::Input( HWND window )
{
	// ��������� ���������� ������������� ���� �� ���������� ���������� ������ Input.
	m_window = window;

	// ������ ��������� DirectInput.
	DirectInput8Create( GetModuleHandle( NULL ), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_di, NULL );

	// ������, ��������������, � ����������� (aquire) ���������� ����������.
	m_di->CreateDevice( GUID_SysKeyboard, &m_keyboard, NULL );
	m_keyboard->SetDataFormat( &c_dfDIKeyboard );
	m_keyboard->SetCooperativeLevel( m_window, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
	m_keyboard->Acquire();

	// ������, ��������������, � ����������� (aquire) ���������� ����.
	m_di->CreateDevice( GUID_SysMouse, &m_mouse, NULL );
	m_mouse->SetDataFormat( &c_dfDIMouse );
	m_mouse->SetCooperativeLevel( m_window, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
	m_mouse->Acquire();

	// �������� ������� � ������� (press stamp).
	m_pressStamp = 0;
}

//-----------------------------------------------------------------------------
// The input class destructor.
//-----------------------------------------------------------------------------
Input::~Input()
{
	SAFE_RELEASE( m_di );
	SAFE_RELEASE( m_keyboard );
	SAFE_RELEASE( m_mouse );
}

//-----------------------------------------------------------------------------
// ��������� (Updates) ��������� ����� ���������: ���������� � ����.
//-----------------------------------------------------------------------------
void Input::Update()
{
	static HRESULT result;

	// ���������� (Poll) ���������� �� ��� ���, ���� �������� �� ���������� ������� ��� ������ ����������� ������.
	while( true )
	{
		m_keyboard->Poll();
		if( SUCCEEDED( result = m_keyboard->GetDeviceState( 256, (LPVOID)&m_keyState ) ) )
			break;
		if( result != DIERR_INPUTLOST && result != DIERR_NOTACQUIRED )
			return;

		// ��������������� (Reacquire) ����������, ���� ����� ��� ������.
		if( FAILED( m_keyboard->Acquire() ) )
			return;
	}

	// ���������� (Poll) ���� �� ��� ���, ���� �������� �� ���������� ������� ��� ������ ����������� ������.
	while( true )
	{
		m_mouse->Poll();
		if( SUCCEEDED( result = m_mouse->GetDeviceState( sizeof( DIMOUSESTATE ), &m_mouseState ) ) )
			break;
		if( result != DIERR_INPUTLOST && result != DIERR_NOTACQUIRED )
			return;

		// ��������������� (Reacquire) ����������, ���� ����� ��� ������.
		if( FAILED( m_mouse->Acquire() ) )
			return;
	}

	// ����������� ������������� ������� ����.
	GetCursorPos( &m_position );
	ScreenToClient( m_window, &m_position );

	// �������������� (����������� �� 1) ������� � ������� (press stamp).
	m_pressStamp++;
}

//-----------------------------------------------------------------------------
// ���������� true (������) ���� ���� ������ ������ �������.
// ����������: ��������������� ������� ������ false ��� ������������� press stamp.
//-----------------------------------------------------------------------------
bool Input::GetKeyPress( char key, bool ignorePressStamp )
{
	if( ( m_keyState[key] & 0x80 ) == false )
		return false;

	bool pressed = true;

	if( ignorePressStamp == false )
		if( m_keyPressStamp[key] == m_pressStamp - 1 || m_keyPressStamp[key] == m_pressStamp )
			pressed = false;

	m_keyPressStamp[key] = m_pressStamp;

	return pressed;
}

//-----------------------------------------------------------------------------
// ���������� true (������) ���� ���� ������ ������ �������.
// ����������: ��������������� ������� ������ false ��� ������������� press stamp.
//-----------------------------------------------------------------------------
bool Input::GetButtonPress( char button, bool ignorePressStamp )
{
	if( ( m_mouseState.rgbButtons[button] & 0x80 ) == false )
		return false;

	bool pressed = true;

	if( ignorePressStamp == false )
		if( m_buttonPressStamp[button] == m_pressStamp - 1 || m_buttonPressStamp[button] == m_pressStamp )
			pressed = false;

	m_buttonPressStamp[button] = m_pressStamp;

	return pressed;
}

//-----------------------------------------------------------------------------
// ���������� ��������� ���� �� ��� x.
//-----------------------------------------------------------------------------
long Input::GetPosX()
{
	return m_position.x;
}

//-----------------------------------------------------------------------------
// ���������� ��������� ���� �� ��� y.
//-----------------------------------------------------------------------------
long Input::GetPosY()
{
	return m_position.y;
}

//-----------------------------------------------------------------------------
// ���������� ��������� ���������� ���� �� ��� x (�������� �� ��� x).
//-----------------------------------------------------------------------------
long Input::GetDeltaX()
{
	return m_mouseState.lX;
}

//-----------------------------------------------------------------------------
// ���������� ��������� ���������� ���� �� ��� y (�������� �� ��� y).
//-----------------------------------------------------------------------------
long Input::GetDeltaY()
{
	return m_mouseState.lY;
}

//-----------------------------------------------------------------------------
// ���������� ��������� ��������� (��������) ������ ��������� ����.
//-----------------------------------------------------------------------------
long Input::GetDeltaWheel()
{
	return m_mouseState.lZ;
}