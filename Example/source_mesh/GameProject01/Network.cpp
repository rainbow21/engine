//-----------------------------------------------------------------------------
// ���������� ������� � �������, ����������� � Network.h.
// ������ ���������� ����������� � Network.h ��� ��������� ����� ��������� ����������.
//
// Original SourceCode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#include "Engine.h"

//-----------------------------------------------------------------------------
// The network class constructor.
//-----------------------------------------------------------------------------
Network::Network( GUID guid, void (*HandleNetworkMessageFunction)( ReceivedMessage *msg ) )
{
	// ��������������� ����������� ������.
	InitializeCriticalSection( &m_sessionCS );
	InitializeCriticalSection( &m_playerCS );
	InitializeCriticalSection( &m_messageCS );

	// �������� ���������� (peer) ��������� DirectPlay � ��������� ����������.
	m_dpp = NULL;
	m_device = NULL;

	// ��������� GUID ����������.
	memcpy( &m_guid, &guid, sizeof( GUID ) );

	// ������ ������� ������ ��������������� ������.
	m_sessions = new LinkedList< SessionInfo >;

	// ������ ������� ������ �������.
	m_players = new LinkedList< PlayerInfo >;

	// ������ ������� ������ ������� ���������.
	m_messages = new LinkedList< ReceivedMessage >;

	// ��������� ������� ��������� �� ����� NetworkSettings.txt.
	Script *settings = new Script( "NetworkSettings.txt" );
	if( settings->GetNumberData( "processing_time" ) == NULL )
	{
		m_port = 2509;
		m_sendTimeOut = 100;
		m_processingTime = 100;
	}
	else
	{
		m_port = *settings->GetNumberData( "port" );
		m_sendTimeOut = *settings->GetNumberData( "send_time_out" );
		m_processingTime = *settings->GetNumberData( "processing_time" );
	}
	SAFE_DELETE( settings );

	// ���������� ������� ������� ��������� ��������� �������� ���������, ����������� ��� ����������.
	m_receiveAllowed = false;

	// ������������� ���������� ������� ��������� (network message handler).
	HandleNetworkMessage = HandleNetworkMessageFunction;

	// ������ � �������������� ���������� (peer) ��������� DirectPlay.
	CoCreateInstance( CLSID_DirectPlay8Peer, NULL, CLSCTX_INPROC, IID_IDirectPlay8Peer, (void**)&m_dpp );
	m_dpp->Initialize( (PVOID)this, NetworkMessageHandler, 0 );

	// ������ ����� ���������� (device address).
	CoCreateInstance( CLSID_DirectPlay8Address, NULL, CLSCTX_INPROC, IID_IDirectPlay8Address, (LPVOID*) &m_device );
	m_device->SetSP( &CLSID_DP8SP_TCPIP );
	m_device->AddComponent( DPNA_KEY_PORT, &m_port, sizeof(DWORD), DPNA_DATATYPE_DWORD );
}

//-----------------------------------------------------------------------------
// The network class destructor.
//-----------------------------------------------------------------------------
Network::~Network()
{
	// ��������� ������� ���������.
	Script *settings = new Script( "NetworkSettings.txt" );
	if( settings->GetNumberData( "processing_time" ) == NULL )
	{
		settings->AddVariable( "port", VARIABLE_NUMBER, &m_port );
		settings->AddVariable( "send_time_out", VARIABLE_NUMBER, &m_sendTimeOut );
		settings->AddVariable( "processing_time", VARIABLE_NUMBER, &m_processingTime );
	}
	else
	{
		settings->SetVariable( "port", &m_port );
		settings->SetVariable( "send_time_out", &m_sendTimeOut );
		settings->SetVariable( "processing_time", &m_processingTime );
	}
	settings->SaveScript();
	SAFE_DELETE( settings );

	// ����������� ����� ����������.
	SAFE_RELEASE( m_device );

	// ��������� � ����������� ���������� (peer) ��������� DirectPlay.
	if( m_dpp != NULL )
		m_dpp->Close( 0 );
	SAFE_RELEASE( m_dpp );

	// ���������� ������� ������ ��������������� ������.
	SAFE_DELETE( m_sessions );

	// ���������� ������� ������ �������.
	SAFE_DELETE( m_players );

	// ���������� ������� ������ ������� ���������.
	SAFE_DELETE( m_messages );

	// ���������� ����������� ������.
	DeleteCriticalSection( &m_sessionCS );
	DeleteCriticalSection( &m_playerCS );
	DeleteCriticalSection( &m_messageCS );
}

//-----------------------------------------------------------------------------
// ��������� (Update) ������ �������� ����������, �������� ��� ������������ ���������.
//-----------------------------------------------------------------------------
void Network::Update()
{
	EnterCriticalSection( &m_messageCS );

	ReceivedMessage *message = m_messages->GetFirst();

	unsigned long endTime = timeGetTime() + m_processingTime;
	while( endTime > timeGetTime() && message != NULL )
	{
		HandleNetworkMessage( message );
		m_messages->Remove( &message );
		message = m_messages->GetFirst();
	}

	LeaveCriticalSection( &m_messageCS );
}

//-----------------------------------------------------------------------------
// ����������� ������ � ��������� ���� (LAN).
//-----------------------------------------------------------------------------
void Network::EnumerateSessions()
{
	// ������� ������� ������.
	m_players->Empty();
	m_messages->Empty();
	m_sessions->Empty();

	// �������������� �������� ���������� (application description).
	DPN_APPLICATION_DESC description;
	ZeroMemory( &description, sizeof( DPN_APPLICATION_DESC ) );
	description.dwSize = sizeof( DPN_APPLICATION_DESC );
	description.guidApplication = m_guid;

	// ����������� ������ ��������� (synchronously).
	m_dpp->EnumHosts( &description, NULL, m_device, NULL, 0, 1, 0, 0, NULL, NULL, DPNENUMHOSTS_SYNC );
}

//-----------------------------------------------------------------------------
// �������� ��������� (host) ������.
//-----------------------------------------------------------------------------
bool Network::Host( char *name, char *session, int players, void *playerData, unsigned long dataSize )
{
	WCHAR wide[MAX_PATH];

	// �������������� � ��������� ��������� ���������� �� ������ (player information structure).
	DPN_PLAYER_INFO player;
	ZeroMemory( &player, sizeof( DPN_PLAYER_INFO ) );
	player.dwSize = sizeof( DPN_PLAYER_INFO );
	player.pvData = playerData;
	player.dwDataSize = dataSize;
	player.dwInfoFlags = DPNINFO_NAME | DPNINFO_DATA;
	mbstowcs( wide, name, MAX_PATH );
	player.pwszName = wide;
	if( FAILED( m_dpp->SetPeerInfo( &player, NULL, NULL, DPNSETPEERINFO_SYNC ) ) )
		return false;

	// �������������� �������� ���������� (application description).
	DPN_APPLICATION_DESC description;
	ZeroMemory( &description, sizeof( DPN_APPLICATION_DESC ) );
	description.dwSize = sizeof( DPN_APPLICATION_DESC );
	description.guidApplication = m_guid;
	description.dwMaxPlayers = players;
	mbstowcs( wide, session, MAX_PATH );
	description.pwszSessionName = wide;

	// Host the session.
	if( FAILED( m_dpp->Host( &description, &m_device, 1, NULL, NULL, NULL, 0 ) ) )
		return false;

	return true;
}

//-----------------------------------------------------------------------------
// �������� �������� (to join) � ��������� ������ �� �������� ������ ��������������� ������.
//-----------------------------------------------------------------------------
bool Network::Join( char *name, int session, void *playerData, unsigned long dataSize )
{
	WCHAR wide[MAX_PATH];

	// ������� ������� ������ ������� (player list) � ������� ��������� (newtork messages).
	m_players->Empty();
	m_messages->Empty();

	// ������� ������������ (invalid) ������.
	if( session < 0 )
		return false;

	// �������������� � ��������� ��������� ���������� �� ������ (player information structure).
	DPN_PLAYER_INFO player;
	ZeroMemory( &player, sizeof( DPN_PLAYER_INFO ) );
	player.dwSize = sizeof( DPN_PLAYER_INFO );
	player.pvData = playerData;
	player.dwDataSize = dataSize;
	player.dwInfoFlags = DPNINFO_NAME | DPNINFO_DATA;
	mbstowcs( wide, name, MAX_PATH );
	player.pwszName = wide;
	if( FAILED( m_dpp->SetPeerInfo( &player, NULL, NULL, DPNSETPEERINFO_SYNC ) ) )
		return false;

	// ������ � ����������� ������ �������� ������ ��������������� ������ (sessions linked list critical section).
	EnterCriticalSection( &m_sessionCS );

	// ������� ���� ��������� ������.
	m_sessions->Iterate( true );
	for( int s = 0; s < session + 1; s++ )
	{
		if( m_sessions->Iterate() ==  NULL )
		{
			LeaveCriticalSection( &m_sessionCS );
			return false;
		}
	}

	// � ������� (join) � ������.
	if( FAILED( m_dpp->Connect( &m_sessions->GetCurrent()->description, m_sessions->GetCurrent()->address, m_device, NULL, NULL, NULL, 0, NULL, NULL, NULL, DPNCONNECT_SYNC ) ) )
	{
		LeaveCriticalSection( &m_sessionCS );
		return false;
	}
	LeaveCriticalSection( &m_sessionCS );

	return true;
}

//-----------------------------------------------------------------------------
// ���������� ������� ������.
//-----------------------------------------------------------------------------
void Network::Terminate()
{
	// ��������� ���������� (terminate) ������ ������ �����.
	if( m_dpnidHost == m_dpnidLocal )
		m_dpp->TerminateSession( NULL, 0, 0 );

	// ��������� ����������. ��� ����� �������� � ��������������� ����������� (peer) ���������� DirectPlay.
	if( m_dpp != NULL )
		m_dpp->Close( 0 );

	// �������������� ���������� (peer) ��������� DirectPlay.
	m_dpp->Initialize( (PVOID)this, NetworkMessageHandler, 0 );
}

//-----------------------------------------------------------------------------
// ������������� ����, ����������� ��������� � ��������� ������� ���������.
//-----------------------------------------------------------------------------
void Network::SetReceiveAllowed( bool allowed )
{
	m_receiveAllowed = allowed;
}

//-----------------------------------------------------------------------------
// ���������� ��������� (next) ������������� ������ �� �������� ������ ��������������� ������.
//-----------------------------------------------------------------------------
SessionInfo *Network::GetNextSession( bool restart )
{
	EnterCriticalSection( &m_sessionCS );

	m_sessions->Iterate( restart );
	if( restart == true )
		m_sessions->Iterate();

	LeaveCriticalSection( &m_sessionCS );

	return m_sessions->GetCurrent();
}

//-----------------------------------------------------------------------------
// ���������� ��������� (pointer) �� ��������� � ����������� � ������ ������ (player information structure of the given player).
//-----------------------------------------------------------------------------
PlayerInfo *Network::GetPlayer( DPNID dpnid )
{
	EnterCriticalSection( &m_playerCS );

	m_players->Iterate( true );
	while( m_players->Iterate() )
	{
		if( m_players->GetCurrent()->dpnid == dpnid )
		{
			LeaveCriticalSection( &m_playerCS );

			return m_players->GetCurrent();
		}
	}

	LeaveCriticalSection( &m_playerCS );

	return NULL;
}

//-----------------------------------------------------------------------------
// ���������� DirectPlay ID ���������� ������.
//-----------------------------------------------------------------------------
DPNID Network::GetLocalID()
{
	return m_dpnidLocal;
}

//-----------------------------------------------------------------------------
// ���������� DirectPlay ID �����.
//-----------------------------------------------------------------------------
DPNID Network::GetHostID()
{
	return m_dpnidHost;
}

//-----------------------------------------------------------------------------
// ����������, �������� �� ��������� ������� ������ ������ ��� ���.
//-----------------------------------------------------------------------------
bool Network::IsHost()
{
	if( m_dpnidHost == m_dpnidLocal )
		return true;
	else
		return false;
}

//-----------------------------------------------------------------------------
// ���������� ������� ���������.
//-----------------------------------------------------------------------------
void Network::Send( void *data, long size, DPNID dpnid, long flags )
{
	DPNHANDLE hAsync;
	DPN_BUFFER_DESC dpbd;

	if( ( dpbd.dwBufferSize = size ) == 0 )
		return;
	dpbd.pBufferData = (BYTE*)data;

	m_dpp->SendTo( dpnid, &dpbd, 1, m_sendTimeOut, NULL, &hAsync, DPNSEND_NOCOMPLETE);
}

//-----------------------------------------------------------------------------
// ���������� ���������� ������� ��������� (internal network message handler).
//-----------------------------------------------------------------------------
HRESULT WINAPI Network::NetworkMessageHandler( PVOID context, DWORD msgid, PVOID data )
{
	// �������� ��������� �� ���������� ������� ������.
	Network *network = (Network*)context;

	// ������������ �������� ���������, ����������� �� �� ����.
	switch( msgid )
	{
		case DPN_MSGID_CREATE_PLAYER:
		{
			unsigned long size = 0;
			DPN_PLAYER_INFO *info = NULL;
			HRESULT hr = DPNERR_CONNECTING;
			PDPNMSG_CREATE_PLAYER msgCreatePlayer = (PDPNMSG_CREATE_PLAYER)data;

			// ������ ��������� ���������� �� ������ (player information structure) ��� ������ ������.
			PlayerInfo *playerInfo = new PlayerInfo;
			ZeroMemory( playerInfo, sizeof( PlayerInfo ) );
			playerInfo->dpnid = msgCreatePlayer->dpnidPlayer;

			// ���������� �������� ������� GetPeerInfo(), �.�. �� �� ��� ����� �������� ������������.
			while( hr == DPNERR_CONNECTING )
				hr = network->m_dpp->GetPeerInfo( playerInfo->dpnid, info, &size, 0 );

			// ���������, ������� �� ������� GetPeerInfo() ������ ��������� DPN_PLAYER_INFO.
			if( hr == DPNERR_BUFFERTOOSMALL )
			{
				info = (DPN_PLAYER_INFO*) new BYTE[size];
				ZeroMemory( info, size );
				info->dwSize = sizeof( DPN_PLAYER_INFO );

				// �������� �����, ��������� ������ ������.
				if( SUCCEEDED( network->m_dpp->GetPeerInfo( playerInfo->dpnid, info, &size, 0 ) ) )
				{
					// ��������� ��� ������ ������.
					playerInfo->name = new char[wcslen( info->pwszName ) + 1];
					ZeroMemory( playerInfo->name, wcslen( info->pwszName ) + 1 );
					wcstombs( playerInfo->name, info->pwszName, wcslen( info->pwszName ) );

					// ��������� ������ ������ ������.
					playerInfo->data = new BYTE[info->dwDataSize];
					memcpy( playerInfo->data, info->pvData, info->dwDataSize );
					playerInfo->size = info->dwDataSize;

					// ��������� ��������� ��������� ���������� ������ � �����.
					if( info->dwPlayerFlags & DPNPLAYER_LOCAL )
						network->m_dpnidLocal = playerInfo->dpnid;
					if( info->dwPlayerFlags & DPNPLAYER_HOST )
						network->m_dpnidHost = playerInfo->dpnid;
				}

				SAFE_DELETE_ARRAY( info );
			}

			// ��������� ������ ������ � ������� ������ ������� (player list).
			EnterCriticalSection( &network->m_playerCS );
			network->m_players->Add( playerInfo );
			LeaveCriticalSection( &network->m_playerCS );

			// ���� ��� �������� ����������� ������� ���������, �� ���������� ��������� (break).
			if( network->HandleNetworkMessage == NULL )
				break;

			// ������ ��������� ������ � �������� ������.
			ReceivedMessage *message = new ReceivedMessage;
			message->msgid = MSGID_CREATE_PLAYER;
			message->dpnid = playerInfo->dpnid;

			// ��������� ��������� ��� ����������� ��������� �����������.
			EnterCriticalSection( &network->m_messageCS );
			network->m_messages->Add( message );
			LeaveCriticalSection( &network->m_messageCS );

			break;
		}

		case DPN_MSGID_DESTROY_PLAYER:
		{
			// ������� ������, ���������� ��� � ������� �� �������� ������ ������� (player list).
			EnterCriticalSection( &network->m_playerCS );
			network->m_players->Iterate( true );
			while( network->m_players->Iterate() )
			{
				if( network->m_players->GetCurrent()->dpnid == ( (PDPNMSG_DESTROY_PLAYER)data )->dpnidPlayer )
				{
					network->m_players->Remove( (PlayerInfo**)network->m_players->GetCurrent() );
					break;
				}
			}
			LeaveCriticalSection( &network->m_playerCS );

			// ���� ��� �������� ����������� ������� ���������, �� ���������� ��������� (break).
			if( network->HandleNetworkMessage == NULL )
				break;

			// ������ ��������� ������ �� ����������� ������.
			ReceivedMessage *message = new ReceivedMessage;
			message->msgid = MSGID_DESTROY_PLAYER;
			message->dpnid = ( (PDPNMSG_DESTROY_PLAYER)data )->dpnidPlayer;

			// ��������� ��������� ��� ����������� ��������� �����������.
			EnterCriticalSection( &network->m_messageCS );
			network->m_messages->Add( message );
			LeaveCriticalSection( &network->m_messageCS );

			break;
		}

		case DPN_MSGID_ENUM_HOSTS_RESPONSE:
		{
			PDPNMSG_ENUM_HOSTS_RESPONSE response = (PDPNMSG_ENUM_HOSTS_RESPONSE)data;

			// ������ ��������� ���������� � ������ (session information structure) ��� ����� ������.
			SessionInfo *sessionInfo = new SessionInfo;
			response->pAddressSender->Duplicate( &sessionInfo->address );
			memcpy( &sessionInfo->description, response->pApplicationDescription, sizeof( DPN_APPLICATION_DESC ) );

			// ��������� ����� ������ � ������� ������ ������ (session list).
			EnterCriticalSection( &network->m_sessionCS );
			network->m_sessions->Add( sessionInfo );
			LeaveCriticalSection( &network->m_sessionCS );

			break;
		}

		case DPN_MSGID_RECEIVE:
		{
			// ���� ��� �������� ����������� ������� ���������, �� ���������� ��������� (break).
			if( network->HandleNetworkMessage == NULL )
				break;

			// ���������, ��������� �� ������� ��������� ���� �������� ���������, ����������� ��� ���������� (application specific messages).
			if( network->m_receiveAllowed == false )
				break;

			// ������ ��������� � ��������� ���������.
			ReceivedMessage *message = new ReceivedMessage;
			memcpy( message, ( (PDPNMSG_RECEIVE)data )->pReceiveData, ( (PDPNMSG_RECEIVE)data )->dwReceiveDataSize );

			// ��������� ��������� ��� ����������� ��������� �����������.
			EnterCriticalSection( &network->m_messageCS );
			network->m_messages->Add( message );
			LeaveCriticalSection( &network->m_messageCS );

			break;
		}

		case DPN_MSGID_TERMINATE_SESSION:
		{
			// ���� ��� �������� ����������� ������� ���������, �� ���������� ��������� (break).
			if( network->HandleNetworkMessage == NULL )
				break;

			// ������ ��������� �� ����������� ������.
			ReceivedMessage *message = new ReceivedMessage;
			message->msgid = MSGID_TERMINATE_SESSION;

			// ��������� ��������� ��� ����������� ��������� �����������.
			EnterCriticalSection( &network->m_messageCS );
			network->m_messages->Add( message );
			LeaveCriticalSection( &network->m_messageCS );

			break;
		}
	}

	return S_OK;
}