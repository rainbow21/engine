//-----------------------------------------------------------------------------
// ����������� ������� ����������� ��� ������������� ������� � ������� �����.
// �������, ����������� ��� ����������, ������ ��������� (derive) �� ������� ������.
//
// Original SourceCode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef SCENE_OBJECT_H
#define SCENE_OBJECT_H

//-----------------------------------------------------------------------------
// ����������� ���� ������� �����
//-----------------------------------------------------------------------------
#define TYPE_SCENE_OBJECT 0

//-----------------------------------------------------------------------------
// Scene Object Class
//-----------------------------------------------------------------------------
class SceneObject : public BoundingVolume
{
public:
	SceneObject( unsigned long type = TYPE_SCENE_OBJECT, char *meshName = NULL, char *meshPath = "./", bool sharedMesh = true );
	virtual ~SceneObject();

	virtual void Update( float elapsed, bool addVelocity = true );
	virtual void Render( D3DXMATRIX *world = NULL );

	virtual void CollisionOccurred( SceneObject *object, unsigned long collisionStamp );

	void Drive( float force, bool lockYAxis = true );
	void Strafe( float force, bool lockYAxis = true );
	void Stop();

	void SetTranslation( float x, float y, float z );
	void SetTranslation( D3DXVECTOR3 translation );
	void AddTranslation( float x, float y, float z );
	void AddTranslation( D3DXVECTOR3 translation );
	D3DXVECTOR3 GetTranslation();

	void SetRotation( float x, float y, float z );
	void SetRotation( D3DXVECTOR3 rotation );
	void AddRotation( float x, float y, float z );
	void AddRotation( D3DXVECTOR3 rotation );
	D3DXVECTOR3 GetRotation();

	void SetVelocity( float x, float y, float z );
	void SetVelocity( D3DXVECTOR3 velocity );
	void AddVelocity( float x, float y, float z );
	void AddVelocity( D3DXVECTOR3 velocity );
	D3DXVECTOR3 GetVelocity();

	void SetSpin( float x, float y, float z );
	void SetSpin( D3DXVECTOR3 spin );
	void AddSpin( float x, float y, float z );
	void AddSpin( D3DXVECTOR3 spin );
	D3DXVECTOR3 GetSpin();

	D3DXVECTOR3 GetForwardVector();
	D3DXVECTOR3 GetRightVector();

	D3DXMATRIX *GetTranslationMatrix();
	D3DXMATRIX *GetRotationMatrix();
	D3DXMATRIX *GetWorldMatrix();
	D3DXMATRIX *GetViewMatrix();

	void SetType( unsigned long type );
	unsigned long GetType();

	void SetFriction( float friction );

	unsigned long GetCollisionStamp();

	void SetVisible( bool visible );
	bool GetVisible();

	void SetEnabled( bool enabled );
	bool GetEnabled();

	void SetGhost( bool ghost );
	bool GetGhost();

	void SetIgnoreCollisions( bool ignoreCollisions );
	bool GetIgnoreCollisions();

	void SetTouchingGroundFlag( bool touchingGround );
	bool IsTouchingGround();

	void SetMesh( char *meshName = NULL, char *meshPath = "./", bool sharedMesh = true );
	Mesh *GetMesh();

protected:
	D3DXVECTOR3 m_forward; // ������ �������, ������������ ����� (Object's forward vector).
	D3DXVECTOR3 m_right; // ������ �������, ������������ ������ (Object's right vector).

	D3DXMATRIX m_worldMatrix; // ������� ������� (World matrix).
	D3DXMATRIX m_viewMatrix; // ������� ���� (View matrix).

private:
	D3DXVECTOR3 m_translation; // ���������� ������� � 3D-������������.
	D3DXVECTOR3 m_rotation; // �������� ������� � ��������.

	D3DXVECTOR3 m_velocity; // �������� ������� � ������/���.
	D3DXVECTOR3 m_spin; // �������� �������� ������� � ������/���.

	D3DXMATRIX m_translationMatrix; // ������� ���������� ������� ������� (Translation matrix).
	D3DXMATRIX m_rotationMatrix; // ������� �������� (Rotation matrix).

	unsigned long m_type; // �������������� ������������ ����� ������� �����.
	float m_friction; // ����������� ������ (���������), ����������� � ��������� �������� � �������� �������.
	unsigned long m_collisionStamp; // �������� ��������� ����, � ������� ��������� ������������.
	bool m_visible; // ���� ��������� �������. ��������� ������� ������ �� ����������.
	bool m_enabled; // ���� "���������" ������� � �����. ����������� ������� ����� �� �����������.
	bool m_ghost; // ���� ����, �������� �� ������ "���������" (ghost). �������-�������� �� ����� ������������ � ������� ���������� � �������� ����� ���.
	bool m_ignoreCollisions; // ����, ���������� �� ��, ����� �� ������ ������������ ������������. "����������" ������������ ����� �����������, �� ��� ������ �� ����� ����������������.
	bool m_touchingGround; // ����, ���������� ������� ������� � ������������ �����/����.
	bool m_sharedMesh; // ���� ���������, ���������� �� ������ 3D-��� ��������� � ������� ��������� ���� ����� ������������ ������ � ����.
	Mesh *m_mesh; // ��������� �� 3D-��� �������.
};

#endif