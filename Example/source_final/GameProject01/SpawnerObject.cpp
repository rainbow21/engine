//-----------------------------------------------------------------------------
// ���������� �������, ����������� � SpawnerObject.h.
//
// Original SourceCode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#include "Engine.h"

//-----------------------------------------------------------------------------
// The spawner object class constructor.
//-----------------------------------------------------------------------------
SpawnerObject::SpawnerObject( char *name, char *path, unsigned long type ) : SceneObject( type )
{
	// ��������� ������-������� ��� ������� (ghost).
	SetGhost( true );

	// ��������� ������ �������-��������.
	Script *script = new Script( name, path );

	// ��������� ������� ���������� �������-��������.
	m_frequency = *script->GetFloatData( "frequency" );

	// ������� ������ �������-��������.
	m_spawnTimer = 0.0f;

	// ��������� ����, ��������������� ��� ������� ������� �����-�������.
	if( script->GetStringData( "sound" ) != NULL )
	{
		m_sound = new Sound( script->GetStringData( "sound" ) );
		m_audioPath = new AudioPath3D;
	}
	else
	{
		m_sound = NULL;
		m_audioPath = NULL;
	}

	// ��������� ������ �������-��������.
	m_objectScript = g_engine->GetScriptManager()->Add( script->GetStringData( "object" ), script->GetStringData( "object_path" ) );

	// �������� ��� �������-��������.
	m_name = new char[strlen( m_objectScript->GetStringData( "name" ) ) + 1];
	strcpy( m_name, m_objectScript->GetStringData( "name" ) );

	// ��������� �������-������� ��� "��������������" �������.
	SetMesh( m_objectScript->GetStringData( "mesh" ), m_objectScript->GetStringData( "mesh_path" ) );

	// ������ ������� ��������� ��������.
	SetSpin( 0.0f, 1.0f, 0.0f );

	// �������� ������ �������-�������. ������ 0.0 ��������, ��� ������
	// ������ ���� ������� �� "��������������" �������.
	if( *script->GetFloatData( "radius" ) != 0.0f )
		SetBoundingSphere( D3DXVECTOR3( 0.0f, 0.0f, 0.0f ), *script->GetFloatData( "radius" ) );
	else if( GetMesh() != NULL )
		SetEllipsoidRadius( *m_objectScript->GetVectorData( "ellipse_radius" ) );

	// ���������� ������ �������-��������.
	SAFE_DELETE( script );
}

//-----------------------------------------------------------------------------
// The spawner object class destructor.
//-----------------------------------------------------------------------------
SpawnerObject::~SpawnerObject()
{
	// ���������� ��������� ����� (string buffer) ���������� ��� �������-��������.
	SAFE_DELETE_ARRAY( m_name );

	// ���������� ���� ������������ �����-������� � ��� ���������.
	SAFE_DELETE( m_sound );
	SAFE_DELETE( m_audioPath );

	// ���������� ������ �������-��������.
	g_engine->GetScriptManager()->Remove( &m_objectScript );
}

//-----------------------------------------------------------------------------
// ��������� ������-�������.
//-----------------------------------------------------------------------------
void SpawnerObject::Update( float elapsed, bool addVelocity )
{
	// ��������� ������ �������� ������ SceneObject.
	SceneObject::Update( elapsed, addVelocity );

	// ���������, ������� �� ������-�������
	if( GetVisible() == false )
	{
		// ������-������� ����� ������ ������� ����� ���� ��� �������
		// ������������� ���������� ������� (frequency).
		m_spawnTimer += elapsed;
		if( m_spawnTimer >= m_frequency )
		{
			SetVisible( true );
			SetIgnoreCollisions( false );
			m_spawnTimer = 0.0f;
		}
	}

	// ��������� ��������� ����� �������.
	if( m_audioPath != NULL )
	{
		m_audioPath->SetPosition( GetTranslation() );
		m_audioPath->SetVelocity( GetVelocity() );
	}
}

//-----------------------------------------------------------------------------
// ����������, ����� ���-���� ����������� �� �������-��������.
//-----------------------------------------------------------------------------
void SpawnerObject::CollisionOccurred( SceneObject *object, unsigned long collisionStamp )
{
	// ��������� �������� ������� SceneObject ���������������� ������������.
	SceneObject::CollisionOccurred( object, collisionStamp );

	// ������������� ��������� ������ �������-������� �����-���� ������
	// �������� ������. �� ����� ���� �������� ������ ���������, ������������ ������������� (user defined objects).
	if( object->GetType() == TYPE_SCENE_OBJECT || object->GetType() == TYPE_ANIMATED_OBJECT || object->GetType() == TYPE_SPAWNER_OBJECT )
		return;

	// ������ �������-������ ���������. ��� ������������ ������������ � ��� ������ ��������
	// �� ��� ���, ���� �� �� �������� ���� ������ (�.�. ����� �� ������ �������).
	SetVisible( false );
	SetIgnoreCollisions( true );

	// ������������� ���� �������.
	if( m_audioPath != NULL && m_sound != NULL )
		m_audioPath->Play( m_sound->GetSegment() );
}

//-----------------------------------------------------------------------------
// ���������� ������ �������������� �������.
//-----------------------------------------------------------------------------
Script *SpawnerObject::GetObjectScript()
{
	return m_objectScript;
}