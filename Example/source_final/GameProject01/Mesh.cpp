//-----------------------------------------------------------------------------
// ���������� �������, ����������� � Mesh.h.
//
// Original Sourcecode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#include "Engine.h"

//-----------------------------------------------------------------------------
// ������ ����� �����.
//-----------------------------------------------------------------------------
HRESULT AllocateHierarchy::CreateFrame( THIS_ LPCSTR Name, LPD3DXFRAME *ppNewFrame )
{
	// ������ ����� ����� � �������� ��� ������.
	Frame *frame = new Frame;
	ZeroMemory( frame, sizeof( Frame ) );

	// �������� ��� ������.
	if( Name == NULL )
	{
		// ��� �����������, ������� ������ ���������� ���.
		static unsigned long nameCount = 0;
		char newName[32];
		sprintf( newName, "unknown_frame_%d", nameCount );
		nameCount++;

		frame->Name = new char[strlen( newName ) + 1];
		strcpy( frame->Name, newName );
	}
	else
	{
		frame->Name = new char[strlen( Name ) + 1];
		strcpy( frame->Name, Name );
	}

	*ppNewFrame = frame;

	return S_OK;
}

//-----------------------------------------------------------------------------
// ������ ����� ���-���������.
//-----------------------------------------------------------------------------
HRESULT AllocateHierarchy::CreateMeshContainer( THIS_ LPCSTR Name, CONST D3DXMESHDATA *pMeshData, CONST D3DXMATERIAL *pMaterials, CONST D3DXEFFECTINSTANCE *pEffectInstances, DWORD NumMaterials, CONST DWORD *pAdjacency, LPD3DXSKININFO pSkinInfo, LPD3DXMESHCONTAINER *ppNewMeshContainer )
{
	// ������ ����� ���-��������� � �������� ��� ������.
	MeshContainer *meshContainer = new MeshContainer;
	ZeroMemory( meshContainer, sizeof( MeshContainer ) );

	// �������� ��� ����.
	if( Name == NULL )
	{
		// ��� �����������, ������� ������ ���������� ���.
		static unsigned long nameCount = 0;
		char newName[32];
		sprintf( newName, "unknown_mesh_%d", nameCount );
		nameCount++;

		meshContainer->Name = new char[strlen( newName ) + 1];
		strcpy( meshContainer->Name, newName );
	}
	else
	{
		meshContainer->Name = new char[strlen( Name ) + 1];
		strcpy( meshContainer->Name, Name );
	}

	// ���������, ���� �� � ���� �����-���� ���������.
	if( ( meshContainer->NumMaterials = NumMaterials ) > 0 )
	{
		// �������� ������ ��� ���������� ����, � ����� �� ��� (�.�. ��� �������).
		meshContainer->materials = new Material*[meshContainer->NumMaterials];
		meshContainer->materialNames = new char*[meshContainer->NumMaterials];

		// ��������� ��� ����� ���������� (�������).
		for( unsigned long m = 0; m < NumMaterials; m++ )
		{
			if( pMaterials[m].pTextureFilename )
			{
				meshContainer->materialNames[m] = new char[strlen( pMaterials[m].pTextureFilename ) + 1];
				memcpy( meshContainer->materialNames[m], pMaterials[m].pTextureFilename, ( strlen( pMaterials[m].pTextureFilename ) + 1 ) * sizeof( char ) );
			}
			else
				meshContainer->materialNames[m] = NULL;

			meshContainer->materials[m] = NULL;
		}
	}

	// ��������� ���������� � ������� ���������� ���� (mesh's adjacency information).
	meshContainer->pAdjacency = new DWORD[pMeshData->pMesh->GetNumFaces() * 3];
	memcpy( meshContainer->pAdjacency, pAdjacency, sizeof( DWORD ) * pMeshData->pMesh->GetNumFaces() * 3 );

	// ��������� ������ ����.
	meshContainer->MeshData.pMesh = meshContainer->originalMesh = pMeshData->pMesh;
	meshContainer->MeshData.Type = D3DXMESHTYPE_MESH;
	pMeshData->pMesh->AddRef();
	pMeshData->pMesh->AddRef();

	// ���������, �������� �� ��� ������������� (skinned mesh).
	if( pSkinInfo != NULL )
	{
		// ��������� ���������� � ����� � � ����.
		meshContainer->pSkinInfo = pSkinInfo;
		pSkinInfo->AddRef();

		// ��������� �������� ��� ��� �������� �������������� (skinned) ����.
		meshContainer->originalMesh->CloneMeshFVF( D3DXMESH_MANAGED, meshContainer->originalMesh->GetFVF(), g_engine->GetDevice(), &meshContainer->MeshData.pMesh );

		// ��������� ������� ���������.
		meshContainer->MeshData.pMesh->GetAttributeTable( NULL, &meshContainer->totalAttributeGroups );
		meshContainer->attributeTable = new D3DXATTRIBUTERANGE[meshContainer->totalAttributeGroups];
		meshContainer->MeshData.pMesh->GetAttributeTable( meshContainer->attributeTable, NULL );
	}

	*ppNewMeshContainer = meshContainer;

	return S_OK;
}

//-----------------------------------------------------------------------------
// ���������� ������ �����.
//-----------------------------------------------------------------------------
HRESULT AllocateHierarchy::DestroyFrame( THIS_ LPD3DXFRAME pFrameToFree )
{
	SAFE_DELETE_ARRAY( pFrameToFree->Name );
	SAFE_DELETE( pFrameToFree );

	return S_OK;
}

//-----------------------------------------------------------------------------
// ���������� ������ �����-���������.
//-----------------------------------------------------------------------------
HRESULT AllocateHierarchy::DestroyMeshContainer( THIS_ LPD3DXMESHCONTAINER pMeshContainerToFree )
{
	MeshContainer *meshContainer = (MeshContainer*)pMeshContainerToFree;

	// ������� ��� ��������� ���� �� ��������� ����������.
	for( unsigned long m = 0; m < meshContainer->NumMaterials; m++ )
		if( meshContainer->materials )
			g_engine->GetMaterialManager()->Remove( &meshContainer->materials[m] );

	// ���������� ���-���������.
	SAFE_DELETE_ARRAY( meshContainer->Name );
	SAFE_DELETE_ARRAY( meshContainer->pAdjacency );
	SAFE_DELETE_ARRAY( meshContainer->pMaterials );
	SAFE_DELETE_ARRAY( meshContainer->materialNames );
	SAFE_DELETE_ARRAY( meshContainer->materials );
	SAFE_DELETE_ARRAY( meshContainer->boneMatrixPointers );
	SAFE_DELETE_ARRAY( meshContainer->attributeTable );
	SAFE_RELEASE( meshContainer->MeshData.pMesh );
	SAFE_RELEASE( meshContainer->pSkinInfo );
	SAFE_RELEASE( meshContainer->originalMesh );
	SAFE_DELETE( meshContainer );

	return S_OK;
}

//-----------------------------------------------------------------------------
// The mesh class constructor.
//-----------------------------------------------------------------------------
Mesh::Mesh( char *name, char *path ) : Resource< Mesh >( name, path )
{
	// ������ ������ ������� ����� (reference points).
	m_frames = new LinkedList< Frame >;
	m_refPoints = new LinkedList< Frame >;

	// ��������� �������� ����.
	AllocateHierarchy ah;
	D3DXLoadMeshHierarchyFromX((LPCSTR)GetFilename(), D3DXMESH_MANAGED, g_engine->GetDevice(), &ah, NULL, (D3DXFRAME**)&m_firstFrame, &m_animationController );

	// ���������� ��������� ��� ����� �������� (animation tracks).
	if( m_animationController != NULL )
		for( unsigned long t = 0; t < m_animationController->GetMaxNumTracks(); ++t )
			m_animationController->SetTrackEnable( t, false );

	// �������� (Invalidate) ������ ������� ������������� ������ (bone transformation matrices array).
	m_boneMatrices = NULL;
	m_totalBoneMatrices = 0;

	// �������������� �������� �������.
	PrepareFrame( m_firstFrame );

	// �������� ������ ��� ������ ������������� ������.
	m_boneMatrices = new D3DXMATRIX[m_totalBoneMatrices];

	// ������ ��������� (���������������) ������ ����.
	m_staticMesh = new MeshContainer;
	ZeroMemory( m_staticMesh, sizeof( MeshContainer ) );

	// ��������� ���.
	ID3DXBuffer *materialBuffer = NULL, *adjacencyBuffer = NULL;
	D3DXLoadMeshFromX( GetFilename(), D3DXMESH_MANAGED, g_engine->GetDevice(), &adjacencyBuffer, &materialBuffer, NULL, &m_staticMesh->NumMaterials, &m_staticMesh->originalMesh );

	// ������������ ��� ��� ������ ������������������ ����������.
	m_staticMesh->originalMesh->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)adjacencyBuffer->GetBufferPointer(), NULL, NULL, NULL);

	// ��������� �������� � ������� ���������� (adjacency buffer), ������� ���������� ���.
	SAFE_RELEASE(adjacencyBuffer);
	// ���������, ���� �� � ���� ���������.
	if( m_staticMesh->NumMaterials > 0 )
	{
		// ������ ������ ����������.
		m_staticMesh->materials = new Material*[m_staticMesh->NumMaterials];

		// �������� ������ ���������� �� ������ ����������.
		D3DXMATERIAL *materials = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();

		// ��������� ������ �������� � ������ ����� �������� ����������.
		for( unsigned long m = 0; m < m_staticMesh->NumMaterials; m++ )
		{
			// ����������, ��� �������� ����� ��������.
			if( materials[m].pTextureFilename )
			{
				// �������� ��� ������� ��������� � ��������� ���.
				char *name = new char[strlen( materials[m].pTextureFilename ) + 5];
				sprintf( name, "%s.txt", materials[m].pTextureFilename );
				m_staticMesh->materials[m] = g_engine->GetMaterialManager()->Add( name, GetPath() );
				SAFE_DELETE_ARRAY( name );
			}
			else
				m_staticMesh->materials[m] = NULL;
		}
	}

	// ������ �������������� ����� ������ ����.
	BoundingVolumeFromMesh( m_staticMesh->originalMesh );

	// ���������� ����� ����������.
	SAFE_RELEASE( materialBuffer );

	// ������ ������ ������ (vertex array) � ������ �������� (array of indices) � ���.
	m_vertices = new Vertex[m_staticMesh->originalMesh->GetNumVertices()];
	m_indices = new unsigned short[m_staticMesh->originalMesh->GetNumFaces() * 3];

	// ���������� ������� ��� ���������� ��������� ����� ������� ������ ���������� ���� �
	// �������� ����� ����� �� ����� ���� ������������ � ��������� ����� ��� ���������� "�� ����".
	Vertex* verticesPtr;
	m_staticMesh->originalMesh->LockVertexBuffer( 0, (void**)&verticesPtr );
	unsigned short *indicesPtr;
	m_staticMesh->originalMesh->LockIndexBuffer( 0, (void**)&indicesPtr );

	memcpy( m_vertices, verticesPtr, VERTEX_FVF_SIZE * m_staticMesh->originalMesh->GetNumVertices() );
	memcpy( m_indices, indicesPtr, sizeof( unsigned short ) * m_staticMesh->originalMesh->GetNumFaces() * 3 );

	m_staticMesh->originalMesh->UnlockVertexBuffer();
	m_staticMesh->originalMesh->UnlockIndexBuffer();
}

//-----------------------------------------------------------------------------
// The mesh class destructor.
//-----------------------------------------------------------------------------
Mesh::~Mesh()
{
	// ���������� �������� �������.
	AllocateHierarchy ah;
	D3DXFrameDestroy( m_firstFrame, &ah );

	// ���������� ������ ������� � ������ ������� ����� (reference points list).
	m_frames->ClearPointers();
	SAFE_DELETE( m_frames );
	m_refPoints->ClearPointers();
	SAFE_DELETE( m_refPoints );

	// ����������� ���������� ��������.
	SAFE_RELEASE( m_animationController );

	// ���������� ������� (��������������, ����������) ������.
	SAFE_DELETE_ARRAY( m_boneMatrices );

	// ���������� ��������� ���.
	if( m_staticMesh )
	{
		// ������� ��� �������� ���������� ����.
		for( unsigned long m = 0; m < m_staticMesh->NumMaterials; m++ )
			if( m_staticMesh->materials )
				g_engine->GetMaterialManager()->Remove( &m_staticMesh->materials[m] );

		// ��������� ���������.
		SAFE_DELETE_ARRAY( m_staticMesh->materials );
		SAFE_RELEASE( m_staticMesh->originalMesh );
		SAFE_DELETE( m_staticMesh );
	}

	// ���������� ������� ������ � �������� ������.
	SAFE_DELETE_ARRAY( m_vertices );
	SAFE_DELETE_ARRAY( m_indices );
}

//-----------------------------------------------------------------------------
// ��������� ���.
//-----------------------------------------------------------------------------
void Mesh::Update()
{
	UpdateFrame( m_firstFrame );
}

//-----------------------------------------------------------------------------
// �������� ���.
//-----------------------------------------------------------------------------
void Mesh::Render()
{
	RenderFrame( m_firstFrame );
}

//-----------------------------------------------------------------------------
// ������ ���� ����������� �������� ���� (mesh's animation controller).
//-----------------------------------------------------------------------------
void Mesh::CloneAnimationController( ID3DXAnimationController **animationController )
{
	if( m_animationController )
		m_animationController->CloneAnimationController( m_animationController->GetMaxNumAnimationOutputs(), m_animationController->GetMaxNumAnimationSets(), m_animationController->GetMaxNumTracks(), m_animationController->GetMaxNumEvents(), &*animationController );
	else
		*animationController = NULL;
}

//-----------------------------------------------------------------------------
// ���������� ��������� (�� �������������) ������ ����.
//-----------------------------------------------------------------------------
MeshContainer *Mesh::GetStaticMesh()
{
	return m_staticMesh;
}

//-----------------------------------------------------------------------------
// ���������� ��������� (�� �������������) ������� ����.
//-----------------------------------------------------------------------------
Vertex *Mesh::GetVertices()
{
	return m_vertices;
}

//-----------------------------------------------------------------------------
// ���������� ������� ������ ����.
//-----------------------------------------------------------------------------
unsigned short *Mesh::GetIndices()
{
	return m_indices;
}

//-----------------------------------------------------------------------------
// ���������� ������ ������� � ����.
//-----------------------------------------------------------------------------
LinkedList< Frame > *Mesh::GetFrameList()
{
	return m_frames;
}

//-----------------------------------------------------------------------------
// ���������� ����� � ������ ������.
//-----------------------------------------------------------------------------
Frame *Mesh::GetFrame( char *name )
{
	m_frames->Iterate( true );
	while( m_frames->Iterate() )
		if( strcmp( m_frames->GetCurrent()->Name, name ) == 0 )
			return m_frames->GetCurrent();

	return NULL;
}

//-----------------------------------------------------------------------------
// ���������� ������� ����� (reference point) � ������ ������.
//-----------------------------------------------------------------------------
Frame *Mesh::GetReferencePoint( char *name )
{
	m_refPoints->Iterate( true );
	while( m_refPoints->Iterate() )
		if( strcmp( m_refPoints->GetCurrent()->Name, name ) == 0 )
			return m_refPoints->GetCurrent();

	return NULL;
}

//-----------------------------------------------------------------------------
// �������������� ������ �����.
//-----------------------------------------------------------------------------
void Mesh::PrepareFrame( Frame *frame )
{
	m_frames->Add( frame );

	// ���������, �������� �� ������ ����� ������� ������.
	if( strncmp( "rp_", frame->Name, 3 ) == 0 )
		m_refPoints->Add( frame );

	// ������������� �������� ��������� �������������.
	frame->finalTransformationMatrix = frame->TransformationMatrix;

	// �������������� ���-��������� ������, ���� ������� �������.
	if( frame->pMeshContainer != NULL )
	{
		MeshContainer *meshContainer = (MeshContainer*)frame->pMeshContainer;

		// ���������, �������� �� ������ ��� ������������� (skinned).
		if( meshContainer->pSkinInfo != NULL )
		{
			// ������ ������ ���������� ������� (��������������) ������.
			meshContainer->boneMatrixPointers = new D3DXMATRIX*[meshContainer->pSkinInfo->GetNumBones()];

			// ������������� ��������� �� ������� ������������� ������ ����.
			for( unsigned long b = 0; b < meshContainer->pSkinInfo->GetNumBones(); b++ )
			{
				Frame *bone = (Frame*)D3DXFrameFind( m_firstFrame, meshContainer->pSkinInfo->GetBoneName( b ) );
				if( bone == NULL )
					continue;

				meshContainer->boneMatrixPointers[b] = &bone->finalTransformationMatrix;
			}

			// �����������, ������� ������ ���������� �� ���� ���-�����������.
			if( m_totalBoneMatrices < meshContainer->pSkinInfo->GetNumBones() )
				m_totalBoneMatrices = meshContainer->pSkinInfo->GetNumBones();
		}

		// ���������, ���� �� � ���� ��������.
		if( meshContainer->NumMaterials > 0 )
		{
			// ��������� ��� ��������� ����� �������� ����������.
			for( unsigned long m = 0; m < meshContainer->NumMaterials; m++ )
			{
				// ����������, ��� �������� �������� ��������.
				if( meshContainer->materialNames[m] != NULL )
				{
					// �������� ��� ������� ��������� � ��������� ���.
					char *name = new char[strlen( meshContainer->materialNames[m] ) + 5];
					sprintf( name, "%s.txt", meshContainer->materialNames[m] );
					meshContainer->materials[m] = g_engine->GetMaterialManager()->Add( name, GetPath() );
					SAFE_DELETE_ARRAY( name );
				}
			}
		}
	}

	// �������������� ������������� ������ (siblings) ������� ������.
	if( frame->pFrameSibling != NULL )
		PrepareFrame( (Frame*)frame->pFrameSibling );

	// �������������� �������� (�����������) ������ (childrens) ������� ������.
	if( frame->pFrameFirstChild != NULL )
		PrepareFrame( (Frame*)frame->pFrameFirstChild );
}

//-----------------------------------------------------------------------------
// ��������� ������� �������������� ������� ������.
//-----------------------------------------------------------------------------
void Mesh::UpdateFrame( Frame *frame, D3DXMATRIX *parentTransformationMatrix )
{
	if( parentTransformationMatrix != NULL )
		D3DXMatrixMultiply( &frame->finalTransformationMatrix, &frame->TransformationMatrix, parentTransformationMatrix );
	else
		frame->finalTransformationMatrix = frame->TransformationMatrix;

	// ��������� ������������� ������ (siblings) ������� ������.
	if( frame->pFrameSibling != NULL )
		UpdateFrame( (Frame*)frame->pFrameSibling, parentTransformationMatrix );

	// ��������� �������� (�����������) ������ (childrens) ������� ������.
	if( frame->pFrameFirstChild != NULL )
		UpdateFrame( (Frame*)frame->pFrameFirstChild, &frame->finalTransformationMatrix );
}

//-----------------------------------------------------------------------------
// �������� ���-���������� ������� ������, ���� ������� �������.
//-----------------------------------------------------------------------------
void Mesh::RenderFrame( Frame *frame )
{
	MeshContainer *meshContainer = (MeshContainer*)frame->pMeshContainer;

	// �������� ��� ������� ������, ���� ������� �������.
	if( frame->pMeshContainer != NULL )
	{
		// ���������, ��������� �� ������ ��� ������������� (skined).
		if( meshContainer->pSkinInfo != NULL )
		{
			// ������ ������������� ������ � �������������� ������ �������������� ����.
			for( unsigned long b = 0; b < meshContainer->pSkinInfo->GetNumBones(); ++b )
				D3DXMatrixMultiply( &m_boneMatrices[b], meshContainer->pSkinInfo->GetBoneOffsetMatrix( b ), meshContainer->boneMatrixPointers[b] );

			// ��������� ������� ���� � ������ ������ �������������� ����.
			PBYTE sourceVertices, destinationVertices;
			meshContainer->originalMesh->LockVertexBuffer( D3DLOCK_READONLY, (void**)&sourceVertices );
			meshContainer->MeshData.pMesh->LockVertexBuffer( 0, (void**)&destinationVertices );
			meshContainer->pSkinInfo->UpdateSkinnedMesh( m_boneMatrices, NULL, sourceVertices, destinationVertices );
			meshContainer->originalMesh->UnlockVertexBuffer();
			meshContainer->MeshData.pMesh->UnlockVertexBuffer();

			// �������� ��� �� ������� ���������.
			for( unsigned long a = 0; a < meshContainer->totalAttributeGroups; a++ )
			{
				g_engine->GetDevice()->SetMaterial( meshContainer->materials[meshContainer->attributeTable[a].AttribId]->GetLighting() );
				g_engine->GetDevice()->SetTexture( 0, meshContainer->materials[meshContainer->attributeTable[a].AttribId]->GetTexture() );
				meshContainer->MeshData.pMesh->DrawSubset( meshContainer->attributeTable[a].AttribId );
			}
		}
		else
		{
			// ��� �� ������������� ���, ������� ��� ����� ��������� ��� ��������� (static).
			for( unsigned long m = 0; m < meshContainer->NumMaterials; m++)
			{
				if( meshContainer->materials[m] )
				{
					g_engine->GetDevice()->SetMaterial( meshContainer->materials[m]->GetLighting() );
					g_engine->GetDevice()->SetTexture( 0, meshContainer->materials[m]->GetTexture() );
				}
				else
					g_engine->GetDevice()->SetTexture( 0, NULL );

				meshContainer->MeshData.pMesh->DrawSubset( m );
			}
		}
	}

	// �������� ������������� ������ (siblings) ������� ������.
	if( frame->pFrameSibling != NULL )
		RenderFrame( (Frame*)frame->pFrameSibling );

	// �������� �������� (����������� � ��������) ������ (childrens) ������� ������.
	if( frame->pFrameFirstChild != NULL )
		RenderFrame( (Frame*)frame->pFrameFirstChild );
}