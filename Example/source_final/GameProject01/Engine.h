//-----------------------------------------------------------------------------
// ������� ������������ ���� ������. ���� ���� ��������� ������ �������,
// � �������� ������������ ������������ ������, ������� ������� ��������� � �������,
// ����������� �� ���� ������� ������.
//
//	Original Source code:
//  Programming a Multiplayer First Person Shooter in DirectX
//  Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef ENGINE_H
#define ENGINE_H

//-----------------------------------------------------------------------------
// System Includes
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <tchar.h>
#include <windowsx.h>

//-----------------------------------
// ��������� �� ������������� 8 ������ DirectInput (����������� 8 ������ ����� �������������)
//-----------------------------------
#define DIRECTINPUT_VERSION 0x0800

//-----------------------------------------------------------------------------
// DirectX Includes
//-----------------------------------------------------------------------------
#include <d3dx9.h>
#include <dinput.h>

#pragma comment (lib, "dplay.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "dsound.lib")

#include <dplay8.h>
#include <dmusici.h>

//-----------------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------------
#define SAFE_DELETE( p )       { if( p ) { delete ( p );     ( p ) = NULL; } }
#define SAFE_DELETE_ARRAY( p ) { if( p ) { delete[] ( p );   ( p ) = NULL; } }
#define SAFE_RELEASE( p )      { if( p ) { ( p )->Release(); ( p ) = NULL; } }

//-----------------------------------------------------------------------------
// Engine Includes
//-----------------------------------------------------------------------------
#include "Resource.h"
#include "LinkedList.h"
#include "ResourceManagement.h"
#include "Geometry.h"
#include "Font.h"
#include "Scripting.h"
#include "DeviceEnumeration.h"
#include "Input.h"
#include "Network.h"
#include "SoundSystem.h"
#include "BoundingVolume.h"
#include "Material.h"
#include "Mesh.h"
#include "SceneObject.h"
#include "AnimatedObject.h"
#include "SpawnerObject.h"
#include "RenderCache.h"
#include "State.h"

//-----------------------------------------------------------------------------
// Engine Setup Structure
//-----------------------------------------------------------------------------
struct EngineSetup
{
	HINSTANCE instance; // ���������� ���������� ����������.
	GUID guid; // Application GUID.
	char *name; // �������� ����������.
	float scale; // ������� (scale) � ������/�����.
	unsigned char totalBackBuffers; // ����� ������������ ����������.
	void (*HandleNetworkMessage)( ReceivedMessage *msg ); // ���������� ������� ���������.
	void (*StateSetup)(); // ������� ���������� ������.
	void (*CreateMaterialResource)( Material **resource, char *name, char *path ); // ������� �������� ���������.
	char *spawnerPath; // ���� ��� ������ �������� �������-�������.

	//-------------------------------------------------------------------------
	// The engine setup structure constructor.
	//-------------------------------------------------------------------------
	EngineSetup()
	{
		GUID defaultGUID = { 0x24215591, 0x24c0, 0x4316, { 0xb5, 0xb2, 0x67, 0x98, 0x2c, 0xb3, 0x82, 0x54 } };

		instance = NULL;
		name = "Application"; // �������� �������� ����������
		scale = 1.0f;
		totalBackBuffers = 1;
		HandleNetworkMessage = NULL;
		StateSetup = NULL;
		CreateMaterialResource = NULL;
	}
};

//-----------------------------------------------------------------------------
// Engine Class
//-----------------------------------------------------------------------------
class Engine
{
public:
	Engine( EngineSetup *setup = NULL );
	virtual ~Engine();

	void Run();

	HWND GetWindow();
	void SetDeactiveFlag( bool deactive );

	float GetScale(); 
	IDirect3DDevice9 *GetDevice(); 
	D3DDISPLAYMODE *GetDisplayMode(); 
	ID3DXSprite *GetSprite();

	void AddState( State *state, bool change = true );
	void RemoveState( State *state );
	void ChangeState( unsigned long id );
	State *GetCurrentState();

	ResourceManager< Script > *GetScriptManager();
	ResourceManager< Material > *GetMaterialManager();
	ResourceManager< Mesh > *GetMeshManager();

	Input *GetInput();
	Network *GetNetwork();
	SoundSystem *GetSoundSystem();

private:
	bool m_loaded; // ���� ����������, �������� �� ������ ��� ���.
	HWND m_window; // ���������� �������� ����.
	bool m_deactive; // ���� ����������, ������� ���������� ��� ���.

	EngineSetup *m_setup; // ����� (�������) ��������� EngineSetup.

	IDirect3DDevice9 *m_device; 
	D3DDISPLAYMODE m_displayMode; 
	ID3DXSprite *m_sprite; 
	unsigned char m_currentBackBuffer;

	LinkedList< State > *m_states; // ������� ������ (Linked list) �������.
	State *m_currentState; // ��������� �� ������� �����.
	bool m_stateChanged; // ���� ����������, ������� �� ����� � ������� �����.

	ResourceManager< Script > *m_scriptManager; // �������� ��������
	ResourceManager< Material > *m_materialManager; // �������� ����������.
	ResourceManager< Mesh > *m_meshManager; // Mesh manager.

	Input *m_input; // ������ ������ Input.
	Network *m_network; // ������ ������ Network.
	SoundSystem *m_soundSystem; // ������ ������ SoundSystem.
};

//-----------------------------------------------------------------------------
// Externals
//-----------------------------------------------------------------------------
extern Engine *g_engine;

#endif