//-----------------------------------------------------------------------------
// ������ ����� �������� �� ������ SceneObject ��� ��������� ��������-���������. 
// ������� - ������, "�������������" �� ������� ����� ������ �������.
//
// Original SourceCode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef SPAWNER_OBJECT_H
#define SPAWNER_OBJECT_H

//-----------------------------------------------------------------------------
// ����������� ���� Spawner Object
//-----------------------------------------------------------------------------
#define TYPE_SPAWNER_OBJECT 2

//-----------------------------------------------------------------------------
// Spawner Object Class
//-----------------------------------------------------------------------------
class SpawnerObject : public SceneObject
{
public:
	SpawnerObject( char *name, char *path = "./", unsigned long type = TYPE_SPAWNER_OBJECT );
	virtual ~SpawnerObject();

	virtual void Update( float elapsed, bool addVelocity = true );

	virtual void CollisionOccurred( SceneObject *object, unsigned long collisionStamp );

	Script *GetObjectScript();

private:
	char *m_name; // ��� �������, ������� ����� �������� ������ ���������.
	float m_frequency; // ��� ����� ������� ����� ����������� ������� (� ��������).
	float m_spawnTimer; // ������, ������������ ��� ������������ �������� �� ������� �����.
	Sound *m_sound; // ����, ��������������� ��� ���������� ������� �������.
	AudioPath3D *m_audioPath; // ���������, �� ������� ������ ���� ����� ������������.
	Script *m_objectScript; // ������ "��������������" �������.
};

#endif