//-----------------------------------------------------------------------------
// System Includes
//-----------------------------------------------------------------------------
#include <windows.h>

//-----------------------------------------------------------------------------
// Engine Includes
//-----------------------------------------------------------------------------
#include "..\GameProject01\Engine.h"

//-----------------------------------------------------------------------------
// Test State Class
//-----------------------------------------------------------------------------
class TestState : public State
{
public:
	//-------------------------------------------------------------------------
	// Allows the test state to preform any pre-processing construction.
	//-------------------------------------------------------------------------
	virtual void Load()
	{
		// ������ ������������� ������, � ������������� ���� �� ��� ��������.
		m_character = new AnimatedObject( "Marine.x", "./Assets/" );
		m_character->SetTranslation( -100.0f, 0.0f, 0.0f );
		m_character->SetRotation( 0.0f, 3.14f, 0.0f );
		m_character->PlayAnimation( 3, 0.0f );

		// ������ ������ �����, ������� ����� �������������� � �������� ������.
		m_viewer = new SceneObject;
		m_viewer->SetTranslation( 0.0f, 0.0f, -400.0f );
		m_viewer->SetFriction( 5.0f );

		// ������ �������� �������-������.
		m_spawner = new SpawnerObject( "Spawner.txt", "./Assets/" );
		m_spawner->SetTranslation( 100.0f, -50.0f, 0.0f );
	};

	//-------------------------------------------------------------------------
	// Allows the test state to preform any post-processing destruction.
	//-------------------------------------------------------------------------
	virtual void Close()
	{
		SAFE_DELETE( m_character );
		SAFE_DELETE( m_viewer );
		SAFE_DELETE( m_spawner );
	};

	//-------------------------------------------------------------------------
	// ���������� ��������� ��������� ��� �������� �����.
	//-------------------------------------------------------------------------
	virtual void RequestViewer( ViewerSetup *viewer )
	{
		viewer->viewer = m_viewer;
		viewer->viewClearFlags = D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER;
	}

	//-------------------------------------------------------------------------
	// ��������� test state.
	//-------------------------------------------------------------------------
	virtual void Update( float elapsed )
	{
		/*
		// ��������� ���������� ������ (scene object) �� ������� ������ �� ����������.
		if( g_engine->GetInput()->GetKeyPress( DIK_UP, true ) == true )
			m_viewer->Drive( 1000.0f * elapsed, false );
		if( g_engine->GetInput()->GetKeyPress( DIK_DOWN, true ) == true )
			m_viewer->Drive( -1000.0f * elapsed, false );
		if( g_engine->GetInput()->GetKeyPress( DIK_LEFT, true ) == true )
			m_viewer->Strafe( -1000.0f * elapsed, false );
		if( g_engine->GetInput()->GetKeyPress( DIK_RIGHT, true ) == true )
			m_viewer->Strafe( 1000.0f * elapsed, false );
		*/
		if (g_engine->GetInput()->GetKeyPress(DIK_W, true) == true)
			m_viewer->Drive(2000.0f * elapsed, false);
		if (g_engine->GetInput()->GetKeyPress(DIK_S, true) == true)
			m_viewer->Drive(-2000.0f * elapsed, false);
		if (g_engine->GetInput()->GetKeyPress(DIK_Q, true))
			PostQuitMessage(0);
		
		
		/*
		if (g_engine->GetInput()->GetDeltaWheel() > 0)
			m_viewer->Drive(-2000.0f *elapsed, false);
		if (g_engine->GetInput()->GetDeltaWheel() < 0)
			m_viewer->Drive(2000.0f *elapsed, false);
		*/
		//m_viewer->Drive((float)g_engine->GetInput()->GetDeltaY()*2.0f, false);
		m_viewer->Drive((float)g_engine->GetInput()->GetDeltaWheel()*2.0f, false);
		m_viewer->AddRotation((float)g_engine->GetInput()->GetDeltaY()*0.005f, (float)g_engine->GetInput()->GetDeltaX()*0.005f, 0.0f);


		// ���������� �������� ������, ����������� �� ����������� ����.
		//m_viewer->AddRotation( (float)g_engine->GetInput()->GetDeltaY() * elapsed, (float)g_engine->GetInput()->GetDeltaX() * elapsed, 0.0f );

		m_character->Update( elapsed );
		m_viewer->Update( elapsed );
		m_spawner->Update( elapsed );
	};

	//-------------------------------------------------------------------------
	// �������� test state.
	//-------------------------------------------------------------------------
	virtual void Render()
	{
		m_character->Render();
		m_spawner->Render();
	};

private:
	AnimatedObject *m_character; // ������������� ������ ���������.
	SceneObject *m_viewer; // ������ ��������� ����� ��������� ���� ������.
	SpawnerObject *m_spawner; // �������� �������-������.
};

//-----------------------------------------------------------------------------
// ��������� ������, ����������� ��� ����������.
//-----------------------------------------------------------------------------
void StateSetup()
{
	g_engine->AddState( new TestState, true );
}

//-----------------------------------------------------------------------------
// ������� ����� � ����������.
//-----------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE instance, HINSTANCE prev, LPSTR cmdLine, int cmdShow )
{
	// ������ ��������� ��������� EngineSetup.
	EngineSetup setup;
	setup.instance = instance;
	setup.name = "Object Test";
	setup.scale = 0.01f;
	setup.StateSetup = StateSetup;

	// ������ ��������� ������ (��������� ��������� EngineSetup), � �������� ���.
	new Engine( &setup );
	g_engine->Run();

	return true;
}