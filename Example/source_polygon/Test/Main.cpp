//-----------------------------------------------------------------------------
// System Includes
//-----------------------------------------------------------------------------
#include <windows.h>

//-----------------------------------------------------------------------------
// Engine Includes
//-----------------------------------------------------------------------------
#include "..\GameProject01\Engine.h"


//-----------------------------------------------------------------------------
// Test State Class
//-----------------------------------------------------------------------------
class TestState : public State
{
public:
	//-------------------------------------------------------------------------
	// Allows the test state to preform any pre-processing construction.
	//-------------------------------------------------------------------------
	virtual void Load()
	{
		m_mesh = new Mesh( "Gun.x", "./Assets/" );

		// ������������� ���������� ������� ��������� ��� ��������� ��������� ����.
		D3DXMATRIX view;
		D3DXMatrixLookAtLH( &view, &D3DXVECTOR3( -50.0f, 50.0f, -150.0f ), &D3DXVECTOR3( 0.0f, 0.0f, 0.0f ), &D3DXVECTOR3( 0.0f, 1.0f, 0.0f ) );
		g_engine->GetDevice()->SetTransform( D3DTS_VIEW, &view );	
	};

	//--------------------------------------
	// ���������� ������ �� ������� ������ �� ����������...
	//----------------------------------------
	virtual void Update(float elapsed)
	{
		static D3DXVECTOR3 pos = D3DXVECTOR3( -50.0f, 50.0f, -150.0f);
			if (g_engine->GetInput()->GetKeyPress(DIK_W, true) == true) pos.z += 0.05f;
			if (g_engine->GetInput()->GetKeyPress(DIK_S, true) == true) pos.z -= 0.05f;
			if (g_engine->GetInput()->GetKeyPress(DIK_A, true) == true) pos.x -= 0.05f;
			if (g_engine->GetInput()->GetKeyPress(DIK_D, true) == true) pos.x += 0.05f;

		D3DXMATRIX view;
		D3DXMatrixLookAtLH(&view, &pos, &D3DXVECTOR3(pos.x + 50.f, pos.y - 50.0f, pos.z + 150.0f),
			&D3DXVECTOR3(0.0f, 1.0f, 0.0f));

		g_engine->GetDevice()->SetTransform(D3DTS_VIEW, &view);
	}

	//-------------------------------------------------------------------------
	// Allows the test state to preform any post-processing destruction.
	//-------------------------------------------------------------------------
	virtual void Close()
	{
		SAFE_DELETE( m_mesh );
	};

	//-------------------------------------------------------------------------
	// ���������� ������� ��������� ������ ��� ������� �����.
	//-------------------------------------------------------------------------
	virtual void RequestViewer( ViewerSetup *viewer )
	{
		viewer->viewClearFlags = D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER;
	}

	//-------------------------------------------------------------------------
	// Renders the test state.
	//-------------------------------------------------------------------------
	virtual void Render()
	{
		m_mesh->Render();
	};

private:
	Mesh *m_mesh; // ��������� �� 3D-���.
};

//-----------------------------------------------------------------------------
// ��������� ������, ����������� ��� ����������.
//-----------------------------------------------------------------------------
void StateSetup()
{
	g_engine->AddState( new TestState, true );
}

//-----------------------------------------------------------------------------
// ����� ����� � ����������.
//-----------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE instance, HINSTANCE prev, LPSTR cmdLine, int cmdShow )
{
	// ������ ��������� EngineSetup.
	EngineSetup setup;
	setup.instance = instance;
	setup.name = "Mesh/Material Test";
	setup.scale = 0.01f;
	setup.StateSetup = StateSetup;

	// ������ ������� ������ (��������� ��������� EngineSetup), ����� ��������� ���.
	new Engine( &setup );
	g_engine->Run();

	return true;
}