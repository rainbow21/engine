//-----------------------------------------------------------------------------
// ��������, ��������� �� �������� � �������� ������� ���������.
//
// Original SourceCode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef MATERIAL_H
#define MATERIAL_H

//-----------------------------------------------------------------------------
// Material Class
//-----------------------------------------------------------------------------
class Material : public Resource< Material >
{
public:
	Material( char *name, char *path = "./" );
	virtual ~Material();

	IDirect3DTexture9 *GetTexture();
	D3DMATERIAL9 *GetLighting();
	unsigned long GetWidth();
	unsigned long GetHeight();
	bool GetIgnoreFace();
	bool GetIgnoreFog();
	bool GetIgnoreRay();

private:
	IDirect3DTexture9 *m_texture; // Direct3D texture.
	D3DMATERIAL9 m_lighting; // �������� ���������.
	unsigned long m_width; // ����� ��������.
	unsigned long m_height; // ������ ��������.
	bool m_ignoreFace; // ���������, ������ �� �������������� ����� � ������ ����������.
	bool m_ignoreFog; // ���������, ������ �� ����� � ������ ���������� ������������ ����� (fog).
	bool m_ignoreRay; // ���������, ������ �� ����� � ������ ���������� ������������ ����������� �������� ����� (ray intersection).
};

#endif