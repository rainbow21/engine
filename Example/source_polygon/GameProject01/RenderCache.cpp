//-----------------------------------------------------------------------------
// File: RenderCache.cpp
//
// Description: RenderCache.h implementation.
//              Refer to the RenderCache.h interface for more details.
// ���������� ������ RenderCache, ������������ � RenderCache.h
//
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#include "Engine.h"

//-----------------------------------------------------------------------------
// The render cache class constructor.
//-----------------------------------------------------------------------------
RenderCache::RenderCache( IDirect3DDevice9 *device, Material *material )
{
	m_device = device;

	m_material = material;

	m_indexBuffer = NULL;
	m_totalIndices = 0;
}

//-----------------------------------------------------------------------------
// The render cache class destructor.
//-----------------------------------------------------------------------------
RenderCache::~RenderCache()
{
	SAFE_RELEASE( m_indexBuffer );
}

//-----------------------------------------------------------------------------
// ����������� ������ ������-���� ��� ���������� ��� ����� ������.
//-----------------------------------------------------------------------------
void RenderCache::AddFace()
{
	m_totalIndices += 3;
}

//-----------------------------------------------------------------------------
// �������������� ������-��� � �������������. ������� ���������� �� ������ ��������� ������� ������-����.
//-----------------------------------------------------------------------------
void RenderCache::Prepare( unsigned long totalVertices )
{
	// ������������� ����� ����� ������, ����������� � ����������.
	m_totalVertices = totalVertices;

	// ������ ��������� ����� ������-����.
	m_device->CreateIndexBuffer( m_totalIndices * sizeof( unsigned short ), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_indexBuffer, NULL );
}

//-----------------------------------------------------------------------------
// ����������� ������-��� � ���, ��� ��������� ��� �������.
//-----------------------------------------------------------------------------
void RenderCache::Begin()
{
	m_indexBuffer->Lock( 0, 0, (void**)&m_indexPointer, 0 );

	m_faces = 0;
}

//-----------------------------------------------------------------------------
// �������� ������� ������ ������ �����, ����������� � ����������.
//-----------------------------------------------------------------------------
void RenderCache::RenderFace( unsigned short vertex0, unsigned short vertex1, unsigned short vertex2 )
{
	*m_indexPointer++ = vertex0;
	*m_indexPointer++ = vertex1;
	*m_indexPointer++ = vertex2;

	m_faces++;
}

//-----------------------------------------------------------------------------
// ����������� ������-��� � ���, ��� ��������� �������, ������� ����� �������� �����.
//-----------------------------------------------------------------------------
void RenderCache::End()
{
	// ������������ (unlock) ����� ��������.
	m_indexBuffer->Unlock();

	// ���������, ���� �� �����, ����������� � ����������.
	if( m_faces == 0 )
		return;

	// ���������, ������ �� �������� ������������ �����.
	if( m_material->GetIgnoreFog() == true )
		m_device->SetRenderState( D3DRS_FOGENABLE, false );

	// ��������� �������� � ��������.
	m_device->SetMaterial( m_material->GetLighting() );
	m_device->SetTexture( 0, m_material->GetTexture() );

	// ��������� ������� ������ ������ ��� ���������� ���������� ������.
	m_device->SetIndices( m_indexBuffer );

	// �������� ��� �����.
	m_device->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 0, m_totalVertices, 0, m_faces );

	// ��������������� ��������� ������, ���� �� ���� ��������.
	if( m_material->GetIgnoreFog() == true )
		m_device->SetRenderState( D3DRS_FOGENABLE, true );
}

//-----------------------------------------------------------------------------
// ���������� ��������� �� ��������, ������������ ������ ������-�����.
//-----------------------------------------------------------------------------
Material *RenderCache::GetMaterial()
{
	return m_material;
}