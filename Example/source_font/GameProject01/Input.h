//-----------------------------------------------------------------------------
// �����-������ ��� ��������� ����������������� ����� ����� DirectInput.
// �������������� ���������� � ����.
//
// Original sourcecode:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
//-----------------------------------------------------------------------------
#ifndef INPUT_H
#define INPUT_H

//-----------------------------------------------------------------------------
// ����� Input
//-----------------------------------------------------------------------------
class Input
{
public:
	Input( HWND window );	// �����������
	virtual ~Input();		// ����������

	void Update();

	bool GetKeyPress( char key, bool ignorePressStamp = false );

	bool GetButtonPress( char button, bool ignorePressStamp = false );
	long GetPosX();
	long GetPosY();
	long GetDeltaX();
	long GetDeltaY();
	long GetDeltaWheel();

private:
	HWND m_window; // ���������� ������������� ����.
	IDirectInput8 *m_di; // ������ DirectInput.
	unsigned long m_pressStamp; // ������� ������� ������� (������������� � ������ ������).

	IDirectInputDevice8 *m_keyboard; // ���������� DirectInput ����������.
	char m_keyState[256]; // ��������� ��������� ������.
	unsigned long m_keyPressStamp[256]; // ������� � ������� ������ �� ������ � ���������� �����.

	IDirectInputDevice8 *m_mouse; // ���������� DirectInput ����.
	DIMOUSESTATE m_mouseState; // ������ ��������� ������ ����.
	unsigned long m_buttonPressStamp[3]; // ������� � ������� ������ �� ������ ���� � ���������� �����.
	POINT m_position; // ������ ������� ������� ������� ���� �� ������.
};

#endif