//-----------------------------------------------------------------------------
// ���������� ������� � �������, ����������� � Engine.h.
// Refer to the Engine.h interface for more details.
//
// ������������ �������� ���:
// Programming a Multiplayer First Person Shooter in DirectX
// Copyright (c) 2004 Vaughan Young
// ������� ������������ � ��������� ��� MS Visual C++ 2010 -  Igrocoder.ru
//-----------------------------------------------------------------------------
#include "Engine.h"

//-----------------------------------------------------------------------------
// ���������� ���������� � ���������
//-----------------------------------------------------------------------------
Engine *g_engine = NULL;

//-----------------------------------------------------------------------------
// ��������� ��������� Windows (������� ���������).
//-----------------------------------------------------------------------------
LRESULT CALLBACK WindowProc( HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam )
{
	switch( msg )
	{
		case WM_ACTIVATEAPP:
			g_engine->SetDeactiveFlag( !wparam );
			return 0;

		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;

		default:
			return DefWindowProc( wnd, msg, wparam, lparam );
	}
}

//-----------------------------------------------------------------------------
// ����������� ������ Engine.
//-----------------------------------------------------------------------------
Engine::Engine( EngineSetup *setup )
{
	// ���������, ��� ������ ��� �� ��������.
	m_loaded = false;

	// ���� ������� ��������� EngineSetup �� ���������, ����������� �������� �.
	// �����, �������� ��� ������������ ���������.
	m_setup = new EngineSetup;
	if( setup != NULL )
		memcpy( m_setup, setup, sizeof( EngineSetup ) );

	// ��������� ��������� ������ � ���������� ���������� ��� ����� �������� � �������� �������.
	g_engine = this;

	// �������������� ������� ����� � ������������ ���.
	WNDCLASSEX wcex;
	wcex.cbSize        = sizeof( WNDCLASSEX );
	wcex.style         = CS_CLASSDC;
	wcex.lpfnWndProc   = WindowProc;
	wcex.cbClsExtra    = 0;
	wcex.cbWndExtra    = 0;
	wcex.hInstance     = m_setup->instance;
	wcex.hIcon         = LoadIcon( NULL, IDI_APPLICATION );
	wcex.hCursor       = LoadCursor( NULL, IDC_ARROW );
	wcex.hbrBackground = NULL;
	wcex.lpszMenuName  = NULL;
	wcex.lpszClassName = "WindowClass";
	wcex.hIconSm       = LoadIcon( NULL, IDI_APPLICATION );
	RegisterClassEx( &wcex );

	// �������������� COM ��������� ������������� ��������������.
	CoInitializeEx( NULL, COINIT_MULTITHREADED );

	// Create the window and retrieve a handle to it.
// Note: ������� ���� ����� ����������� � ������ windowed/fullscreen (�������/������������� ������).
	m_window = CreateWindowEx( WS_EX_TOPMOST, "WindowClass", m_setup->name, WS_OVERLAPPED, 0, 0, 800, 600, NULL, NULL, m_setup->instance, NULL );

	// ������ ��������� ��������� ����� �� ������ �������� �������.
	srand( timeGetTime() );

	// ������ ��������� �������� � ����� � ������.
	m_loaded = true;
}

//-----------------------------------------------------------------------------
// ���������� ������ Engine.
//-----------------------------------------------------------------------------
Engine::~Engine()
{
	// ���������, ��� ������ ��������.
	if( m_loaded == true )
	{
		// ��, ��� �����, ����� ������������ (��������, ����� �������������� DirectX ����������).
	}

	// ��������������� COM.
	CoUninitialize();

	// Unregister the window class.
	UnregisterClass( "WindowClass", m_setup->instance );

	// ����������� ��������� engine setup.
	SAFE_DELETE( m_setup );
}

//-----------------------------------------------------------------------------
// ������ ������ � ������� ���� ������� ���������.
//-----------------------------------------------------------------------------
void Engine::Run()
{
	// ����������, ��� ������ ��������.
	if( m_loaded == true )
	{
		// ���������� ����.
		ShowWindow( m_window, SW_NORMAL );

		// ������ � ���� ������� ���������.
		MSG msg;
		ZeroMemory( &msg, sizeof( MSG ) );
		while( msg.message != WM_QUIT )
		{
			if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
			else if( !m_deactive )
			{
				// ������������ ����������� �����.
				unsigned long currentTime = timeGetTime();
				static unsigned long lastTime = currentTime;
				float elapsed = ( currentTime - lastTime ) / 1000.0f;
				lastTime = currentTime;
			}
		}
	}

	// ���������� ������.
	SAFE_DELETE( g_engine );
}

//-----------------------------------------------------------------------------
// ���������� ���������� �������� ����.
//-----------------------------------------------------------------------------
HWND Engine::GetWindow()
{
	return m_window;
}

//-----------------------------------------------------------------------------
// ������������� ���� ������������.
//-----------------------------------------------------------------------------
void Engine::SetDeactiveFlag( bool deactive )
{
	m_deactive = deactive;
}